defmodule KusanagiWeb.GitVersionController do
  use KusanagiWeb, :controller

  action_fallback(KusanagiWeb.FallbackController)

  def index(conn, _params) do
    case File.read(Path.absname("/root/app_config/current_version.txt")) do
      {:ok, body} ->
        text(conn, String.replace(body, "\n", ""))

      {:error, reason} ->
        {:error, reason}
    end
  end
end
