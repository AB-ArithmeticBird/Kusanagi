defmodule KusanagiWeb.SessionController do
  use KusanagiWeb, :controller
  use PhoenixSwagger

  alias Kusanagi.Auth
  alias Kusanagi.Accounts.Projections.User

  action_fallback(KusanagiWeb.FallbackController)

  swagger_path :create do
    post("/api/users/login")
    summary("Login User")
    description("Login User")

    parameters do
      tracker(
        :body,
        %Schema{
          type: :object,
          example: %{
            user: %{
              password: "12345",
              email: "12345@gmail.com"
            }
          }
        },
        "User Credentials to Login",
        required: true
      )
    end

    response(201, "Ok", %{})
  end

  def create(conn, %{"user" => %{"email" => email, "password" => password}}) do
    with {:ok, %User{} = user} <- Auth.authenticate(email, password),
         {:ok, jwt} <- generate_jwt(user) do
      conn
      |> put_status(:created)
      |> render(KusanagiWeb.UserView, "show.json", user: user, jwt: jwt)
    else
      {:error, :unauthenticated} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(KusanagiWeb.ValidationView, "error.json",
          errors: %{"email or password" => ["is invalid"]}
        )
    end
  end
end
