defmodule KusanagiWeb.OrganizationController do
  use KusanagiWeb, :controller
  use PhoenixSwagger

  alias Kusanagi.Organizations
  alias Kusanagi.Organizations.Projections.Organization

  action_fallback(KusanagiWeb.FallbackController)

  swagger_path :create do
    post("/api/org")
    summary("Create an Organization")
    description("Create an Organization")
    parameters do
      tracker(:body, %Schema{
        type: :object,
        example: %{
          organization: %{
            address: "some address",
      	    attribute: "some attribute",
      	    email: "some email",
      	    name: "some name",
      	    phone_number: "some phone_number",
      	    timeZone: "some timeZone",
      	    status: 1,
      	    website: "some website"
          }
        }
        }, "Create an Organization", required: true)
    end
    response(201, "Ok", %{})
  end
  def create(conn, %{"organization" => organization_params}) do
    with {:ok, %Organization{} = organization} <-
           Organizations.create_organization(organization_params) do
      conn
      |> put_status(:created)
      |> render("show.json", organization: organization)
    end
  end

  swagger_path :index do
    get("/api/org")
    summary("List All Organizations")
    description("List All Organizations")
    response(200, "Ok", %{})
  end
  def index(conn, _params) do
    organizations = Organizations.list_organizations()
    render(conn, "index.json", organizations: organizations)
  end

  swagger_path :show do
    get("/api/org/{uuid}")
    summary("List Organization")
    description("List Organization")
    parameters do
      uuid :path, :string, "The uuid of the Organization", required: true
    end
    response(200, "Ok", %{})
  end
  def show(conn, %{"uuid" => uuid}) do
    organization = Organizations.get_organization!(uuid)
    render(conn, "show.json", organization: organization)
  end

  swagger_path :update do
    put("/api/org/update/{uuid}")
    summary("Update an Organization")
    description("Update an Organization")
    parameters do
      uuid :path, :string, "The uuid of the Organization", required: true
      tracker(:body, %Schema{
        type: :object,
        example: %{
          organization: %{
            address: "some Other address",
      	    email: "some Other email",
      	    name: "some Other name",
      	    phone_number: "some Other phone_number",
      	    timeZone: "some Other timeZone",
      	    website: "some Other website"
          }
        }
        }, "Update an Organization", required: true)
    end
    response(200, "Ok", %{})
  end
  def update(conn, %{"uuid" => uuid, "organization" => organization_params}) do
    organization = Organizations.get_organization!(uuid)

    with {:ok, %Organization{} = organization} <-
           Organizations.update_organization(organization, organization_params) do
      render(conn, "show.json", organization: organization)
    end
  end

  swagger_path :close do
    delete("/api/org/{uuid}")
    summary("Close an Organization")
    description("Close an Organization")
    parameters do
      uuid :path, :string, "The uuid of the Organization", required: true
    end
    response(204, "Ok", %{})
  end
  def close(conn, %{"uuid" => uuid}) do
    organization = Organizations.get_organization!(uuid)

    with {:ok, %Organization{} = _organization} <- Organizations.close_organization(organization) do
      send_resp(conn, :no_content, "")
    end
  end

  swagger_path :status do
    put("/api/org/status")
    summary("Update Status of an Organization")
    description("Update Status of an Organization")
    parameters do
      tracker(:body, %Schema{
        type: :object,
        example: %{
          organization: %{
            uuid: "e51c7f8a-4562-4d91-b4e1-0212ec476510",
            status: "ACTIVE"
          }
        }
        }, "Update Status Of an Organization", required: true)
    end
    response(200, "Ok", %{})
  end
  def status(conn, %{"organization" => %{"uuid" => uuid, "status" => status}}) do
    organization = Organizations.get_organization!(uuid)

    with {:ok, %Organization{} = organization} <-
           Organizations.update_status(organization, %{"uuid" => uuid, "status" => status}) do
      render(conn, "show.json", organization: organization)
    end
  end
end
