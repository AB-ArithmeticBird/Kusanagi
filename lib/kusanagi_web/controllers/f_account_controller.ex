defmodule KusanagiWeb.FAccountController do
  use KusanagiWeb, :controller
  use PhoenixSwagger

  alias Kusanagi.FinancialAccounting
  alias Kusanagi.FinancialAccounting.Projections.FAccount
  alias Kusanagi.OrgBranch.Projections.OrgBranch
  # TODO Cross Bounding Context
  alias Kusanagi.Accounts.Projections.User
  alias Kusanagi.Repo
  alias Kusanagi.OrgBranch.Enums.EnumsOrgBranch

  action_fallback(KusanagiWeb.FallbackController)

  def index(conn, _params) do
    faccounts = FinancialAccounting.list_faccounts()
    render(conn, "index.json", faccounts: faccounts)
  end

  swagger_path :create do
    post("/api/faccounts")
    summary("Open a User Financial Account")
    description("Open a User Financial Account")
    parameters do
      tracker(:body, %Schema{
        type: :object,
        example: %{
          f_account: %{
            account_type: 1,
            account_name: "Liability Account",
            description: "Liability account for the user navneetgupta",
            user_uuid: "ca3e2329-38c4-485a-b99c-b99d0286da42",
            currency: "USD",
            org_id: "e51c7f8a-4562-4d91-b4e1-0212ec476510",
            branch_id: "ac246596-6dc9-4fdf-ab3c-21f10e5a9a3c"
          }
        }
        }, "Open a User Financial Account", required: true)
    end
    response(201, "Ok", %{})
  end
  def create(conn, %{
        "f_account" =>
          %{"user_uuid" => user_uuid, "branch_id" => branch_id, "org_id" => org_id} =
            f_account_params
      }) do
    _user = Repo.get!(User, user_uuid)

    case Repo.get(OrgBranch, branch_id) do
      nil ->
        {:error, :branch_not_found}

      %OrgBranch{org_uuid: ^org_id, status: status} ->
        cond do
          status == EnumsOrgBranch.org_branch_status()[:ACTIVE] ->
            with {:ok, %FAccount{} = f_account} <-
                   FinancialAccounting.create_f_account(f_account_params) do
              conn
              |> put_status(:created)
              |> render("show.json", f_account: f_account)
            end

          true ->
            {:error, :branch_not_actve}
        end

      _ ->
        {:error, :invalid_org}
    end
  end

  def show(conn, %{"id" => id}) do
    f_account = FinancialAccounting.get_f_account!(id)
    render(conn, "show.json", f_account: f_account)
  end

  swagger_path :debit do
    post("/api/faccounts/debit")
    summary("Debit From a User Account")
    description("Debit From a User Financial Account")
    parameters do
      tracker(:body, %Schema{
        type: :object,
        example: %{
          f_d_account: %{
            uuid: "8c62fb5f-79d7-4bf7-8a11-c7f3f554615f",
            amount: 100
          }
        }
        }, "Debit From a User Financial Account", required: true)
    end
    response(200, "Ok", %{})
  end
  def debit(conn, %{"f_d_account" => %{"uuid" => uuid} = debit_f_account_params}) do
    user = Guardian.Plug.current_resource(conn)
    f_account = Repo.get!(FAccount, uuid)

    with :ok <- Bodyguard.permit(FinancialAccounting, :debit_f_account, user, f_account),
         {:ok, %FAccount{} = f_account} <-
           FinancialAccounting.debit_f_account(debit_f_account_params) do
      conn
      |> put_status(:ok)
      |> render("show.json", f_account: f_account)
    end
  end

  swagger_path :credit do
    post("/api/faccounts/credit")
    summary("Credit to a User Financial Account")
    description("Credit to a User Financial Account")
    parameters do
      tracker(:body, %Schema{
        type: :object,
        example: %{
          f_c_account: %{
            uuid: "8c62fb5f-79d7-4bf7-8a11-c7f3f554615f",
            amount: 100
          }
        }
        }, "Credit to a User Financial Account", required: true)
    end
    response(200, "Ok", %{})
  end
  def credit(conn, %{"f_c_account" => %{"uuid" => uuid} = credit_f_account_params}) do
    user = Guardian.Plug.current_resource(conn)
    f_account = Repo.get!(FAccount, uuid)

    with :ok <- Bodyguard.permit(FinancialAccounting, :credit_f_account, user, f_account),
         {:ok, %FAccount{} = f_account} <-
           FinancialAccounting.credit_f_account(credit_f_account_params) do
      conn
      |> put_status(:ok)
      |> render("show.json", f_account: f_account)
    end
  end

  swagger_path :close do
    post("/api/faccounts/close")
    summary("Close a User Financial Account")
    description("Close a User Financial Account")
    parameters do
      tracker(:body, %Schema{
        type: :object,
        example: %{
          f_cl_account: %{
            uuid: "8c62fb5f-79d7-4bf7-8a11-c7f3f554615f"
          }
        }
        }, "Close a User Financial Account", required: true)
    end
    response(200, "Ok", %{})
  end
  def close(conn, %{"f_cl_account" => f_close_account_params}) do
    with {:ok, %FAccount{} = f_account} <-
           FinancialAccounting.close_f_account(f_close_account_params) do
      conn
      |> put_status(:ok)
      |> render("show.json", f_account: f_account)
    end
  end

  swagger_path :migrate do
    post("/api/faccounts/migrate")
    summary("Migrate a User Financial Account")
    description("Migrate a User Financial Account")
    parameters do
      tracker(:body, %Schema{
        type: :object,
        example: %{
          migrate_f_account: %{
            uuid: "8c62fb5f-79d7-4bf7-8a11-c7f3f554615f",
            new_branch_id: "8c62fb5f-79d7-4bf7-8a11-c7f3f554615f",
            old_branch_id: "8c62fb5f-79d7-4bf7-8a11-c7f3f554615f"
          }
        }
        }, "Migrate a User Financial Account", required: true)
    end
    response(200, "Ok", %{})
  end
  def migrate(conn, %{
        "migrate_f_account" => %{"new_branch_id" => new_branch_id} = migrate_account_params
      }) do
    case Repo.get(OrgBranch, new_branch_id) do
      nil ->
        {:error, :branch_not_found}

      %OrgBranch{org_uuid: org_id, status: status} ->
        cond do
          status == EnumsOrgBranch.org_branch_status()[:ACTIVE] ->
            migrate_account_params = migrate_account_params |> Map.put("new_org_id", org_id)

            with {:ok, %FAccount{} = f_account} <-
                   FinancialAccounting.migrate_f_account(migrate_account_params) do
              conn
              |> put_status(:ok)
              |> render("show.json", f_account: f_account)
            end

          true ->
            {:error, :branch_not_actve}
        end
    end
  end
end
