defmodule KusanagiWeb.OrgBranchController do
  use KusanagiWeb, :controller
  use PhoenixSwagger

  alias Kusanagi.OrgBranch
  alias Kusanagi.Organizations
  alias Kusanagi.OrgBranch.Projections.OrgBranch, as: OrgBranchPro
  alias Kusanagi.OrgBranch.Projections.BranchAttributes
  alias Kusanagi.OrgBranch.Queries.GetOrgBranch
  alias Kusanagi.Repo

  action_fallback(KusanagiWeb.FallbackController)

  swagger_path :add do
    post("/api/branch")
    summary("Add a Branch to an Organization ")
    description("Add a Branch to an Organization")

    parameters do
      tracker(
        :body,
        %Schema{
          type: :object,
          example: %{
            branch: %{
              address: "some address",
              branch_type: "PHYSICAL",
              org_uuid: "org_uuid",
              name: "some name"
            }
          }
        },
        "Add a Branch to an Organization",
        required: true
      )
    end

    response(200, "Ok", %{})
  end

  def add(conn, %{"branch" => %{"org_uuid" => org_uuid} = branch_params}) do
    organization = Organizations.get_organization!(org_uuid)

    with {:ok, %OrgBranchPro{} = org_branch} <-
           OrgBranch.create_branch(organization, branch_params) do
      render(conn, "show.json", branch: org_branch)
    end
  end

  swagger_path :update do
    put("/api/branch/update")
    summary("Update a Branch")
    description("Update a Branch")

    parameters do
      tracker(
        :body,
        %Schema{
          type: :object,
          example: %{
            branch: %{
              uuid: "branch_uuid",
              address: "some address",
              name: "some name"
            }
          }
        },
        "Update a Branch of an Organization",
        required: true
      )
    end

    response(200, "Ok", %{})
  end

  def update(conn, %{"branch" => %{"uuid" => uuid} = branch_params}) do
    branch = OrgBranch.get_org_branch!(uuid)

    with {:ok, %OrgBranchPro{} = org_branch} <- OrgBranch.update_branch(branch, branch_params) do
      render(conn, "show.json", branch: org_branch)
    end
  end

  swagger_path :close do
    delete("/api/branch/{uuid}")
    summary("Delete a Branch")
    description("Delete a Branch")

    parameters do
      uuid(:path, :string, "Branch ID", required: true)
    end

    response(200, "Ok", %{})
  end

  def close(conn, %{"uuid" => uuid}) do
    branch = OrgBranch.get_org_branch!(uuid)

    with {:ok, %OrgBranchPro{} = branch} <- OrgBranch.close_branch(branch) do
      # TODO remove the returnign of branch since it is closed Just for testing purpose kept it
      render(conn, "show.json", branch: branch)
    end
  end

  swagger_path :status do
    put("/api/branch/status")
    summary("Update Status of a Branch")
    description("Update Status of a Branch")

    parameters do
      tracker(
        :body,
        %Schema{
          type: :object,
          example: %{
            branch: %{
              uuid: "branch_uuid",
              status: "some status"
            }
          }
        },
        "Update Status of a Branch",
        required: true
      )
    end

    response(200, "Ok", %{})
  end

  def status(conn, %{"branch" => %{"uuid" => uuid, "status" => status}}) do
    branch = OrgBranch.get_org_branch!(uuid)

    with {:ok, %OrgBranchPro{} = org_branch} <-
           OrgBranch.update_branch_status(branch, %{"status" => status}) do
      render(conn, "show.json", branch: org_branch)
    end
  end

  swagger_path :index do
    get("/api/branch/org_id/{org_uuid}")
    summary("Get all Branch of an Organization")
    description("Get all Branch of an Organization")

    parameters do
      org_uuid(:path, :string, "Organization ID", required: true)
    end

    response(200, "Ok", %{})
  end

  def index(conn, %{"org_uuid" => org_uuid}) do
    branches = org_uuid |> GetOrgBranch.get_org_branches() |> Repo.all()
    render(conn, "index.json", branches: branches)
  end

  swagger_path :show do
    get("/api/branch/{branch_id}")
    summary("Get a Branch Details")
    description("Get a Branch Details")

    parameters do
      branch_id(:path, :string, "Branch Id", required: true)
    end

    response(200, "Ok", %{})
  end

  def show(conn, %{"branch_id" => branch_id}) do
    branch = GetOrgBranch.get_branch(branch_id) |> Repo.one()
    render(conn, "show.json", branch: branch)
  end

  swagger_path :add_attr do
    post("/api/branch/attribute")
    summary("Add an attribute to a  Branch")
    description("Add an attribute to a  Branch")

    parameters do
      tracker(
        :body,
        %Schema{
          type: :object,
          example: %{
            branch_attribute: %{
              key: "attribute key",
              value: "attribute Value",
              b_uuid: "branch_id"
            }
          }
        },
        "Add an attribute to a  Branch",
        required: true
      )
    end

    response(200, "Ok", %{})
  end

  def add_attr(conn, %{"branch_attribute" => %{"b_uuid" => b_uuid} = b_attribute_params}) do
    branch = OrgBranch.get_org_branch!(b_uuid)

    with {:ok, %BranchAttributes{} = branch_attr} <-
           OrgBranch.add_branch_attribute(branch, b_attribute_params) do
      render(conn, "show.json", branch_attr: branch_attr)
    end
  end

  swagger_path :index_attr do
    get("/api/branch/attribute/{b_uuid}")
    summary("List all Branch Attributes")
    description("List all Branch Attributes")

    parameters do
      b_uuid(:path, :string, "Branch ID", required: true)
    end

    response(200, "Ok", %{})
  end

  def index_attr(conn, %{"b_uuid" => _b_uuid} = params) do
    attributes = OrgBranch.list_b_attributes(params)
    render(conn, "index.json", branch_attrs: attributes)
  end

  swagger_path :delete_attr do
    delete("/api/branch/attribute/{branch_id}/{key}")
    summary("Delete a Branch attribute")
    description("Delete a Branch attribute")

    parameters do
      branch_id(:path, :string, "Branch ID", required: true)
      key(:path, :string, "Attribute key", required: true)
    end

    response(200, "Ok", %{})
  end

  def delete_attr(conn, %{"branch_id" => branch_id, "key" => _key} = params) do
    branch = GetOrgBranch.get_branch(branch_id) |> Repo.one!()

    with :ok <- OrgBranch.delete_branch_attribute(branch, params) do
      send_resp(conn, :no_content, "")
    end
  end

  swagger_path :update_attr do
    put("/api/branch/attribute/update")
    summary("Update an attribute of a Branch")
    description("Update an attribute of a Branch")

    parameters do
      tracker(
        :body,
        %Schema{
          type: :object,
          example: %{
            branch_attribute: %{
              key: "existing attribute key",
              value: "updated attribute Value",
              b_uuid: "branch_id"
            }
          }
        },
        "Update an attribute of a Branch",
        required: true
      )
    end

    response(200, "Ok", %{})
  end

  def update_attr(conn, %{"branch_attribute" => %{"b_uuid" => b_uuid} = b_attribute_params}) do
    branch = OrgBranch.get_org_branch!(b_uuid)

    with {:ok, %BranchAttributes{} = branch_attr} <-
           OrgBranch.update_branch_attribute(branch, b_attribute_params) do
      render(conn, "show.json", branch_attr: branch_attr)
    end
  end
end
