defmodule KusanagiWeb.FallbackController do
  use KusanagiWeb, :controller

  def call(conn, {:error, :validation_failure, errors}) do
    conn
    |> put_status(:unprocessable_entity)
    |> render(KusanagiWeb.ValidationView, "error.json", errors: errors)
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> render(KusanagiWeb.ErrorView, :"404")
  end

  def call(conn, {:error, :unauthorized}) do
    conn
    |> put_status(:forbidden)
    |> render(KusanagiWeb.ErrorView, :"403")
  end

  def call(conn, {:error, _, errors}) do
    conn
    |> put_status(:unprocessable_entity)
    |> render(KusanagiWeb.ValidationView, "error.json", errors: errors)
  end

  def call(conn, {:error, errors}) do
    conn
    |> put_status(:unprocessable_entity)
    |> render(KusanagiWeb.ValidationView, "error.json", errors: errors)
  end
end
