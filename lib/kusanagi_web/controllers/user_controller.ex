defmodule KusanagiWeb.UserController do
  use KusanagiWeb, :controller
  use Guardian.Phoenix.Controller
  use PhoenixSwagger

  alias Kusanagi.Accounts
  alias Kusanagi.Accounts.Projections.User

  action_fallback(KusanagiWeb.FallbackController)

  plug(
    Guardian.Plug.EnsureAuthenticated,
    %{handler: KusanagiWeb.ErrorHandler} when action in [:current]
  )

  plug(
    Guardian.Plug.EnsureResource,
    %{handler: KusanagiWeb.ErrorHandler} when action in [:current]
  )

  swagger_path :create do
    post("/api/users")
    summary("Register User")
    description("Register User")
    parameters do
      tracker(:body, %Schema{
        type: :object,
        example: %{
          user: %{
            username: "1235",
            password: "12345",
            email: "12345@gmail.com"
          }
        }
        }, "User to Register", required: true)
    end
    response(201, "Ok", %{})
  end
  def create(conn, %{"user" => user_params}, _user, _claims) do
    with {:ok, %User{} = user} <- Accounts.register_user(user_params),
         {:ok, jwt} = generate_jwt(user) do
      conn
      |> put_status(:created)
      |> render("show.json", user: user, jwt: jwt)
    end
  end

  def current(conn, _params, user, _claims) do
    jwt = Guardian.Plug.current_token(conn)

    conn
    |> put_status(:ok)
    |> render("show.json", user: user, jwt: jwt)
  end
end
