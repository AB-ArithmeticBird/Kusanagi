defmodule KusanagiWeb.OrgAttrController do
  use KusanagiWeb, :controller
  use PhoenixSwagger

  alias Kusanagi.Organizations
  alias Kusanagi.Organizations.Projections.OrgAttributes

  action_fallback(KusanagiWeb.FallbackController)

  swagger_path :add_attr do
    post("/api/org/attribute")
    summary("Add an attribute to an Organization")
    description("Add an attribute to an Organization")

    parameters do
      tracker(
        :body,
        %Schema{
          type: :object,
          example: %{
            org_attribute: %{
              key: "attribute key",
              value: "attribute Value",
              org_uuid: "org_uuid"
            }
          }
        },
        "Add an attribute to an Organization",
        required: true
      )
    end

    response(200, "Ok", %{})
  end

  def add_attr(conn, %{"org_attribute" => %{"org_uuid" => org_uuid} = org_attribute_params}) do
    organization = Organizations.get_organization!(org_uuid)

    with {:ok, %OrgAttributes{} = org_attributes} <-
           Organizations.add_org_attribute(organization, org_attribute_params) do
      render(conn, "show.json", org_attr: org_attributes)
    end
  end

  swagger_path :index_attr do
    get("/api/org/attribute/{org_uuid}")
    summary("List all Organization Attributes")
    description("List all Organization Attributes")

    parameters do
      org_uuid(:path, :string, "Organization ID", required: true)
    end

    response(200, "Ok", %{})
  end

  def index_attr(conn, %{"org_uuid" => _org_uuid} = params) do
    attributes = Organizations.list_attributes(params)
    render(conn, "index.json", org_attributes: attributes)
  end

  swagger_path :show_attr do
    get("/api/org/attribute/{org_uuid}/{key}")
    summary("Get the Organization Attribute Value")
    description("Get the Organization Attribute Value")

    parameters do
      org_uuid(:path, :string, "Organization ID", required: true)
      key(:path, :string, "Attribute key", required: true)
    end

    response(200, "Ok", %{})
  end

  def show_attr(conn, %{"org_uuid" => _org_uuid} = params) do
    attributes = Organizations.get_attribute(params)
    render(conn, "index.json", org_attributes: attributes)
  end

  swagger_path :delete_attr do
    delete("/api/org/attribute/{org_uuid}/{key}")
    summary("Delete a Organization attribute")
    description("Delete a Organization attribute")

    parameters do
      org_uuid(:path, :string, "Organization ID", required: true)
      key(:path, :string, "Attribute key", required: true)
    end

    response(200, "Ok", %{})
  end

  def delete_attr(conn, %{"org_uuid" => org_uuid, "key" => _key} = params) do
    organization = Organizations.get_organization!(org_uuid)

    with :ok <- Organizations.delete_org_attribute(organization, params) do
      send_resp(conn, :no_content, "")
    end
  end

  swagger_path :update_attr do
    put("/api/org/attribute/update")
    summary("Update an attribute of an Organization")
    description("Update an attribute of an Organization")

    parameters do
      tracker(
        :body,
        %Schema{
          type: :object,
          example: %{
            org_attribute: %{
              key: "existing attribute key",
              value: "updated attribute Value",
              org_uuid: "org_uuid"
            }
          }
        },
        "Update an attribute of an Organization",
        required: true
      )
    end

    response(200, "Ok", %{})
  end

  def update_attr(conn, %{"org_attribute" => %{"org_uuid" => org_uuid} = org_attribute_params}) do
    organization = Organizations.get_organization!(org_uuid)

    with {:ok, %OrgAttributes{} = org_attributes} <-
           Organizations.update_org_attribute(organization, org_attribute_params) do
      render(conn, "show.json", org_attr: org_attributes)
    end
  end
end
