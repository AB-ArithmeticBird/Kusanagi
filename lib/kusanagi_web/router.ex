defmodule KusanagiWeb.Router do
  use KusanagiWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
    plug(Guardian.Plug.VerifyHeader, realm: "Token")
    plug(Guardian.Plug.LoadResource)
    plug(Plug.RequestId)
  end

  scope "/api", KusanagiWeb do
    pipe_through(:api)

    get("/user", UserController, :current)
    post("/users/login", SessionController, :create)
    post("/users", UserController, :create)
    # resources "/faccounts", FAccountController, except: [:new, :edit]

    post("/faccounts", FAccountController, :create)

    post("/faccounts/debit", FAccountController, :debit)
    post("/faccounts/credit", FAccountController, :credit)
    post("/faccounts/close", FAccountController, :close)
    post("/faccounts/migrate", FAccountController, :migrate)

    scope "/org" do
      post("/", OrganizationController, :create)
      get("/", OrganizationController, :index)
      get("/:uuid", OrganizationController, :show)
      put("/update/:uuid", OrganizationController, :update)
      delete("/:uuid", OrganizationController, :close)
      put("/status", OrganizationController, :status)
    end

    scope "/org/attribute" do
      get("/:org_uuid", OrgAttrController, :index_attr)
      put("/update", OrgAttrController, :update_attr)
      post("/", OrgAttrController, :add_attr)
      delete("/:org_uuid/:key", OrgAttrController, :delete_attr)
      get("/:org_uuid/:key", OrgAttrController, :show_attr)
    end

    scope "/branch" do
      # all Brances of an Org
      get("/org_id/:org_uuid", OrgBranchController, :index)
      # by uuid or identifier
      get("/:branch_id", OrgBranchController, :show)
      # by uuid or identifier
      put("/update", OrgBranchController, :update)
      put("/status", OrgBranchController, :status)
      post("/", OrgBranchController, :add)
      delete("/:uuid", OrgBranchController, :close)
    end

    scope "/branch/attribute" do
      get("/:b_uuid", OrgBranchController, :index_attr)
      put("/update", OrgBranchController, :update_attr)
      post("/", OrgBranchController, :add_attr)
      delete("/:branch_id/:key", OrgBranchController, :delete_attr)
    end

    get("/_version", GitVersionController, :index)
  end

  scope "/swagger" do
    forward("/", PhoenixSwagger.Plug.SwaggerUI,
      otp_app: :kusanagi,
      swagger_file: "swagger.json",
      disable_validator: true
    )
  end

  def swagger_info do
    %{
      info: %{
        version: "1.0",
        title: "Kusanagi"
      }
    }
  end
end
