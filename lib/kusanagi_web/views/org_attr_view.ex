defmodule KusanagiWeb.OrgAttrView do
  use KusanagiWeb, :view
  alias KusanagiWeb.OrgAttrView

  def render("index.json", %{org_attributes: org_attributes}) do
    %{data: render_many(org_attributes, OrgAttrView, "org_attribute.json")}
  end

  def render("show.json", %{org_attr: org_attr}) do
    %{data: render_one(org_attr, OrgAttrView, "org_attribute.json")}
  end

  def render("org_attribute.json", %{org_attr: org_attr}) do
    %{
      org_uuid: org_attr.org_uuid,
      key: org_attr.key,
      value: org_attr.value
    }
  end
end
