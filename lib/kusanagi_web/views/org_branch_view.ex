defmodule KusanagiWeb.OrgBranchView do
  use KusanagiWeb, :view
  alias KusanagiWeb.OrgBranchView

  def render("index.json", %{branches: branches}) do
    %{data: render_many(branches, OrgBranchView, "org_branch.json")}
  end

  def render("show.json", %{branch: branch}) do
    %{data: render_one(branch, OrgBranchView, "org_branch.json")}
  end

  def render("org_branch.json", %{org_branch: branch}) do
    %{
      uuid: branch.uuid,
      name: branch.name,
      address: branch.address,
      branch_type: branch.branch_type,
      identifier: branch.identifier,
      org_uuid: branch.org_uuid,
      status: branch.status
    }
  end

  def render("index.json", %{branch_attrs: branch_attrs}) do
    %{data: render_many(branch_attrs, OrgBranchView, "b_attribute.json")}
  end

  def render("show.json", %{branch_attr: branch_attr}) do
    %{data: render_one(branch_attr, OrgBranchView, "b_attribute.json")}
  end

  def render("b_attribute.json", %{org_branch: branch_attr}) do
    %{
      branch_uuid: branch_attr.branch_uuid,
      key: branch_attr.key,
      value: branch_attr.value
    }
  end
end
