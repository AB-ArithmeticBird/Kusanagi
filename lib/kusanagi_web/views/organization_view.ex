defmodule KusanagiWeb.OrganizationView do
  use KusanagiWeb, :view
  alias KusanagiWeb.OrganizationView

  def render("index.json", %{organizations: organizations}) do
    %{data: render_many(organizations, OrganizationView, "organization.json")}
  end

  def render("show.json", %{organization: organization}) do
    %{data: render_one(organization, OrganizationView, "organization.json")}
  end

  def render("organization.json", %{organization: organization}) do
    %{
      uuid: organization.uuid,
      name: organization.name,
      email: organization.email,
      phone_number: organization.phone_number,
      timeZone: organization.timeZone,
      address: organization.address,
      website: organization.website,
      status: organization.status
    }
  end
end
