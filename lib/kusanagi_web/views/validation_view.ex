defmodule KusanagiWeb.ValidationView do
  use KusanagiWeb, :view

  def render("error.json", %{errors: errors}) do
    %{errors: errors}
  end
end
