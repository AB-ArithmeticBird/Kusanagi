defmodule KusanagiWeb.FAccountView do
  use KusanagiWeb, :view
  alias KusanagiWeb.FAccountView

  def render("index.json", %{faccounts: faccounts}) do
    %{data: render_many(faccounts, FAccountView, "f_account.json")}
  end

  def render("show.json", %{f_account: f_account}) do
    %{data: render_one(f_account, FAccountView, "f_account.json")}
  end

  def render("f_account.json", %{f_account: f_account}) do
    %{
      id: f_account.uuid,
      account_type: f_account.account_type,
      account_name: f_account.account_name,
      description: f_account.description,
      balance: f_account.balance
    }
  end
end
