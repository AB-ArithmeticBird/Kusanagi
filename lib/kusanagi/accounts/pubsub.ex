defmodule Kusanagi.Accounts.Pubsub do
  alias Kusanagi.Accounts.Projections.User
  alias Kusanagi.Repo
  alias Phoenix.PubSub

  def wait_for(schema, uuid, version) do
    case Repo.get_by(schema, uuid: uuid, version: version) do
      nil -> subscribe_and_wait(schema, uuid, version)
      projection -> {:ok, projection}
    end
  end

  def publish_changes(%{user: %User{} = user}), do: publish(user)
  def publish_changes(%{user: {_, users}}) when is_list(users), do: Enum.each(users, &publish/1)
  def publish_changes(_changes), do: :ok

  defp publish(%User{uuid: uuid, version: version} = user) do
    PubSub.broadcast(Kusanagi.PubSub, "User:#{uuid}:#{version}", {User, user})
  end

  defp subscribe_and_wait(schema, uuid, version) do
    PubSub.subscribe(Kusanagi.PubSub, "User:#{uuid}:#{version}")

    receive do
      {^schema, projection} -> {:ok, projection}
    after
      10_000 ->
        {:error, :timeout}
    end
  end
end
