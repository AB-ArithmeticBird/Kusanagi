defmodule Kusanagi.Accounts.Aggregates.Lifespan.UserLifespan do
  @behaviour Commanded.Aggregates.AggregateLifespan

  alias Kusanagi.Accounts.Events.UserRegistered
  alias Kusanagi.Accounts.Commands.RegisterUser

  def after_event(%UserRegistered{}), do: :stop
  def after_command(%RegisterUser{}), do: :infinity
  def after_error(_error), do: :stop
end
