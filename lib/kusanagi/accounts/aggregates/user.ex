defmodule Kusanagi.Accounts.Aggregates.User do
  alias __MODULE__

  defstruct [
    :uuid,
    :username,
    :email,
    :hashed_password,
    :role
  ]

  alias Kusanagi.Accounts.Commands.RegisterUser
  alias Kusanagi.Accounts.Events.UserRegistered

  @doc """
  Register a User
  """
  def execute(%User{uuid: nil}, %RegisterUser{} = register) do
    %UserRegistered{
      user_uuid: register.user_uuid,
      username: register.username,
      email: register.email,
      role: register.role,
      hashed_password: register.hashed_password
    }
  end

  # state mutators

  def apply(%User{} = user, %UserRegistered{} = registered) do
    %User{
      user
      | uuid: registered.user_uuid,
        username: registered.username,
        email: registered.email,
        role: registered.role,
        hashed_password: registered.hashed_password
    }
  end
end
