defmodule Kusanagi.Accounts do
  require Logger

  @moduledoc """
  The boundary for the Accounts system.
  """
  defdelegate authorize(action, user, params), to: Kusanagi.AuthPolicy

  alias Kusanagi.Accounts.Commands.RegisterUser
  alias Kusanagi.Accounts.Queries.{UserByUsername, UserByEmail, UserByUsernameOrEmail}
  alias Kusanagi.Accounts.Projections.User
  alias Kusanagi.Repo
  alias Kusanagi.Router
  alias Kusanagi.Accounts.Pubsub, as: AccountPubSub

  @doc """
  Register a new user.
  """

  def register_user(attrs \\ %{}) do
    uuid = UUID.uuid4()

    register_user =
      attrs
      |> RegisterUser.new()
      |> RegisterUser.assign_uuid(uuid)
      |> RegisterUser.downcase_username()
      |> RegisterUser.downcase_email()
      |> RegisterUser.hash_password()

    with {:ok, version} <- Router.dispatch(register_user, include_aggregate_version: true) do
      AccountPubSub.wait_for(User, uuid, version)
    else
      reply -> reply
    end
  end

  @doc """
  Get an existing user by their username, or return `nil` if not registered
  """
  def user_by_username(username) when is_binary(username) do
    username
    |> String.downcase()
    |> UserByUsername.new()
    |> Repo.one()
  end

  @doc """
  Get an existing user by their email address, or return `nil` if not registered
  """
  def user_by_email(email) when is_binary(email) do
    email
    |> String.downcase()
    |> UserByEmail.new()
    |> Repo.one()
  end

  @doc """
  Get a single user by their UUID
  """
  def user_by_uuid(uuid) when is_binary(uuid) do
    Repo.get(User, uuid)
  end

  @doc """
  Get an existing user by their username or email, or return `nil` if not registered
  """
  def user_by_email_or_username(email, username) when is_binary(username) and is_binary(email) do
    username
    |> String.downcase()
    |> UserByUsernameOrEmail.new(String.downcase(email))
    |> Repo.all()
  end
end
