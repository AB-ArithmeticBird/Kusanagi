defmodule Kusanagi.Accounts.Events.UserRegistered do
  @derive [Poison.Encoder]
  defstruct [
    :user_uuid,
    :username,
    :email,
    :role,
    :hashed_password
  ]
end
