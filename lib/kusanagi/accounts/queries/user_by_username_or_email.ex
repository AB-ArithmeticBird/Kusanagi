defmodule Kusanagi.Accounts.Queries.UserByUsernameOrEmail do
  import Ecto.Query

  alias Kusanagi.Accounts.Projections.User

  def new(email, username) do
    from(u in User,
      where: u.email == ^email or u.username == ^username
    )
  end
end
