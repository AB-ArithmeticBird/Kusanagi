defmodule Kusanagi.Accounts.Queries.UserByUsername do
  import Ecto.Query

  alias Kusanagi.Accounts.Projections.User

  def new(username) do
    from(u in User,
      where: u.username == ^username
    )
  end
end
