defmodule Kusanagi.Accounts.Projectors.User do
  use Commanded.Projections.Ecto,
    name: "Accounts.Projectors.User"

  alias Kusanagi.Accounts.Events.UserRegistered
  alias Kusanagi.Accounts.Projections.User
  alias Kusanagi.Accounts.Pubsub, as: AccountPubSub

  project %UserRegistered{} = registered, %{stream_version: version} do
    Ecto.Multi.insert(multi, :user, %User{
      uuid: registered.user_uuid,
      username: registered.username,
      email: registered.email,
      version: version,
      hashed_password: registered.hashed_password,
      bio: nil,
      image: nil,
      role: registered.role
    })
  end

  def after_update(_event, _metadata, changes) do
    :timer.sleep(500)
    AccountPubSub.publish_changes(changes)
  end
end
