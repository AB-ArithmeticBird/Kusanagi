defmodule Kusanagi.Accounts.Routers.AccountRouter do
  use Commanded.Commands.Router

  alias Kusanagi.Accounts.Aggregates.User
  alias Kusanagi.Accounts.Commands.RegisterUser
  alias Kusanagi.Accounts.Aggregates.Lifespan.UserLifespan
  alias Kusanagi.Support.Middleware.{Uniqueness, Validate}
  alias Kusanagi.Support.Middleware.Authorizations.AccountAuth

  middleware(Commanded.Middleware.Auditing)
  middleware(Commanded.Middleware.Logger)
  middleware(Validate)
  middleware(Uniqueness)
  middleware(AccountAuth)

  dispatch([RegisterUser], lifespan: UserLifespan, to: User, identity: :user_uuid)
end
