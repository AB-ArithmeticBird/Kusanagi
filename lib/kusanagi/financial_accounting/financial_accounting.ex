defmodule Kusanagi.FinancialAccounting do
  @moduledoc """
  The FinancialAccounting context.
  """

  import Ecto.Query, warn: false

  alias Kusanagi.FinancialAccounting.Commands.{
    CreateFAccount,
    CreditFAccount,
    DebitFAccount,
    CloseFAccount,
    MigrateFAccount
  }

  alias Kusanagi.FinancialAccounting.Projections.FAccount
  alias Kusanagi.OrgBranch.Projections.OrgBranch
  alias Kusanagi.Repo
  alias Kusanagi.Router
  alias Kusanagi.FinancialAccounting.Pubsub, as: FAccountPubsub
  alias Kusanagi.OrgBranch.Enums.EnumsOrgBranch

  defdelegate authorize(action, user, params), to: Kusanagi.AuthPolicy

  @branch_active_status EnumsOrgBranch.org_branch_status()[:ACTIVE]

  @doc """
  Returns the list of faccounts.

  ## Examples

      iex> list_faccounts()
      [%FAccount{}, ...]

  """
  def list_faccounts do
    Repo.all(FAccount)
  end

  @doc """
  Gets a single f_account.

  Raises `Ecto.NoResultsError` if the F account does not exist.

  ## Examples

      iex> get_f_account!(123)
      %FAccount{}

      iex> get_f_account!(456)
      ** (Ecto.NoResultsError)

  """
  def get_f_account!(id), do: Repo.get!(FAccount, id)

  @doc """
  Creates a f_account.

  ## Examples

      iex> create_f_account(%{field: value})
      {:ok, %FAccount{}}

      iex> create_f_account(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_f_account(attrs \\ %{}) do
    uuid = UUID.uuid4()

    create_f_account =
      attrs
      |> CreateFAccount.new()
      |> CreateFAccount.assign_uuid(uuid)
      |> CreateFAccount.downcase_f_account_name()

    with {:ok, version} <- Router.dispatch(create_f_account, include_aggregate_version: true) do
      FAccountPubsub.wait_for(FAccount, uuid, version)
    else
      reply -> reply
    end
  end

  def credit_f_account(%{"uuid" => uuid, "amount" => amount} = attrs \\ %{}) do
    credit_f_account =
      attrs
      |> CreditFAccount.new()
      |> CreditFAccount.assign_uuid(uuid)
      |> CreditFAccount.assign_amount(amount)

    with {:ok, version} <- Router.dispatch(credit_f_account, include_aggregate_version: true) do
      FAccountPubsub.wait_for(FAccount, uuid, version)
    else
      reply -> reply
    end
  end

  def debit_f_account(%{"uuid" => uuid, "amount" => amount} = attrs \\ %{}) do
    debit_f_account =
      attrs
      |> DebitFAccount.new()
      |> DebitFAccount.assign_uuid(uuid)
      |> DebitFAccount.assign_amount(amount)

    with {:ok, version} <- Router.dispatch(debit_f_account, include_aggregate_version: true) do
      FAccountPubsub.wait_for(FAccount, uuid, version)
    else
      reply -> reply
    end
  end

  def close_f_account(%{"uuid" => uuid} = _attrs \\ %{}) do
    close_f_account = CloseFAccount.new(%{}) |> CloseFAccount.assign_uuid(uuid)

    with {:ok, version} <- Router.dispatch(close_f_account, include_aggregate_version: true) do
      FAccountPubsub.wait_for(FAccount, uuid, version)
    else
      reply -> reply
    end
  end

  # def migrate_f_account(_attrs, nil), do: {:error, :invalid_branch}
  #
  # def migrate_f_account(_attrs, %OrgBranch{status: status})
  #     when status != @branch_active_status,
  #     do: {:error, :branch_not_active}

  def migrate_f_account(
        %{
          "uuid" => uuid,
          "new_branch_id" => n_branch_id,
          "old_branch_id" => o_branch_id,
          "new_org_id" => n_org_id
        } = _attrs \\ %{}
      ) do
    migrate_f_account =
      MigrateFAccount.new(%{})
      |> MigrateFAccount.assign_uuid(uuid)
      |> MigrateFAccount.assign_o_branch_id(o_branch_id)
      |> MigrateFAccount.assign_n_branch_id(n_branch_id)
      |> MigrateFAccount.assign_n_org_id(n_org_id)

    with {:ok, version} <- Router.dispatch(migrate_f_account, include_aggregate_version: true) do
      FAccountPubsub.wait_for(FAccount, uuid, version)
    else
      reply -> reply
    end
  end
end
