defmodule Kusanagi.FinancialAccounting.Constants.Currency do
  alias __MODULE__

  defstruct [
    :code,
    :name,
    :symbol
  ]

  def getCurrencies do
    [
      {:USD, %Currency{code: "USD", name: "Dollar", symbol: "U+0024"}},
      {:INR, %Currency{code: "INR", name: "Rupees", symbol: "U+20B9"}},
      {:EUR, %Currency{code: "EUR", name: "Euro", symbol: "U+20AC"}},
      {:GBP, %Currency{code: "GBP", name: "Pound", symbol: "U+00A3"}},
      {:YEN, %Currency{code: "YEN", name: "Yen", symbol: "U+00A5"}}
    ]
  end
end
