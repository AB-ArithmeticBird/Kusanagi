defmodule Kusanagi.FinancialAccounting.Projections.FAccount do
  use Ecto.Schema

  @primary_key {:uuid, :binary_id, autogenerate: false}
  @foreign_key_type :binary_id

  schema "financial_accounts" do
    field(:version, :integer, default: 0)
    field(:account_type, :integer)
    field(:account_name, :string)
    field(:description, :string)
    field(:balance, :integer)
    field(:status, :integer)
    field(:currency, :string)
    # Not Linking to organization for now
    field(:org_id, :binary_id)
    # Not Linking to branch for now
    field(:branch_id, :binary_id)

    belongs_to(:user, Kusanagi.Accounts.Projections.User,
      foreign_key: :user_uuid,
      references: :uuid
    )

    timestamps()
  end
end
