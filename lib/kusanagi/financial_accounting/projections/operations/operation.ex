defmodule Kusanagi.FinancialAccounting.Projections.FAccount.Operations do
  alias Kusanagi.FinancialAccounting.Operation.Operation
  alias Kusanagi.FinancialAccounting.Projections.FAccount
  alias Kusanagi.FinancialAccounting.Enums.EnumsFAcoount

  defimpl Operation, for: FAccount do
    def credit(%FAccount{} = f_account, amount) do
      cond do
        EnumsFAcoount.account_type()[:liability] == f_account.account_type ->
          f_account.balance + amount

        EnumsFAcoount.account_type()[:contraliability] == f_account.account_type ->
          f_account.balance - amount

        EnumsFAcoount.account_type()[:asset] == f_account.account_type ->
          f_account.balance - amount

        EnumsFAcoount.account_type()[:contraasset] == f_account.account_type ->
          f_account.balance + amount

        EnumsFAcoount.account_type()[:expense] == f_account.account_type ->
          f_account.balance + amount

        EnumsFAcoount.account_type()[:revenue] == f_account.account_type ->
          f_account.balance + amount

        EnumsFAcoount.account_type()[:contrarevenue] == f_account.account_type ->
          f_account.balance - amount

        EnumsFAcoount.account_type()[:equity] == f_account.account_type ->
          f_account.balance + amount

        EnumsFAcoount.account_type()[:contraequity] == f_account.account_type ->
          f_account.balance - amount

        EnumsFAcoount.account_type()[:gain] == f_account.account_type ->
          f_account.balance + amount

        EnumsFAcoount.account_type()[:loss] == f_account.account_type ->
          f_account.balance - amount

        EnumsFAcoount.account_type()[:dividend] == f_account.account_type ->
          f_account.balance - amount
      end
    end

    def debit(%FAccount{} = f_account, amount) do
      cond do
        EnumsFAcoount.account_type()[:liability] == f_account.account_type ->
          f_account.balance - amount

        EnumsFAcoount.account_type()[:contraliability] == f_account.account_type ->
          f_account.balance + amount

        EnumsFAcoount.account_type()[:asset] == f_account.account_type ->
          f_account.balance + amount

        EnumsFAcoount.account_type()[:contraasset] == f_account.account_type ->
          f_account.balance - amount

        EnumsFAcoount.account_type()[:expense] == f_account.account_type ->
          f_account.balance - amount

        EnumsFAcoount.account_type()[:revenue] == f_account.account_type ->
          f_account.balance - amount

        EnumsFAcoount.account_type()[:contrarevenue] == f_account.account_type ->
          f_account.balance + amount

        EnumsFAcoount.account_type()[:equity] == f_account.account_type ->
          f_account.balance - amount

        EnumsFAcoount.account_type()[:contraequity] == f_account.account_type ->
          f_account.balance + amount

        EnumsFAcoount.account_type()[:gain] == f_account.account_type ->
          f_account.balance - amount

        EnumsFAcoount.account_type()[:loss] == f_account.account_type ->
          f_account.balance + amount

        EnumsFAcoount.account_type()[:dividend] == f_account.account_type ->
          f_account.balance + amount
      end
    end
  end
end
