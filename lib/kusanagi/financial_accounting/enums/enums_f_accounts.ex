defmodule Kusanagi.FinancialAccounting.Enums.EnumsFAcoount do
  import Kusanagi.Support.EnumsHelper

  enum "account_type" do
    %{
      liability: 1,
      contraliability: 2,
      asset: 3,
      contraasset: 4,
      expense: 5,
      revenue: 6,
      contrarevenue: 7,
      equity: 8,
      contraequity: 9,
      gain: 10,
      loss: 11,
      dividend: 12
    }
  end

  enum "account_status" do
    %{
      OPEN: 1,
      CLOSE: 2,
      OTHERS: 3
    }
  end
end
