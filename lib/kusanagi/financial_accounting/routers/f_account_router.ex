defmodule Kusanagi.FinancialAccounting.Routers.FAccountRouter do
  use Commanded.Commands.Router

  alias Kusanagi.FinancialAccounting.Aggregates.FAccount

  alias Kusanagi.FinancialAccounting.Commands.{
    CreateFAccount,
    CreditFAccount,
    DebitFAccount,
    CloseFAccount,
    MigrateFAccount
  }

  alias Kusanagi.FinancialAccounting.Aggregates.Lifespan.FAccountLifespan
  alias Kusanagi.Support.Middleware.{Uniqueness, Validate}
  alias Kusanagi.Support.Middleware.Authorizations.FAccountAuth

  middleware(Commanded.Middleware.Auditing)
  middleware(Commanded.Middleware.Logger)
  middleware(Validate)
  middleware(Uniqueness)
  middleware(FAccountAuth)

  identify(FAccount, by: :uuid)

  dispatch(
    [
      CreateFAccount,
      MigrateFAccount,
      CreditFAccount,
      DebitFAccount,
      CloseFAccount
    ],
    lifespan: FAccountLifespan,
    to: FAccount
  )
end
