defmodule Kusanagi.FinancialAccounting.Events.FAccountDebited do
  @moduledoc false
  @derive [Poison.Encoder]

  defstruct [
    :uuid,
    :amount
  ]
end
