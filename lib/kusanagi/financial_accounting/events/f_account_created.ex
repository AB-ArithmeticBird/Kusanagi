defmodule Kusanagi.FinancialAccounting.Events.FAccountCreated do
  @moduledoc false
  @derive [Poison.Encoder]

  defstruct [
    :uuid,
    :account_type,
    :account_name,
    :description,
    :user_uuid,
    :status,
    :currency,
    :org_id,
    :branch_id
  ]
end
