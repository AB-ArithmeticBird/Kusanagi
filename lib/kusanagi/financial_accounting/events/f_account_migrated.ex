defmodule Kusanagi.FinancialAccounting.Events.FAccountMigrated do
  @moduledoc false
  @derive [Poison.Encoder]

  defstruct [
    :uuid,
    :old_branch_id,
    :new_branch_id
  ]
end
