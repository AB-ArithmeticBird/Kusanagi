defmodule Kusanagi.FinancialAccounting.Events.FAccountCredited do
  @moduledoc false
  @derive [Poison.Encoder]

  defstruct [
    :uuid,
    :amount
  ]
end
