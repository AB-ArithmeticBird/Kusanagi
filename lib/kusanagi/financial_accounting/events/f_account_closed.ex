defmodule Kusanagi.FinancialAccounting.Events.FAccountClosed do
  @moduledoc false
  @derive [Poison.Encoder]

  defstruct [
    :uuid,
    :branch_id,
    :org_id
  ]
end
