defmodule Kusanagi.FinancialAccounting.Pubsub do
  alias Kusanagi.FinancialAccounting.Projections.FAccount
  alias Kusanagi.Repo
  alias Phoenix.PubSub

  def wait_for(schema, uuid, version) do
    case Repo.get_by(schema, uuid: uuid, version: version) do
      nil -> subscribe_and_wait(schema, uuid, version)
      projection -> {:ok, projection}
    end
  end

  def publish_changes(%{faccount: %FAccount{} = f_account}), do: publish(f_account)

  def publish_changes(%{faccount: {_, faccounts}}) when is_list(faccounts),
    do: Enum.each(faccounts, &publish/1)

  def publish_changes(_changes), do: :ok

  defp publish(%FAccount{uuid: uuid, version: version} = faccount) do
    PubSub.broadcast(Kusanagi.PubSub, "FAccount:#{uuid}:#{version}", {FAccount, faccount})
  end

  defp subscribe_and_wait(schema, uuid, version) do
    PubSub.subscribe(Kusanagi.PubSub, "FAccount:#{uuid}:#{version}")

    receive do
      {^schema, projection} -> {:ok, projection}
    after
      10_000 ->
        {:error, :timeout}
    end
  end
end
