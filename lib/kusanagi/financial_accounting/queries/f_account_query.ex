defmodule Kusanagi.FinancialAccounting.Queries.FAccountQuery do
  import Ecto.Query

  alias Kusanagi.FinancialAccounting.Projections.FAccount

  def get_all_account_by_branch_id_and_status(branch_id, status) do
    from(fa in FAccount,
      where: fa.branch_id == ^branch_id and fa.status == ^status
    )
  end
end
