defmodule Kusanagi.FinancialAccounting.ProcessManager.FAccountManager do
  use Commanded.ProcessManagers.ProcessManager,
    name: "FAccountManager",
    router: Kusanagi.Router
    # ,
    # event_timeout: :timer.minutes(5)

  alias __MODULE__

  @derive [Poison.Encoder]
  defstruct uuid: nil,
            org_id: nil,
            branch_id: nil

  alias Kusanagi.FinancialAccounting.Events.{FAccountCreated, FAccountClosed}
  alias Kusanagi.OrgBranch.Events.BranchClosureInitiated
  alias Kusanagi.FinancialAccounting.Commands.CloseFAccount

  # Stop process manager after three failures
  def error({:error, _failure}, _failed_command, %{context: %{failures: failures}})
      when failures >= 2 do
    # take Corrective Measures
    {:stop, :too_many_failures}
  end

  # Retry command, record failure count in context map
  def error({:error, _failure}, _failed_command, %{context: context}) do
    context = Map.update(context, :failures, 1, fn failures -> failures + 1 end)
    {:retry, context}
  end

  def interested?(%FAccountCreated{uuid: account_uuid}), do: {:start, account_uuid}
  def interested?(%BranchClosureInitiated{}), do: {:continue}
  def interested?(%FAccountClosed{uuid: account_uuid}), do: {:stop, account_uuid}
  def interested?(_event), do: false

  def handle(
        %FAccountManager{uuid: account_uuid, branch_id: branch_id},
        %BranchClosureInitiated{
          uuid: branch_uuid
        }
      )
      when branch_uuid == branch_id do
    %CloseFAccount{uuid: account_uuid}
  end

  def apply(%FAccountManager{} = f_account, %FAccountCreated{} = created) do
    %FAccountManager{
      f_account
      | uuid: created.uuid,
        org_id: created.org_id,
        branch_id: created.branch_id
    }
  end
end
