defmodule Kusanagi.FinancialAccounting.Operation do
  defprotocol Operation do
    def debit(data, amount)
    def credit(data, amount)
  end
end
