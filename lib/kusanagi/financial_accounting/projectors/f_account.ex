defmodule Kusanagi.FinancialAccounting.Projectors.FAccount do
  use Commanded.Projections.Ecto,
    name: "FinancialAccounting.Projectors.FAccount"

  alias Kusanagi.FinancialAccounting.Events.{
    FAccountCreated,
    FAccountCredited,
    FAccountDebited,
    FAccountClosed,
    FAccountMigrated
  }

  alias Kusanagi.FinancialAccounting.Projections.FAccount
  alias Ecto.Multi
  alias Kusanagi.Repo
  alias Ecto.Changeset
  alias Kusanagi.FinancialAccounting.Operation.Operation
  alias Kusanagi.FinancialAccounting.Pubsub, as: FAccountPubsub
  alias Kusanagi.FinancialAccounting.Enums.EnumsFAcoount

  project %FAccountCreated{} = created, %{stream_version: version} do
    Ecto.Multi.insert(multi, :faccount, %FAccount{
      uuid: created.uuid,
      account_type: created.account_type,
      account_name: created.account_name,
      version: version,
      description: created.description,
      balance: 0,
      currency: created.currency,
      user_uuid: created.user_uuid,
      org_id: created.org_id,
      branch_id: created.branch_id,
      status: 1
    })
  end

  project %FAccountCredited{} = credited, %{stream_version: version} do
    multi
    |> Multi.run(:account, fn _changes -> get_account(credited.uuid) end)
    |> Multi.run(:faccount, fn %{account: account} ->
      changeset =
        Changeset.cast(
          account,
          %{balance: Operation.credit(account, credited.amount), version: version},
          [:balance, :version]
        )

      Repo.update(changeset)
    end)
  end

  project %FAccountDebited{} = debited, %{stream_version: version} do
    multi
    |> Multi.run(:account, fn _changes -> get_account(debited.uuid) end)
    |> Multi.run(:faccount, fn %{account: account} ->
      changeset =
        Changeset.cast(
          account,
          %{balance: Operation.debit(account, debited.amount), version: version},
          [:balance, :version]
        )

      Repo.update(changeset)
    end)
  end

  project %FAccountClosed{} = closed, %{stream_version: version} do
    multi
    |> Multi.run(:account, fn _changes -> get_account(closed.uuid) end)
    |> Multi.run(:faccount, fn %{account: account} ->
      changeset =
        Changeset.cast(
          account,
          %{status: EnumsFAcoount.account_status()[:CLOSE], version: version},
          [:status, :version]
        )

      Repo.update(changeset)
    end)
  end

  project %FAccountMigrated{} = migrated, %{stream_version: version} do
    multi
    |> Multi.run(:account, fn _changes -> get_account(migrated.uuid) end)
    |> Multi.run(:faccount, fn %{account: account} ->
      changeset =
        Changeset.cast(
          account,
          %{branch_id: migrated.new_branch_id, version: version},
          [:branch_id, :version]
        )

      Repo.update(changeset)
    end)
  end

  defp get_account(uuid) do
    case Repo.get(FAccount, uuid) do
      nil ->
        {:error, :account_not_found}

      account ->
        {:ok, account}
    end
  end

  defp update_balance(multi, user_uuid, metadata, changes) do
    Ecto.Multi.update_all(
      multi,
      :user,
      user_query(user_uuid),
      [
        set: changes ++ [version: metadata.stream_version]
      ],
      returning: true
    )
  end

  def after_update(_event, _metadata, changes) do
    spawn(fn ->
      schedule(changes)
    end)

    :ok
  end

  defp schedule(changes) do
    :timer.sleep(500)
    FAccountPubsub.publish_changes(changes)
  end
end
