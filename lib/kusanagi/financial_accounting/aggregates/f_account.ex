defmodule Kusanagi.FinancialAccounting.Aggregates.FAccount do
  alias __MODULE__
  @moduledoc false
  defstruct [
    :uuid,
    :account_type,
    :account_name,
    :description,
    :user_uuid,
    :balance,
    :status,
    :currency,
    :branch_id,
    :org_id
  ]

  alias Kusanagi.FinancialAccounting.Commands.{
    CreateFAccount,
    CreditFAccount,
    DebitFAccount,
    CloseFAccount,
    MigrateFAccount
  }

  alias Kusanagi.FinancialAccounting.Events.{
    FAccountCreated,
    FAccountCredited,
    FAccountDebited,
    FAccountClosed,
    FAccountMigrated
  }

  alias Kusanagi.FinancialAccounting.Enums.EnumsFAcoount
  alias Kusanagi.FinancialAccounting.Operation.Operation

  @closed_f_account_status EnumsFAcoount.account_status()[:CLOSE]

  @doc """
  Create a Financial Account

  """

  def execute(%FAccount{uuid: nil} = _z, %CreateFAccount{} = create) do
    %FAccountCreated{
      uuid: create.uuid,
      account_type: create.account_type,
      account_name: create.account_name,
      description: create.description,
      user_uuid: create.user_uuid,
      currency: create.currency,
      status: EnumsFAcoount.account_status()[:OPEN],
      org_id: create.org_id,
      branch_id: create.branch_id
    }
  end

  @doc """
  Common code to filter out closed Account
  TODO: Keep track from PM what happens if account closed and some error happens while branch closure.
  May be required to reopen
  """
  def execute(%FAccount{status: @closed_f_account_status}, _command),
    do: {:error, :f_account_closed}

  # Credit
  def execute(%FAccount{uuid: nil} = _z, %CreditFAccount{}), do: {:error, :f_account_not_found}

  def execute(
        %FAccount{uuid: uuid, status: status} = z,
        %CreditFAccount{amount: amount} = _credit_f_account
      ) do
    cond do
      status != EnumsFAcoount.account_status()[:OPEN] ->
        {:error, :f_account_not_found}

      status == EnumsFAcoount.account_status()[:OPEN] ->
        new_balance = Operation.credit(z, amount)

        case new_balance < 0 do
          true ->
            {:error, :f_account_insufficient_balanace}

          _ ->
            %FAccountCredited{
              uuid: uuid,
              amount: amount
            }
        end
    end
  end

  # Debit
  def execute(%FAccount{uuid: nil} = _z, %DebitFAccount{}), do: {:error, :f_account_not_found}

  def execute(%FAccount{} = f_account, %DebitFAccount{amount: amount} = _debit_f_account) do
    cond do
      f_account.status != EnumsFAcoount.account_status()[:OPEN] ->
        {:error, :f_account_not_found}

      f_account.status == EnumsFAcoount.account_status()[:OPEN] ->
        new_balance = Operation.debit(f_account, amount)

        case new_balance < 0 do
          true ->
            {:error, :f_account_insufficient_balanace}

          _ ->
            %FAccountDebited{
              uuid: f_account.uuid,
              amount: amount
            }
        end
    end
  end

  # Close FAccount

  def execute(%FAccount{uuid: nil}, %CloseFAccount{}), do: {:error, :f_account_not_found}

  def execute(%FAccount{uuid: uuid, branch_id: branch_id, org_id: org_id}, %CloseFAccount{}) do
    %FAccountClosed{
      uuid: uuid,
      branch_id: branch_id,
      org_id: org_id
    }
  end

  # Migrate Account
  def execute(%FAccount{uuid: nil}, %MigrateFAccount{}), do: {:error, :f_account_not_found}

  def execute(%FAccount{} = f_account, %MigrateFAccount{} = migrate) do
    cond do
      f_account.branch_id != migrate.old_branch_id ->
        {:error, :branch_id_mismatch}

      f_account.org_id != migrate.new_org_id ->
        {:error, :invalid_new_branch}

      true ->
        %FAccountMigrated{
          uuid: f_account.uuid,
          old_branch_id: migrate.old_branch_id,
          new_branch_id: migrate.new_branch_id
        }
    end
  end

  # state mutators

  def apply(%FAccount{} = f_account, %FAccountCreated{} = created) do
    %FAccount{
      f_account
      | uuid: created.uuid,
        user_uuid: created.user_uuid,
        account_type: created.account_type,
        account_name: created.account_name,
        description: created.description,
        status: EnumsFAcoount.account_status()[:OPEN],
        balance: 0,
        currency: created.currency,
        org_id: created.org_id,
        branch_id: created.branch_id
    }
  end

  def apply(%FAccount{} = f_account, %FAccountCredited{amount: amount} = _f_account_credited) do
    new_balance = Operation.credit(f_account, amount)
    %FAccount{f_account | balance: new_balance}
  end

  def apply(%FAccount{} = f_account, %FAccountDebited{amount: amount} = _f_account_debited) do
    new_balance = Operation.debit(f_account, amount)
    %FAccount{f_account | balance: new_balance}
  end

  def apply(%FAccount{} = f_account, %FAccountClosed{}) do
    %FAccount{f_account | status: EnumsFAcoount.account_status()[:CLOSE]}
  end

  def apply(%FAccount{} = f_account, %FAccountMigrated{} = migrated) do
    %FAccount{f_account | branch_id: migrated.new_branch_id}
  end
end
