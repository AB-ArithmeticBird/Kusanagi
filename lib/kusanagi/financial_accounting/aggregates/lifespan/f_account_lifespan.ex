defmodule Kusanagi.FinancialAccounting.Aggregates.Lifespan.FAccountLifespan do
  @behaviour Commanded.Aggregates.AggregateLifespan

  alias Kusanagi.FinancialAccounting.Commands.{
    CreateFAccount
  }

  alias Kusanagi.FinancialAccounting.Events.{
    FAccountCreated,
    FAccountClosed
  }

  def after_event(%FAccountCreated{}), do: :infinity
  def after_event(%FAccountClosed{}), do: :stop
  def after_event(_event), do: 100_000

  def after_command(%CreateFAccount{}), do: :infinity
  def after_command(_command), do: 100_000

  def after_error(:f_account_not_found), do: :stop
  def after_error(_error), do: :infinity
end
