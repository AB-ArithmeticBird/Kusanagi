defmodule Kusanagi.FinancialAccounting.Commands.MigrateFAccount do
  alias __MODULE__

  defstruct [
    :uuid,
    :old_branch_id,
    :new_branch_id,
    :new_org_id
  ]

  use ExConstructor
  use Vex.Struct

  validates(:uuid, uuid: true)

  @doc """
  Assign a unique identity for the account
  """
  def assign_uuid(%MigrateFAccount{} = migrate_f_account, uuid) do
    %MigrateFAccount{migrate_f_account | uuid: uuid}
  end

  @doc """
  Assign old branch identity for the account
  """
  def assign_o_branch_id(%MigrateFAccount{} = migrate_f_account, old_branch_id) do
    %MigrateFAccount{migrate_f_account | old_branch_id: old_branch_id}
  end

  @doc """
  Assign new branch identity for the account
  """
  def assign_n_branch_id(%MigrateFAccount{} = migrate_f_account, new_branch_id) do
    %MigrateFAccount{migrate_f_account | new_branch_id: new_branch_id}
  end

  @doc """
  Assign new org identity for the account
  """
  def assign_n_org_id(%MigrateFAccount{} = migrate_f_account, new_org_id) do
    %MigrateFAccount{migrate_f_account | new_org_id: new_org_id}
  end
end
