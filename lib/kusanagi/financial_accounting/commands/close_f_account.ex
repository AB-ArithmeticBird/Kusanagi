defmodule Kusanagi.FinancialAccounting.Commands.CloseFAccount do
  alias __MODULE__
  @moduledoc false

  defstruct uuid: ""

  use ExConstructor
  use Vex.Struct

  validates(:uuid, uuid: true)

  @doc """
  Assign a unique identity for the account
  """
  def assign_uuid(%CloseFAccount{} = close_f_account, uuid) do
    %CloseFAccount{close_f_account | uuid: uuid}
  end
end
