defmodule Kusanagi.FinancialAccounting.Commands.CreditFAccount do
  alias __MODULE__

  defstruct [
    :uuid,
    :amount
  ]

  use ExConstructor
  use Vex.Struct

  validates(:uuid,
    presence: [message: "can't be empty"]
  )

  validates(:amount,
    presence: [message: "can't be empty"],
    amount: true
  )

  @doc """
  Assign a unique identity for the account
  """
  def assign_uuid(%CreditFAccount{} = credit_f_account, uuid) do
    %CreditFAccount{credit_f_account | uuid: uuid}
  end

  @doc """
  Assign an amount for the account
  """
  def assign_amount(%CreditFAccount{} = credit_f_account, amount) do
    %CreditFAccount{credit_f_account | amount: amount}
  end
end
