defmodule Kusanagi.FinancialAccounting.Commands.DebitFAccount do
  alias __MODULE__

  @moduledoc false

  defstruct [
    :uuid,
    :amount
  ]

  use ExConstructor
  use Vex.Struct

  validates(:uuid,
    presence: [message: "can't be empty"]
  )

  validates(:amount,
    presence: [message: "can't be empty"],
    amount: true
  )

  def assign_uuid(%DebitFAccount{} = debit_f_account, uuid) do
    %DebitFAccount{debit_f_account | uuid: uuid}
  end

  def assign_amount(%DebitFAccount{} = debit_f_account, amount) do
    %DebitFAccount{debit_f_account | amount: amount}
  end
end
