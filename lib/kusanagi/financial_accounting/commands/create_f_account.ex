defmodule Kusanagi.FinancialAccounting.Commands.CreateFAccount do
  alias __MODULE__
  @moduledoc false

  defstruct uuid: "",
            account_type: "",
            account_name: "",
            description: "",
            user_uuid: "",
            currency: "",
            org_id: "",
            branch_id: ""

  use ExConstructor
  use Vex.Struct

  validates(:uuid, uuid: true)

  validates(:account_type,
    presence: [message: "can't be empty"]
  )

  validates(:account_name,
    presence: [message: "can't be empty"],
    string: true,
    unique_username: false
  )

  validates(:currency,
    presence: [message: "can't be empty"],
    currency: true
  )

  validates(:org_id,
    presence: [message: "can't be empty"]
  )

  validates(:branch_id,
    presence: [message: "can't be empty"]
  )

  @doc """
  Assign a unique identity for the account
  """
  def assign_uuid(%CreateFAccount{} = create_f_account, uuid) do
    %CreateFAccount{create_f_account | uuid: uuid}
  end

  @doc """
  Convert account name to lowercase characters
  """
  def downcase_f_account_name(%CreateFAccount{account_name: account_name} = create_f_account) do
    %CreateFAccount{create_f_account | account_name: String.downcase(account_name)}
  end

  @doc """
  Assign the organization identity for the account
  """
  def assign_org_id(%CreateFAccount{} = create_f_account, org_id) do
    %CreateFAccount{create_f_account | org_id: org_id}
  end

  @doc """
  Assign the branch identity for the account
  """
  def assign_branch_id(%CreateFAccount{} = create_f_account, branch_id) do
    %CreateFAccount{create_f_account | branch_id: branch_id}
  end
end
