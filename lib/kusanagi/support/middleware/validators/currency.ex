defmodule Kusanagi.Support.Validators.Currency do
  use Vex.Validator

  import Kusanagi.FinancialAccounting.Constants.Currency

  def validate(value, _options) do
    Vex.Validators.By.validate(value,
      function: &valid_currency?/1,
      allow_nil: false,
      allow_blank: false
    )
  end

  defp valid_currency?(currency) do
    case Keyword.fetch(getCurrencies(), String.to_atom(currency)) do
      :error -> false
      _ -> true
    end
  end
end
