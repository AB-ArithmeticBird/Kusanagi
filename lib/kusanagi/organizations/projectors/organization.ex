defmodule Kusanagi.Organizations.Projectors.Organization do
  use Commanded.Projections.Ecto,
    name: "Organizations.Projectors.Organization"

  import Ecto.Query

  alias Kusanagi.Organizations.Events.{
    OrganizationCreated,
    OrgStatusUpdated,
    OrgClosureInitiated,
    OrgClosureFinished,
    OrganizationUpdated,
    OrgAttributeAdded,
    OrgAttributeDeleted,
    OrgAttributeUpdated
  }

  alias Kusanagi.Organizations.Projections.Organization
  alias Kusanagi.Organizations.Projections.OrgAttributes
  alias Ecto.Multi
  alias Kusanagi.Repo
  alias Kusanagi.Organizations.Enums.EnumsOrganization
  alias Kusanagi.Organizations.Pubsub, as: OrgPubSub
  alias Kusanagi.Organizations.Queries.GetOrgAttribute

  project %OrganizationCreated{} = created, %{stream_version: version} do
    Ecto.Multi.insert(multi, :organizations, %Organization{
      uuid: created.uuid,
      name: created.name,
      email: created.email,
      phone_number: created.phone_number,
      timeZone: created.timeZone,
      address: created.address,
      website: created.website,
      status: created.status,
      version: version
    })
  end

  project %OrgClosureInitiated{} = closed, %{stream_version: version} do
    multi
    |> update_organization(closed.uuid,
      status: EnumsOrganization.organization_status()[:CLOSING],
      version: version
    )
  end

  project %OrgClosureFinished{} = closed, %{stream_version: version} do
    multi
    |> update_organization(closed.uuid,
      status: EnumsOrganization.organization_status()[:CLOSED],
      version: version
    )
  end

  project %OrgStatusUpdated{} = status_updated, %{stream_version: version} do
    multi
    |> update_organization(status_updated.uuid, status: status_updated.status, version: version)
  end

  project %OrganizationUpdated{} = updated, %{stream_version: version} do
    multi
    |> update_organization(updated.uuid,
      version: version,
      name: updated.name,
      email: updated.email,
      phone_number: updated.phone_number,
      timeZone: updated.timeZone,
      address: updated.address,
      website: updated.website
    )
  end

  project %OrgAttributeAdded{} = added, %{stream_version: version} do
    multi
    |> Ecto.Multi.insert(:org_attributes, %OrgAttributes{
      org_uuid: added.uuid,
      key: added.key,
      value: added.value,
      version: version
    })
  end

  project %OrgAttributeDeleted{} = deleted do
    multi
    |> Multi.delete_all(:org_attributes, GetOrgAttribute.get_attribute(deleted.uuid, deleted.key))
  end

  project %OrgAttributeUpdated{} = updated, %{stream_version: version} do
    multi
    |> Multi.update_all(
      :org_attributes,
      GetOrgAttribute.get_attribute(updated.uuid, updated.key),
      [
        set: [
          value: updated.value,
          version: version
        ]
      ],
      returning: true
    )
  end

  defp update_organization(multi, org_uuid, changes) do
    # returning true so that after_update get called
    Ecto.Multi.update_all(multi, :organizations, org_query(org_uuid), [set: changes],
      returning: true
    )
  end

  defp org_query(org_uuid) do
    from(o in Organization, where: o.uuid == ^org_uuid)
  end

  def after_update(_event, _metadata, changes) do
    spawn(fn ->
      schedule(changes)
    end)

    :ok
  end

  # Write a seprate Genserver Process to schedule any thing after some time,
  defp schedule(changes) do
    Process.sleep(500)
    OrgPubSub.publish_changes(changes)
  end
end
