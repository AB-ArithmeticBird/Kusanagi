defmodule Kusanagi.Organizations.Queries.GetOrgAttribute do
  import Ecto.Query

  alias Kusanagi.Organizations.Projections.OrgAttributes

  def get_attribute(uuid, key) do
    from(oa in OrgAttributes,
      where: oa.org_uuid == ^uuid and oa.key == ^key
    )
  end

  def get_attribute(uuid, key, version) do
    from(oa in OrgAttributes,
      where: oa.org_uuid == ^uuid and oa.key == ^key and oa.version == ^version
    )
  end
end
