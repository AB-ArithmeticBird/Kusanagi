defmodule Kusanagi.Organizations.Queries.ListOrgAttributes do
  import Ecto.Query

  alias Kusanagi.Organizations.Projections.OrgAttributes

  def attributes(org_uuid) do
    from(oa in OrgAttributes,
      where: oa.org_uuid == ^org_uuid
    )
  end
end
