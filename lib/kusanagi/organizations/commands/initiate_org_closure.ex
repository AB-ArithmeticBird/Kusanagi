defmodule Kusanagi.Organizations.Commands.InitiateOrgClosure do
  alias __MODULE__

  defstruct [
    :uuid
  ]

  use ExConstructor
  use Vex.Struct

  validates(:uuid,
    presence: [message: "can't be empty"]
  )

  @doc """
  Assign a unique identity for the account
  """
  def assign_uuid(%InitiateOrgClosure{} = initiate_org_closure, uuid) do
    %InitiateOrgClosure{initiate_org_closure | uuid: uuid}
  end
end
