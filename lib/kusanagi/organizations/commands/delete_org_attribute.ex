defmodule Kusanagi.Organizations.Commands.DeleteOrgAttribute do
  alias __MODULE__

  defstruct [
    :org_uuid,
    :key
  ]

  use ExConstructor
  use Vex.Struct

  validates(:org_uuid,
    presence: [message: "can't be empty"]
  )

  validates(:key,
    presence: [message: "can't be empty"]
  )

  def assign_org_uuid(%DeleteOrgAttribute{} = delete_org_attribute, org_uuid) do
    %DeleteOrgAttribute{delete_org_attribute | org_uuid: org_uuid}
  end

  def assign_key(%DeleteOrgAttribute{} = delete_org_attribute, key) do
    %DeleteOrgAttribute{delete_org_attribute | key: key}
  end
end
