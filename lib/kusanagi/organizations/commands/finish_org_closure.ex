defmodule Kusanagi.Organizations.Commands.FinishOrgClosure do
  alias __MODULE__

  defstruct [
    :uuid
  ]

  use ExConstructor
  use Vex.Struct

  validates(:uuid,
    presence: [message: "can't be empty"]
  )

  @doc """
  Assign a unique identity for the account
  """
  def assign_uuid(%FinishOrgClosure{} = finish_org_closure, uuid) do
    %FinishOrgClosure{finish_org_closure | uuid: uuid}
  end
end
