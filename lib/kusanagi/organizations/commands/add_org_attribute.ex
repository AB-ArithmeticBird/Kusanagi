defmodule Kusanagi.Organizations.Commands.AddOrgAttribute do
  alias __MODULE__

  defstruct [
    :org_uuid,
    :key,
    :value
  ]

  use ExConstructor
  use Vex.Struct

  validates(:org_uuid,
    presence: [message: "can't be empty"]
  )

  validates(:key,
    presence: [message: "can't be empty"]
  )

  validates(:value,
    presence: [message: "can't be empty"]
  )

  def assign_org_uuid(%AddOrgAttribute{} = add_org_attribute, org_uuid) do
    %AddOrgAttribute{add_org_attribute | org_uuid: org_uuid}
  end
end
