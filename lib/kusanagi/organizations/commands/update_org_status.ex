defmodule Kusanagi.Organizations.Commands.UpdateOrgStatus do
  alias __MODULE__

  defstruct [
    :uuid,
    :status
  ]

  use ExConstructor
  use Vex.Struct

  validates(:uuid,
    presence: [message: "can't be empty"]
  )

  validates(:status,
    presence: [message: "can't be empty"],
    number: [message: "must be a valid status"]
  )

  @doc """
  Assign a unique identity for the account
  """
  def assign_uuid(%UpdateOrgStatus{} = update_organization_status, uuid) do
    %UpdateOrgStatus{update_organization_status | uuid: uuid}
  end

  def update_status(%UpdateOrgStatus{} = update_organization_status, status) do
    %UpdateOrgStatus{update_organization_status | status: status}
  end
end
