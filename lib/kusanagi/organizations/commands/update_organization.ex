defmodule Kusanagi.Organizations.Commands.UpdateOrganization do
  alias __MODULE__

  defstruct [
    :uuid,
    :name,
    :email,
    :website,
    :address,
    :phone_number,
    :timeZone,
    :status
  ]

  use ExConstructor
  use Vex.Struct

  validates(:uuid, uuid: true)

  validates(:name,
    presence: [message: "can't be empty"]
  )

  validates(:email,
    presence: [message: "can't be empty"]
  )

  validates(:address,
    presence: [message: "can't be empty"]
  )

  validates(:phone_number,
    presence: [message: "can't be empty"]
  )

  alias Kusanagi.Organizations.Projections.Organization

  @doc """
  Assign a unique identity for the account
  """
  def assign_uuid(%UpdateOrganization{} = update_organization, uuid) do
    %UpdateOrganization{update_organization | uuid: uuid}
  end

  def update_name(%UpdateOrganization{} = update_organization, %Organization{name: name}) do
    Map.update(update_organization, :name, name, & &1)
  end

  def update_email(%UpdateOrganization{} = update_organization, %Organization{email: email}) do
    Map.update(update_organization, :email, email, & &1)
  end

  def update_website(%UpdateOrganization{} = update_organization, %Organization{website: website}) do
    Map.update(update_organization, :website, website, & &1)
  end

  def update_address(%UpdateOrganization{} = update_organization, %Organization{address: address}) do
    Map.update(update_organization, :address, address, & &1)
  end

  def update_phone_number(%UpdateOrganization{} = update_organization, %Organization{
        phone_number: phone_number
      }) do
    Map.update(update_organization, :phone_number, phone_number, & &1)
  end

  def update_timeZone(%UpdateOrganization{} = update_organization, %Organization{
        timeZone: timeZone
      }) do
    Map.update(update_organization, :timeZone, timeZone, & &1)
  end
end
