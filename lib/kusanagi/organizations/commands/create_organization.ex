defmodule Kusanagi.Organizations.Commands.CreateOrganization do
  alias __MODULE__

  defstruct uuid: "",
            name: "",
            email: "",
            website: "",
            address: "",
            phone_number: "",
            timeZone: ""

  use ExConstructor
  use Vex.Struct

  validates(:uuid, uuid: true)

  validates(:name,
    presence: [message: "can't be empty"]
  )

  validates(:email,
    presence: [message: "can't be empty"]
  )

  validates(:address,
    presence: [message: "can't be empty"]
  )

  validates(:phone_number,
    presence: [message: "can't be empty"]
  )

  @doc """
  Assign a unique identity for the account
  """
  def assign_uuid(%CreateOrganization{} = create_organization, uuid) do
    %CreateOrganization{create_organization | uuid: uuid}
  end
end
