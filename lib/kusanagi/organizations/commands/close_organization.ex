defmodule Kusanagi.Organizations.Commands.CloseOrganization do
  alias __MODULE__

  defstruct [
    :uuid
  ]

  use ExConstructor
  use Vex.Struct

  validates(:uuid,
    presence: [message: "can't be empty"]
  )

  @doc """
  Assign a unique identity for the account
  """
  def assign_uuid(%CloseOrganization{} = close_organization, uuid) do
    %CloseOrganization{close_organization | uuid: uuid}
  end
end
