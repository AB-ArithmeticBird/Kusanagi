defmodule Kusanagi.Organizations.Events.OrgClosureInitiated do
  @derive [Poison.Encoder]

  defstruct [
    :uuid
  ]
end
