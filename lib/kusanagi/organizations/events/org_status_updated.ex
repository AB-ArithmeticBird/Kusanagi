defmodule Kusanagi.Organizations.Events.OrgStatusUpdated do
  @derive [Poison.Encoder]

  defstruct [
    :uuid,
    :status
  ]
end
