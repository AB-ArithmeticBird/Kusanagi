defmodule Kusanagi.Organizations.Events.OrganizationCreated do
  @derive [Poison.Encoder]

  defstruct [
    :uuid,
    :name,
    :email,
    :phone_number,
    :status,
    :website,
    :address,
    :timeZone
  ]
end
