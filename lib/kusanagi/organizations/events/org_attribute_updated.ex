defmodule Kusanagi.Organizations.Events.OrgAttributeUpdated do
  @derive [Poison.Encoder]

  defstruct [
    :uuid,
    :key,
    :value
  ]
end
