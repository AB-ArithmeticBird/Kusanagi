defmodule Kusanagi.Organizations.Events.OrgAttributeDeleted do
  @derive [Poison.Encoder]

  defstruct [
    :uuid,
    :key
  ]
end
