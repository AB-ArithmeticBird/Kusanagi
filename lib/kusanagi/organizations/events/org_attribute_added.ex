defmodule Kusanagi.Organizations.Events.OrgAttributeAdded do
  @derive [Poison.Encoder]

  defstruct [
    :uuid,
    :key,
    :value
  ]
end
