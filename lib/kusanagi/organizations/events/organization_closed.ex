defmodule Kusanagi.Organizations.Events.OrganizationClosed do
  @derive [Poison.Encoder]

  defstruct [
    :uuid
  ]
end
