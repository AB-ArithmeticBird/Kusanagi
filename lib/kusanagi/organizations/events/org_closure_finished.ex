defmodule Kusanagi.Organizations.Events.OrgClosureFinished do
  @derive [Poison.Encoder]

  defstruct [
    :uuid
  ]
end
