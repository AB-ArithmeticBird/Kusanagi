defmodule Kusanagi.Organizations.Events.OrganizationUpdated do
  @derive [Poison.Encoder]

  defstruct [
    :uuid,
    :name,
    :email,
    :phone_number,
    :website,
    :address,
    :timeZone
  ]
end
