defmodule Kusanagi.Organizations.Pubsub do
  alias Kusanagi.Organizations.Projections.Organization
  alias Kusanagi.Organizations.Projections.OrgAttributes
  alias Kusanagi.Repo
  alias Phoenix.PubSub

  def wait_for(schema, uuid, version) do
    case Repo.get_by(schema, uuid: uuid, version: version) do
      nil -> subscribe_and_wait(schema, uuid, version)
      projection -> {:ok, projection}
    end
  end

  def wait_for(schema, uuid, identifier, query) do
    case Repo.one(query) do
      nil ->
        subscribe_and_wait(schema, uuid, identifier)

      projection ->
        {:ok, projection}
    end
  end

  # Ordeirng of pattern mathc is Important
  def publish_changes(%{org_attributes: %OrgAttributes{} = org_attributes}),
    do: publish(org_attributes)

  def publish_changes(%{org_attributes: {_, org_attributes}}) when is_list(org_attributes),
    do: Enum.each(org_attributes, &publish/1)

  def publish_changes(%{organizations: %Organization{} = organization}), do: publish(organization)

  def publish_changes(%{organizations: {_, organization}}) when is_list(organization),
    do: Enum.each(organization, &publish/1)

  def publish_changes(_changes), do: :ok

  defp publish(%Organization{uuid: uuid, version: version} = organization) do
    PubSub.broadcast(
      Kusanagi.PubSub,
      "Organization:#{uuid}:#{version}",
      {Organization, organization}
    )
  end

  defp publish(%OrgAttributes{org_uuid: org_uuid, key: key, version: version} = org_attributes) do
    PubSub.broadcast(
      Kusanagi.PubSub,
      "OrgAttributes:#{org_uuid}:#{key}_#{version}",
      {OrgAttributes, org_attributes}
    )
  end

  defp subscribe_and_wait(schema, org_uuid, extra_identifier) do
    PubSub.subscribe(
      Kusanagi.PubSub,
      "#{getSchemaString(schema)}:#{org_uuid}:#{extra_identifier}"
    )

    receive do
      {^schema, projection} ->
        {:ok, projection}
    after
      20_000 ->
        {:error, :timeout}
    end
  end

  defp getSchemaString(Organization) do
    "Organization"
  end

  defp getSchemaString(OrgAttributes) do
    "OrgAttributes"
  end
end
