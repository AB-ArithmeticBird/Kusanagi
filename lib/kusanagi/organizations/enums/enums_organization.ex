defmodule Kusanagi.Organizations.Enums.EnumsOrganization do
  import Kusanagi.Support.EnumsHelper

  enum "organization_status" do
    %{
      CREATED: 1,
      VERIFIED: 2,
      ACTIVE: 3,
      INACTIVE: 4,
      CLOSED: 5,
      CLOSING: 6
    }
  end
end
