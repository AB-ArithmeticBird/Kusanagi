defmodule Kusanagi.Organizations.ProcessManagers.OrgManager do
  use Commanded.ProcessManagers.ProcessManager,
    name: "OrgManager",
    router: Kusanagi.Router
    # ,
    # event_timeout: :timer.minutes(5)

  alias __MODULE__

  alias Kusanagi.OrgBranch.Events.{BranchCreated, BranchClosureFinished}
  alias Kusanagi.Organizations.Commands.{FinishOrgClosure}

  alias Kusanagi.Organizations.Events.{
    OrganizationCreated,
    OrgClosureFinished,
    OrgClosureInitiated
  }

  @derive [Poison.Encoder]
  defstruct org_id: nil,
            no_of_branch_active: 0,
            closed_branches: [],
            active_branches: []

  def interested?(%OrganizationCreated{uuid: org_id}), do: {:start, org_id}
  def interested?(%BranchCreated{org_uuid: org_id}), do: {:continue, org_id}
  def interested?(%OrgClosureInitiated{uuid: org_id}), do: {:continue, org_id}
  def interested?(%BranchClosureFinished{org_uuid: org_id}), do: {:continue, org_id}
  def interested?(%OrgClosureFinished{uuid: org_id}), do: {:stop, org_id}
  def interested?(_event), do: false

  def handle(%OrgManager{org_id: org_id, no_of_branch_active: active}, %BranchClosureFinished{
        org_uuid: org_uuid
      })
      when org_id == org_uuid and active == 1 do
    %FinishOrgClosure{uuid: org_id}
  end

  def handle(%OrgManager{org_id: org_id, no_of_branch_active: active}, %OrgClosureInitiated{
        uuid: org_uuid
      })
      when org_id == org_uuid and active == 0 do
    %FinishOrgClosure{uuid: org_id}
  end

  def apply(%OrgManager{} = org_pm, %OrganizationCreated{} = created) do
    %OrgManager{org_pm | org_id: created.uuid}
  end

  def apply(
        %OrgManager{
          org_id: org_id,
          no_of_branch_active: active,
          active_branches: active_branches
        } = org_pm,
        %BranchCreated{org_uuid: org_uuid, uuid: branch_id}
      )
      when org_id == org_uuid do
    %OrgManager{
      org_pm
      | no_of_branch_active: active + 1,
        # MapSet.put(active_branches, branch_id)
        active_branches: [branch_id | active_branches]
    }
  end

  def apply(
        %OrgManager{
          org_id: org_id,
          no_of_branch_active: active,
          active_branches: active_branches,
          closed_branches: closed_branches
        } = org_pm,
        %BranchClosureFinished{org_uuid: org_uuid, uuid: branch_id}
      )
      when org_id == org_uuid and active > 1 do
    %OrgManager{
      org_pm
      | no_of_branch_active: active - 1,
        active_branches: List.delete(active_branches, branch_id),
        # MapSet.put(closed_branches, branch_id)
        closed_branches: [branch_id | closed_branches]
    }
  end

  # Stop process manager after three failures
  def error({:error, _failure}, _failed_command, %{context: %{failures: failures}})
      when failures >= 2 do
    # take Corrective Measures
    {:stop, :too_many_failures}
  end

  # Retry command, record failure count in context map
  def error({:error, _failure}, _failed_command, %{context: context}) do
    context = Map.update(context, :failures, 1, fn failures -> failures + 1 end)
    {:retry, context}
  end
end
