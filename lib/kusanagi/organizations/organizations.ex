defmodule Kusanagi.Organizations do
  @moduledoc """
  The Organizations context.
  """

  import Ecto.Query, warn: false
  alias Kusanagi.Repo

  alias Kusanagi.Organizations.Projections.Organization
  alias Kusanagi.Organizations.Projections.OrgAttributes

  alias Kusanagi.Organizations.Commands.{
    CreateOrganization,
    InitiateOrgClosure,
    UpdateOrgStatus,
    UpdateOrganization,
    AddOrgAttribute,
    DeleteOrgAttribute,
    UpdateOrgAttribute
  }

  alias Kusanagi.Router
  alias Kusanagi.Organizations.Pubsub, as: OrgPubsub
  alias Kusanagi.Organizations.Enums.EnumsOrganization
  alias Kusanagi.Organizations.Queries.{ListOrgAttributes, GetOrgAttribute}

  @doc """
  Returns the list of organizations.

  ## Examples

      iex> list_organizations()
      [%Organization{}, ...]

  """
  def list_organizations do
    Repo.all(Organization)
  end

  @doc """
  Gets a single organization.

  Raises `Ecto.NoResultsError` if the Organization does not exist.

  ## Examples

      iex> get_organization!(123)
      %Organization{}

      iex> get_organization!(456)
      ** (Ecto.NoResultsError)

  """
  def get_organization!(uuid), do: Repo.get!(Organization, uuid)

  @doc """
  Creates a organization.

  ## Examples

      iex> create_organization(%{field: value})
      {:ok, %Organization{}}

      iex> create_organization(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_organization(attrs \\ %{}) do
    uuid = UUID.uuid4()

    create_organization =
      attrs
      |> CreateOrganization.new()
      |> CreateOrganization.assign_uuid(uuid)

    with {:ok, version} <- Router.dispatch(create_organization, include_aggregate_version: true) do
      OrgPubsub.wait_for(Organization, uuid, version)
    else
      reply -> reply
    end
  end

  @doc """
  Updates an organization.
  Organization can be updated in any status Except Closed/InActive.

  ## Examples

      iex> update_organization(organization, %{field: new_value})
      {:ok, %Organization{}}

      iex> update_organization(organization, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_organization(%Organization{uuid: uuid} = organization, attrs) do
    update_organization =
      attrs
      |> UpdateOrganization.new()
      |> UpdateOrganization.assign_uuid(uuid)
      |> UpdateOrganization.update_name(organization)
      |> UpdateOrganization.update_email(organization)
      |> UpdateOrganization.update_website(organization)
      |> UpdateOrganization.update_address(organization)
      |> UpdateOrganization.update_phone_number(organization)
      |> UpdateOrganization.update_timeZone(organization)

    with {:ok, version} <- Router.dispatch(update_organization, include_aggregate_version: true) do
      OrgPubsub.wait_for(Organization, uuid, version)
    else
      reply -> reply
    end
  end

  @doc """
  Closes an Organization.
  Organization can be closed Only if status is Active/InActive.

  ## Examples

      iex> close_organization(organization)
      {:ok, %Organization{}}

      iex> close_organization(organization)
      {:error, %Ecto.Changeset{}}

  """
  def close_organization(%Organization{uuid: uuid}) do
    close_organization =
      InitiateOrgClosure.new(%{})
      |> InitiateOrgClosure.assign_uuid(uuid)

    with {:ok, version} <- Router.dispatch(close_organization, include_aggregate_version: true) do
      OrgPubsub.wait_for(Organization, uuid, version)
    else
      reply -> reply
    end
  end

  @doc """
  Update Status of a Organization.
  Valid Status Transitions are
      Created -> Verified
      Verified -> Active
      Active -> InActive
      InActive -> Active

  ## Examples

      iex> update_status(organization,attrs)
      {:ok, %Organization{}}

      iex> delete_organization(organization,attrs)
      {:error, %Ecto.Changeset{}}

  """
  def update_status(%Organization{uuid: uuid}, %{"status" => status}) do
    update_org_status =
      UpdateOrgStatus.new(%{})
      |> UpdateOrgStatus.assign_uuid(uuid)
      |> UpdateOrgStatus.update_status(
        EnumsOrganization.organization_status()[String.to_atom(status)]
      )

    with {:ok, version} <- Router.dispatch(update_org_status, include_aggregate_version: true) do
      OrgPubsub.wait_for(Organization, uuid, version)
    else
      reply -> reply
    end
  end

  ## Organization Attributes Apis
  def add_org_attribute(%Organization{uuid: org_uuid} = _organization, attrs) do
    add_org_attribute =
      attrs
      |> AddOrgAttribute.new()
      |> AddOrgAttribute.assign_org_uuid(org_uuid)

    with {:ok, version} <- Router.dispatch(add_org_attribute, include_aggregate_version: true) do
      OrgPubsub.wait_for(
        OrgAttributes,
        org_uuid,
        "#{add_org_attribute.key}_#{version}",
        GetOrgAttribute.get_attribute(org_uuid, add_org_attribute.key, version)
      )
    else
      reply -> reply
    end
  end

  def update_org_attribute(%Organization{uuid: org_uuid} = _organization, attrs) do
    update_org_attribute =
      attrs
      |> UpdateOrgAttribute.new()
      |> UpdateOrgAttribute.assign_org_uuid(org_uuid)

    with {:ok, version} <- Router.dispatch(update_org_attribute, include_aggregate_version: true) do
      OrgPubsub.wait_for(
        OrgAttributes,
        org_uuid,
        "#{update_org_attribute.key}_#{version}",
        GetOrgAttribute.get_attribute(org_uuid, update_org_attribute.key, version)
      )
    else
      reply -> reply
    end
  end

  def delete_org_attribute(%Organization{uuid: org_uuid} = _organization, attrs) do
    delete_org_attribute =
      attrs
      |> DeleteOrgAttribute.new()
      |> DeleteOrgAttribute.assign_org_uuid(org_uuid)

    Router.dispatch(delete_org_attribute)
  end

  def list_attributes(%{"org_uuid" => org_uuid}) do
    org_uuid
    |> ListOrgAttributes.attributes()
    |> Repo.all()
  end

  def get_attribute(%{"org_uuid" => org_uuid, "key" => key}) do
    org_uuid
    |> GetOrgAttribute.get_attribute(key)
    |> Repo.all()
  end
end
