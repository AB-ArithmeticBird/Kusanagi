defmodule Kusanagi.Organizations.Projections.Organization do
  use Ecto.Schema

  @primary_key {:uuid, :binary_id, autogenerate: false}

  schema "organizations" do
    field(:version, :integer, default: 0)
    field(:address, :string)
    field(:website, :string)
    field(:email, :string)
    field(:name, :string)
    field(:phone_number, :string)
    field(:timeZone, :string)
    field(:status, :integer)

    timestamps()
  end
end
