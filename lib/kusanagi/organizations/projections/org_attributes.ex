defmodule Kusanagi.Organizations.Projections.OrgAttributes do
  use Ecto.Schema

  schema "org_attributes" do
    field(:key, :string)
    field(:value, :string)
    field(:org_uuid, :binary_id)
    field(:version, :integer, default: 0)

    timestamps()
  end
end
