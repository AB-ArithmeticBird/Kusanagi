defmodule Kusanagi.Organizations.Aggregates.Organization do
  alias __MODULE__

  @moduledoc """
    Organization Aggregates
  """
  defstruct uuid: nil,
            name: nil,
            email: nil,
            phone_number: nil,
            timeZone: nil,
            address: nil,
            status: nil,
            website: nil,
            attributes: Map.new()

  alias Kusanagi.Organizations.Commands.{
    CreateOrganization,
    UpdateOrgStatus,
    UpdateOrganization,
    AddOrgAttribute,
    DeleteOrgAttribute,
    UpdateOrgAttribute,
    InitiateOrgClosure,
    FinishOrgClosure
  }

  alias Kusanagi.Organizations.Events.{
    OrganizationCreated,
    OrgStatusUpdated,
    OrganizationUpdated,
    OrgAttributeAdded,
    OrgAttributeDeleted,
    OrgAttributeUpdated,
    OrgClosureInitiated,
    OrgClosureFinished
  }

  alias Kusanagi.Organizations.Enums.EnumsOrganization

  @org_active_status EnumsOrganization.organization_status()[:ACTIVE]
  @org_inactive_status EnumsOrganization.organization_status()[:INACTIVE]
  @org_verified_status EnumsOrganization.organization_status()[:VERIFIED]
  @org_created_status EnumsOrganization.organization_status()[:CREATED]
  @org_closed_status EnumsOrganization.organization_status()[:CLOSED]
  @org_closing_status EnumsOrganization.organization_status()[:CLOSING]

  def execute(
        %Organization{uuid: uuid} = _z,
        %FinishOrgClosure{}
      ) do
    %OrgClosureFinished{
      uuid: uuid
    }
  end

  @doc """
    Common Code for Filtering Out Closed Branch, No Action can be taken on Closed Branch
  """

  def execute(%Organization{status: status} = _z, _command)
      when status == @org_closed_status or status == @org_closing_status do
    {:error, :organization_closed}
  end

  @doc """
    Creates a Organization
  """
  def execute(%Organization{uuid: nil} = _z, %CreateOrganization{} = create) do
    %OrganizationCreated{
      uuid: create.uuid,
      name: create.name,
      email: create.email,
      phone_number: create.phone_number,
      timeZone: create.timeZone,
      address: create.address,
      website: create.website,
      status: EnumsOrganization.organization_status()[:CREATED]
    }
  end

  @doc """
    Update Org Status:
    Valid Transitions :
      Created -> Verified
      Verified -> Active
      Active -> InActive
      InActive -> Active
  """
  def execute(%Organization{uuid: nil}, %UpdateOrgStatus{}), do: {:error, :org_not_found}

  def execute(
        %Organization{status: @org_created_status},
        %UpdateOrgStatus{status: status}
      )
      when status != @org_verified_status,
      do: {:error, :org_not_verified}

  def execute(
        %Organization{status: @org_verified_status},
        %UpdateOrgStatus{status: status}
      )
      when status != @org_active_status,
      do: {:error, :org_not_active}

  def execute(
        %Organization{status: @org_active_status},
        %UpdateOrgStatus{status: status}
      )
      when status != @org_inactive_status,
      do: {:error, :status_update_not_allowed}

  def execute(
        %Organization{status: @org_inactive_status},
        %UpdateOrgStatus{status: status}
      )
      when status != @org_active_status,
      do: {:error, :status_update_not_allowed}

  def execute(
        %Organization{uuid: uuid},
        %UpdateOrgStatus{} = update_status
      ),
      do: %OrgStatusUpdated{
        uuid: uuid,
        status: update_status.status
      }

  @doc """
    Close Organization:
      1. Close All Branch.
      2. Close all Account
      3. Close Organization
      TODO: Use EventManager to Change
  """
  def execute(%Organization{uuid: nil}, %InitiateOrgClosure{}),
    do: {:error, :organization_not_found}

  def execute(%Organization{uuid: uuid, status: status}, %InitiateOrgClosure{})
      when status == @org_active_status or status == @org_inactive_status do
    %OrgClosureInitiated{
      uuid: uuid
    }
  end

  def execute(%Organization{}, %InitiateOrgClosure{}), do: {:error, :org_not_found}

  def execute(%Organization{uuid: nil}, %FinishOrgClosure{}),
    do: {:error, :organization_not_found}

  @doc """
    Common Code for Filtering Out Inactive Organization, No Action can be taken on InActive Organization
    Instead of Reactivating it (Handled Above). Special Case
  """
  def execute(%Organization{status: @org_inactive_status} = _z, _command) do
    {:error, :org_not_active}
  end

  def execute(%Organization{uuid: nil} = _z, %UpdateOrganization{}),
    do: {:error, :org_not_found}

  def execute(%Organization{uuid: uuid} = _z, %UpdateOrganization{} = update) do
    %OrganizationUpdated{
      uuid: uuid,
      name: update.name,
      email: update.email,
      phone_number: update.phone_number,
      timeZone: update.timeZone,
      address: update.address,
      website: update.website
    }
  end

  def execute(%Organization{uuid: nil} = _z, %AddOrgAttribute{}),
    do: {:error, :org_not_found}

  def execute(%Organization{uuid: uuid} = z, %AddOrgAttribute{} = add) do
    case Map.get(z.attributes, add.key) do
      nil ->
        %OrgAttributeAdded{
          uuid: uuid,
          key: add.key,
          value: add.value
        }

      _ ->
        {:error, :key_already_exists}
    end
  end

  def execute(%Organization{uuid: nil} = _z, %DeleteOrgAttribute{}),
    do: {:error, :org_not_found}

  def execute(%Organization{uuid: uuid} = _z, %DeleteOrgAttribute{} = delete) do
    %OrgAttributeDeleted{
      uuid: uuid,
      key: delete.key
    }
  end

  def execute(%Organization{uuid: nil} = _z, %UpdateOrgAttribute{}),
    do: {:error, :org_not_found}

  def execute(%Organization{uuid: uuid} = z, %UpdateOrgAttribute{} = update) do
    case Map.get(z.attributes, update.key) do
      nil ->
        {:error, :key_not_exists}

      _ ->
        %OrgAttributeUpdated{
          uuid: uuid,
          key: update.key,
          value: update.value
        }
    end
  end

  def apply(%Organization{} = organization, %OrganizationCreated{} = created) do
    %Organization{
      organization
      | uuid: created.uuid,
        name: created.name,
        email: created.email,
        phone_number: created.phone_number,
        timeZone: created.timeZone,
        address: created.address,
        website: created.website,
        status: created.status
    }
  end

  def apply(%Organization{} = organization, %OrgClosureInitiated{}) do
    %Organization{organization | status: EnumsOrganization.organization_status()[:CLOSING]}
  end

  def apply(%Organization{} = organization, %OrgClosureFinished{}) do
    %Organization{organization | status: EnumsOrganization.organization_status()[:CLOSED]}
  end

  def apply(%Organization{} = organization, %OrgStatusUpdated{} = status_updated) do
    %Organization{organization | status: status_updated.status}
  end

  def apply(%Organization{} = organization, %OrganizationUpdated{} = updated) do
    %Organization{
      organization
      | name: updated.name,
        email: updated.email,
        phone_number: updated.phone_number,
        timeZone: updated.timeZone,
        address: updated.address,
        website: updated.website
    }
  end

  def apply(%Organization{} = organization, %OrgAttributeAdded{} = added) do
    %Organization{
      organization
      | attributes:
          organization.attributes |> Map.update(added.key, added.value, fn _x -> added.value end)
    }
  end

  def apply(%Organization{} = organization, %OrgAttributeDeleted{} = deleted) do
    %Organization{organization | attributes: organization.attributes |> Map.delete(deleted.key)}
  end

  def apply(%Organization{} = organization, %OrgAttributeUpdated{} = updated) do
    %Organization{
      organization
      | attributes:
          organization.attributes
          |> Map.update(updated.key, updated.value, fn _x -> updated.value end)
    }
  end
end
