defmodule Kusanagi.Organizations.Routers.OrganizationRouter do
  use Commanded.Commands.Router

  alias Kusanagi.Organizations.Aggregates.Organization
  alias Kusanagi.Support.Middleware.{Uniqueness, Validate}

  alias Kusanagi.Organizations.Commands.{
    CreateOrganization,
    UpdateOrgStatus,
    UpdateOrganization,
    AddOrgAttribute,
    DeleteOrgAttribute,
    UpdateOrgAttribute,
    InitiateOrgClosure,
    FinishOrgClosure
  }

  alias Kusanagi.Support.Middleware.Authorizations.OrganizationAuth

  middleware(Commanded.Middleware.Auditing)
  middleware(Commanded.Middleware.Logger)
  middleware(Validate)
  middleware(Uniqueness)
  middleware(OrganizationAuth)

  dispatch(
    [
      CreateOrganization,
      UpdateOrgStatus,
      UpdateOrganization,
      InitiateOrgClosure,
      FinishOrgClosure
    ],
    to: Organization,
    identity: :uuid
  )

  dispatch(
    [
      AddOrgAttribute,
      DeleteOrgAttribute,
      UpdateOrgAttribute
    ],
    to: Organization,
    identity: :org_uuid
  )
end
