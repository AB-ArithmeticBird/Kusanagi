defmodule Kusanagi.OrgBranch.Projectors.OrgBranch do
  use Commanded.Projections.Ecto,
    name: "OrgBranch.Projectors.OrgBranch"

  alias Kusanagi.OrgBranch.Events.{
    BranchCreated,
    BranchUpdated,
    BranchAttrUpdated,
    BranchAttrAdded,
    BranchAttrDeleted,
    BranchClosureInitiated,
    BranchClosureFinished,
    BranchStatusUpdated
  }

  alias Kusanagi.Organizations.Projections.Organization
  alias Kusanagi.OrgBranch.Projections.OrgBranch
  alias Kusanagi.OrgBranch.Projections.BranchAttributes
  alias Ecto.Multi
  alias Kusanagi.Repo
  alias Kusanagi.OrgBranch.Pubsub, as: OrgPubSub
  alias Kusanagi.OrgBranch.Queries.GetOrgBranch
  alias Kusanagi.OrgBranch.Enums.EnumsOrgBranch
  alias Kusanagi.OrgBranch.Queries.BranchAttributes, as: QBranchAttributes

  project %BranchCreated{} = created, %{stream_version: version} do
    multi
    |> Multi.run(:organizations, fn _changes -> get_organization(created.org_uuid) end)
    |> Multi.run(:org_branch, fn %{organizations: org} ->
      branch = %OrgBranch{
        uuid: created.uuid,
        version: version,
        address: created.address,
        name: created.name,
        identifier: created.identifier,
        branch_type: created.branch_type,
        org_uuid: org.uuid,
        status: created.status
      }

      Repo.insert(branch, returning: true)
    end)
  end

  project %BranchUpdated{} = updated, %{stream_version: version} do
    multi
    |> Multi.update_all(
      :org_branch,
      GetOrgBranch.get_branch(updated.uuid),
      [
        set: [
          name: updated.name,
          address: updated.address,
          version: version
        ]
      ],
      returning: true
    )
  end

  project %BranchClosureInitiated{} = closure_initiated, %{stream_version: version} do
    multi
    |> Multi.update_all(
      :org_branch,
      GetOrgBranch.get_branch(closure_initiated.uuid),
      [
        set: [
          status: EnumsOrgBranch.org_branch_status()[:CLOSING],
          version: version
        ]
      ],
      returning: true
    )
  end

  project %BranchClosureFinished{} = closure_finished, %{stream_version: version} do
    multi
    |> Multi.update_all(
      :org_branch,
      GetOrgBranch.get_branch(closure_finished.uuid),
      [
        set: [
          status: EnumsOrgBranch.org_branch_status()[:CLOSED],
          version: version
        ]
      ],
      returning: true
    )
  end

  project %BranchStatusUpdated{} = status_updated, %{stream_version: version} do
    multi
    |> Multi.update_all(
      :org_branch,
      GetOrgBranch.get_branch(status_updated.uuid),
      [
        set: [
          status: status_updated.status,
          version: version
        ]
      ],
      returning: true
    )
  end

  project %BranchAttrAdded{} = added, %{stream_version: version} do
    multi
    |> Ecto.Multi.insert(:branch_attributes, %BranchAttributes{
      branch_uuid: added.b_uuid,
      key: added.key,
      value: added.value,
      version: version
    })
  end

  project %BranchAttrDeleted{} = deleted do
    multi
    |> Multi.delete_all(
      :branch_attributes,
      QBranchAttributes.get_attribute(deleted.b_uuid, deleted.key)
    )
  end

  project %BranchAttrUpdated{} = updated, %{stream_version: version} do
    multi
    |> Multi.update_all(
      :branch_attributes,
      QBranchAttributes.get_attribute(updated.b_uuid, updated.key),
      [
        set: [
          value: updated.value,
          version: version
        ]
      ],
      returning: true
    )
  end

  def after_update(_event, _metadata, changes) do
    spawn(fn ->
      schedule(changes)
    end)

    :ok
  end

  # Write a seprate Genserver Process to schedule any thing after some time,
  defp schedule(changes) do
    Process.sleep(500)
    OrgPubSub.publish_changes(changes)
  end

  defp get_organization(uuid) do
    case Repo.get(Organization, uuid) do
      nil ->
        {:error, :org_not_found}

      org ->
        {:ok, org}
    end
  end
end
