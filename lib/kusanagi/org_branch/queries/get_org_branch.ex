defmodule Kusanagi.OrgBranch.Queries.GetOrgBranch do
  import Ecto.Query

  alias Kusanagi.OrgBranch.Projections.OrgBranch

  def get_branch(branch_id) do
    from(b in OrgBranch,
      where: b.uuid == ^branch_id or b.identifier == ^branch_id
    )
  end

  def get_org_branches(org_uuid) do
    from(b in OrgBranch,
      where: b.org_uuid == ^org_uuid
    )
  end
end
