defmodule Kusanagi.OrgBranch.Queries.BranchAttributes do
  import Ecto.Query

  alias Kusanagi.OrgBranch.Projections.BranchAttributes

  def get_attribute(uuid, key) do
    from(ba in BranchAttributes,
      where: ba.branch_uuid == ^uuid and ba.key == ^key
    )
  end

  def get_attribute(uuid, key, version) do
    from(ba in BranchAttributes,
      where: ba.branch_uuid == ^uuid and ba.key == ^key and ba.version == ^version
    )
  end

  def attributes(b_uuid) do
    from(ba in BranchAttributes,
      where: ba.branch_uuid == ^b_uuid
    )
  end
end
