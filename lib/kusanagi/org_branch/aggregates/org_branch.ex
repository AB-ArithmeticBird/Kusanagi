defmodule Kusanagi.OrgBranch.Aggregates.OrgBranch do
  alias __MODULE__

  defstruct uuid: nil,
            org_uuid: nil,
            name: nil,
            address: nil,
            branch_type: nil,
            identifier: nil,
            status: nil,
            attributes: Map.new()

  alias Kusanagi.OrgBranch.Commands.{
    CreateBranch,
    UpdateBranch,
    AddBranchAttribute,
    UpdateBranchAttribute,
    DeleteBranchAttribute,
    UpdateBranchStatus,
    InitiateBranchClosure,
    FinishBranchClosure
  }

  alias Kusanagi.OrgBranch.Events.{
    BranchCreated,
    BranchUpdated,
    BranchAttrUpdated,
    BranchAttrAdded,
    BranchAttrDeleted,
    BranchStatusUpdated,
    BranchClosureInitiated,
    BranchClosureFinished
  }

  alias Kusanagi.OrgBranch.Enums.EnumsOrgBranch

  @branch_active_status EnumsOrgBranch.org_branch_status()[:ACTIVE]
  @branch_inactive_status EnumsOrgBranch.org_branch_status()[:INACTIVE]
  @branch_verified_status EnumsOrgBranch.org_branch_status()[:VERIFIED]
  @branch_created_status EnumsOrgBranch.org_branch_status()[:CREATED]
  @branch_closed_status EnumsOrgBranch.org_branch_status()[:CLOSED]
  @branch_closing_status EnumsOrgBranch.org_branch_status()[:CLOSING]

  @doc """
    Common Code for Filtering Out Closed Branch, No Action can be taken on Closed Branch
  """

  def execute(%OrgBranch{uuid: nil}, %FinishBranchClosure{}), do: {:error, :branch_not_found}

  def execute(%OrgBranch{uuid: uuid}, %FinishBranchClosure{} = finish_closure) do
    %BranchClosureFinished{
      uuid: uuid,
      org_uuid: finish_closure.org_uuid
    }
  end

  def execute(%OrgBranch{status: status} = _z, _command)
      when status == @branch_closed_status or status == @branch_closing_status do
    {:error, :branch_closed}
  end

  @doc """
    Creates a Branch
  """
  def execute(%OrgBranch{uuid: nil} = _org_branch, %CreateBranch{} = create) do
    %BranchCreated{
      uuid: create.uuid,
      org_uuid: create.org_uuid,
      name: create.name,
      address: create.address,
      branch_type: EnumsOrgBranch.org_branch_type()[String.to_atom(create.branch_type)],
      status: @branch_created_status,
      identifier: create.identifier
    }
  end

  @doc """
    Update Branch Status:
    Valid Transitions :
      Created -> Verified
      Verified -> Active
      Active -> InActive
      InActive -> Active
  """
  def execute(%OrgBranch{uuid: nil}, %UpdateBranchStatus{}), do: {:error, :branch_not_found}

  # Created -> Verified
  def execute(
        %OrgBranch{status: @branch_created_status},
        %UpdateBranchStatus{status: status}
      )
      when status != @branch_verified_status,
      do: {:error, :branch_not_verified}

  # Verified -> Active
  def execute(
        %OrgBranch{status: @branch_verified_status},
        %UpdateBranchStatus{status: status}
      )
      when status != @branch_active_status,
      do: {:error, :branch_not_active}

  # Active -> InActive
  def execute(
        %OrgBranch{status: @branch_active_status},
        %UpdateBranchStatus{status: status}
      )
      when status != @branch_inactive_status,
      do: {:error, :status_update_not_allowed}

  # InActive -> Active
  def execute(
        %OrgBranch{status: @branch_inactive_status},
        %UpdateBranchStatus{status: status}
      )
      when status != @branch_active_status,
      do: {:error, :status_update_not_allowed}

  def execute(
        %OrgBranch{uuid: uuid, org_uuid: org_uuid},
        %UpdateBranchStatus{} = update_status
      ),
      do: %BranchStatusUpdated{
        uuid: uuid,
        org_uuid: org_uuid,
        status: update_status.status
      }

  @doc """
    Close Branch:
      1. Need to Create a new Virtual/Online Branch.
      2. Assign all Account to that branch
      3. Close Current Branch
      TODO: User EventManager to Change
  """
  def execute(%OrgBranch{uuid: nil}, %InitiateBranchClosure{}), do: {:error, :branch_not_found}

  def execute(%OrgBranch{uuid: uuid, status: status}, %InitiateBranchClosure{} = initiate_closure)
      when status == @branch_active_status or status == @branch_inactive_status do
    %BranchClosureInitiated{
      uuid: uuid,
      org_uuid: initiate_closure.org_uuid
    }
  end

  def execute(%OrgBranch{}, %InitiateBranchClosure{}), do: {:error, :branch_not_found}

  @doc """
    Common Code for Filtering Out Inactive Branch, No Action can be taken on InActive Branch
    Instead of Reactivating it (Handled Above). Special Case
  """
  def execute(%OrgBranch{status: @branch_inactive_status} = _z, _command) do
    {:error, :branch_not_active}
  end

  @doc """
    Updates a Branch When branch exists
  """
  def execute(%OrgBranch{uuid: nil} = _z, %UpdateBranch{} = _update),
    do: {:error, :branch_not_found}

  def execute(
        %OrgBranch{uuid: uuid} = _z,
        %UpdateBranch{} = update
      ) do
    %BranchUpdated{
      uuid: uuid,
      name: update.name,
      address: update.address
    }
  end

  @doc """
    Add a attribute to a Branch only When a branch exists
  """
  def execute(%OrgBranch{uuid: nil} = _z, %AddBranchAttribute{}), do: {:error, :branch_not_found}

  def execute(%OrgBranch{uuid: uuid} = z, %AddBranchAttribute{} = add) do
    case Map.get(z.attributes, add.key) do
      nil ->
        %BranchAttrAdded{
          b_uuid: uuid,
          key: add.key,
          value: add.value
        }

      _ ->
        {:error, :key_already_exists}
    end
  end

  @doc """
    Delete a attribute of a Branch only When a branch exists
  """
  def execute(%OrgBranch{uuid: nil} = _z, %DeleteBranchAttribute{}),
    do: {:error, :branch_not_found}

  def execute(%OrgBranch{uuid: uuid} = _z, %DeleteBranchAttribute{} = delete) do
    %BranchAttrDeleted{
      b_uuid: uuid,
      key: delete.key
    }
  end

  @doc """
    Update a attribute of a Branch only When a branch exists
  """
  def execute(%OrgBranch{uuid: nil} = _z, %UpdateBranchAttribute{}),
    do: {:error, :branch_not_found}

  def execute(%OrgBranch{uuid: uuid} = z, %UpdateBranchAttribute{} = update) do
    case Map.get(z.attributes, update.key) do
      nil ->
        {:error, :key_not_exists}

      _ ->
        %BranchAttrUpdated{
          b_uuid: uuid,
          key: update.key,
          value: update.value
        }
    end
  end

  def apply(%OrgBranch{} = org_branch, %BranchCreated{} = created) do
    %OrgBranch{
      org_branch
      | uuid: created.uuid,
        org_uuid: created.org_uuid,
        name: created.name,
        address: created.address,
        branch_type: created.branch_type,
        identifier: created.identifier,
        status: created.status
    }
  end

  def apply(%OrgBranch{} = org_branch, %BranchStatusUpdated{} = status_updated) do
    %OrgBranch{
      org_branch
      | status: status_updated.status
    }
  end

  def apply(%OrgBranch{} = org_branch, %BranchUpdated{} = updated) do
    %OrgBranch{
      org_branch
      | name: updated.name,
        address: updated.address
    }
  end

  def apply(%OrgBranch{} = org_branch, %BranchClosureInitiated{} = _closed) do
    %OrgBranch{
      org_branch
      | status: @branch_closing_status
    }
  end

  def apply(%OrgBranch{} = org_branch, %BranchClosureFinished{} = _closed) do
    %OrgBranch{
      org_branch
      | status: @branch_closed_status
    }
  end

  def apply(%OrgBranch{} = org_branch, %BranchAttrAdded{} = added) do
    %OrgBranch{
      org_branch
      | attributes:
          org_branch.attributes |> Map.update(added.key, added.value, fn _x -> added.value end)
    }
  end

  def apply(%OrgBranch{} = org_branch, %BranchAttrDeleted{} = deleted) do
    %OrgBranch{org_branch | attributes: org_branch.attributes |> Map.delete(deleted.key)}
  end

  def apply(%OrgBranch{} = org_branch, %BranchAttrUpdated{} = updated) do
    %OrgBranch{
      org_branch
      | attributes:
          org_branch.attributes
          |> Map.update(updated.key, updated.value, fn _x -> updated.value end)
    }
  end
end
