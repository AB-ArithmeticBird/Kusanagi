defmodule Kusanagi.OrgBranch.Projections.BranchAttributes do
  use Ecto.Schema

  schema "branch_attributes" do
    field(:key, :string)
    field(:value, :string)
    field(:branch_uuid, :binary_id)
    field(:version, :integer, default: 0)

    timestamps()
  end
end
