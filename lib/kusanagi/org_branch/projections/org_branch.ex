defmodule Kusanagi.OrgBranch.Projections.OrgBranch do
  use Ecto.Schema

  @primary_key {:uuid, :binary_id, autogenerate: false}
  @foreign_key_type :binary_id

  schema "org_branch" do
    field(:version, :integer, default: 0)
    field(:address, :string)
    field(:name, :string)
    field(:identifier, :string)
    field(:branch_type, :integer)
    field(:status, :integer)

    belongs_to(:organizations, Kusanagi.Organizations.Projections.Organization,
      foreign_key: :org_uuid,
      references: :uuid
    )

    timestamps()
  end
end
