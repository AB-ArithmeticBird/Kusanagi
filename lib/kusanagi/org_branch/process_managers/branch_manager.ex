defmodule Kusanagi.OrgBranch.ProcessManagers.BranchManager do
  use Commanded.ProcessManagers.ProcessManager,
    name: "BranchManager",
    router: Kusanagi.Router
    # ,
    # event_timeout: :timer.minutes(5)

  # Process Manager timeout Feature Still in not released in commanded 0.17 may be next release support this
  # event_timeout: :timer.minutes(10)

  alias __MODULE__

  alias Kusanagi.FinancialAccounting.Events.{FAccountClosed, FAccountCreated}
  alias Kusanagi.OrgBranch.Events.{BranchClosureFinished, BranchCreated, BranchClosureInitiated}
  alias Kusanagi.OrgBranch.Commands.{FinishBranchClosure, InitiateBranchClosure}
  alias Kusanagi.Organizations.Events.OrgClosureInitiated

  @derive [Poison.Encoder]
  defstruct branch_id: nil,
            org_id: nil,
            no_of_account_active: 0,
            closed_accounts: [],
            active_accounts: []

  def interested?(%BranchCreated{uuid: branch_id}), do: {:start, branch_id}
  def interested?(%FAccountCreated{branch_id: branch_id}), do: {:continue, branch_id}
  def interested?(%FAccountClosed{branch_id: branch_id}), do: {:continue, branch_id}
  def interested?(%OrgClosureInitiated{}), do: {:continue}
  def interested?(%BranchClosureInitiated{uuid: branch_id}), do: {:continue, branch_id}
  def interested?(%BranchClosureFinished{uuid: branch_id}), do: {:stop, branch_id}
  def interested?(_event), do: false

  def handle(
        %BranchManager{branch_id: branch_uuid, no_of_account_active: active, org_id: org_id},
        %FAccountClosed{
          branch_id: branch_id
        }
      )
      when branch_uuid == branch_id and active == 1 do
    %FinishBranchClosure{
      uuid: branch_uuid,
      org_uuid: org_id
    }
  end

  def handle(
        %BranchManager{branch_id: branch_uuid, no_of_account_active: active, org_id: org_id},
        %BranchClosureInitiated{
          uuid: branch_id
        }
      )
      when branch_uuid == branch_id and active == 0 do
    %FinishBranchClosure{
      uuid: branch_uuid,
      org_uuid: org_id
    }
  end

  def handle(%BranchManager{branch_id: branch_id, org_id: org_id}, %OrgClosureInitiated{
        uuid: org_uuid
      })
      when org_id == org_uuid do
    %InitiateBranchClosure{
      uuid: branch_id,
      org_uuid: org_id
    }
  end

  def apply(%BranchManager{} = branch_pm, %BranchCreated{} = created) do
    %BranchManager{
      branch_pm
      | branch_id: created.uuid,
        org_id: created.org_uuid
    }
  end

  def apply(
        %BranchManager{
          branch_id: branch_uuid,
          no_of_account_active: active,
          active_accounts: active_accounts
        } = branch_pm,
        %FAccountCreated{uuid: account_id, branch_id: branch_id}
      )
      when branch_uuid == branch_id do
    %BranchManager{
      branch_pm
      | no_of_account_active: active + 1,
        active_accounts: [account_id | active_accounts]
    }
  end

  def apply(
        %BranchManager{
          branch_id: branch_uuid,
          no_of_account_active: active,
          active_accounts: active_accounts,
          closed_accounts: closed_accounts
        } = branch_pm,
        %FAccountClosed{uuid: account_id, branch_id: branch_id}
      )
      when branch_uuid == branch_id and active > 1 do
    %BranchManager{
      branch_pm
      | no_of_account_active: active - 1,
        active_accounts: List.delete(active_accounts, account_id),
        closed_accounts: [account_id | closed_accounts]
    }
  end

  # Stop process manager after three failures
  def error({:error, _failure}, _failed_command, %{context: %{failures: failures}})
      when failures >= 2 do
    # take Corrective Measures
    {:stop, :too_many_failures}
  end

  # Retry command, record failure count in context map
  def error({:error, _failure}, _failed_command, %{context: context}) do
    context = Map.update(context, :failures, 1, fn failures -> failures + 1 end)
    {:retry, context}
  end
end
