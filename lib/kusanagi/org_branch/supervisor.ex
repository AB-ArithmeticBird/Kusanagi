defmodule Kusanagi.OrgBranch.Supervisor do
  use Supervisor

  alias Kusanagi.OrgBranch

  def start_link do
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_arg) do
    Supervisor.init(
      [
        OrgBranch.Projectors.OrgBranch,
        OrgBranch.ProcessManagers.BranchManager
        # OrgBranch.ProcessManagers.BranchCycleManager
      ],
      strategy: :one_for_one
    )
  end
end
