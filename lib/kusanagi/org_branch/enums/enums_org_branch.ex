defmodule Kusanagi.OrgBranch.Enums.EnumsOrgBranch do
  import Kusanagi.Support.EnumsHelper

  enum "org_branch_type" do
    %{
      VIRTUAL: 1,
      PHYSICAL: 2
    }
  end

  enum "org_branch_status" do
    %{
      CREATED: 1,
      VERIFIED: 2,
      ACTIVE: 3,
      INACTIVE: 4,
      CLOSED: 5,
      CLOSING: 6
    }
  end
end
