defmodule Kusanagi.OrgBranch.Routers.OrgBranchRouter do
  use Commanded.Commands.Router

  alias Kusanagi.OrgBranch.Aggregates.OrgBranch
  alias Kusanagi.Support.Middleware.{Uniqueness, Validate}

  alias Kusanagi.OrgBranch.Commands.{
    CreateBranch,
    UpdateBranch,
    AddBranchAttribute,
    UpdateBranchAttribute,
    DeleteBranchAttribute,
    InitiateBranchClosure,
    FinishBranchClosure,
    UpdateBranchStatus
  }

  alias Kusanagi.Support.Middleware.Authorizations.OrganizationAuth

  middleware(Commanded.Middleware.Auditing)
  middleware(Commanded.Middleware.Logger)
  middleware(Validate)
  middleware(Uniqueness)
  middleware(OrganizationAuth)

  dispatch(
    [
      CreateBranch,
      UpdateBranch,
      InitiateBranchClosure,
      FinishBranchClosure,
      UpdateBranchStatus
    ],
    to: OrgBranch,
    identity: :uuid
  )

  dispatch(
    [
      AddBranchAttribute,
      UpdateBranchAttribute,
      DeleteBranchAttribute
    ],
    to: OrgBranch,
    identity: :branch_uuid
  )
end
