defmodule Kusanagi.OrgBranch do
  @moduledoc """
  The Organizations context.
  """

  import Ecto.Query, warn: false
  alias Kusanagi.Repo
  alias Kusanagi.OrgBranch.Projections.OrgBranch, as: OrgBranchPro
  alias Kusanagi.Organizations.Projections.Organization
  alias Kusanagi.OrgBranch.Projections.BranchAttributes

  alias Kusanagi.OrgBranch.Commands.{
    UpdateBranch,
    CreateBranch,
    AddBranchAttribute,
    UpdateBranchAttribute,
    DeleteBranchAttribute,
    InitiateBranchClosure,
    UpdateBranchStatus
  }

  alias Kusanagi.Router
  alias Kusanagi.OrgBranch.Pubsub, as: OrgPubsub
  alias Kusanagi.Organizations.Enums.EnumsOrganization
  alias Kusanagi.OrgBranch.Enums.EnumsOrgBranch
  alias Kusanagi.OrgBranch.Queries.BranchAttributes, as: QBranchAttributes

  @org_active_status EnumsOrganization.organization_status()[:ACTIVE]
  @org_created_status EnumsOrganization.organization_status()[:CREATED]
  @org_verified_status EnumsOrganization.organization_status()[:VERIFIED]

  # Org Branch Apis

  @doc """
  Creates a organization Branch.

  ## Examples

      iex> create_branch(%Organization{},%{field: value})
      {:ok, %OrgBranchPro{}}

      iex> create_organization(%Organization{},%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_branch(%Organization{status: status}, _attrs) when status != @org_active_status do
    case status do
      @org_created_status -> {:error, :org_not_verified}
      @org_verified_status -> {:error, :org_not_active}
      _ -> {:error, :org_not_found}
    end
  end

  def create_branch(
        %Organization{uuid: org_uuid} = _organization,
        attrs
      ) do
    uuid = UUID.uuid4()

    create_branch =
      attrs
      |> CreateBranch.new()
      |> CreateBranch.assign_uuid(uuid)
      |> CreateBranch.assign_org_uuid(org_uuid)

    with {:ok, version} <- Router.dispatch(create_branch, include_aggregate_version: true) do
      OrgPubsub.wait_for(OrgBranchPro, uuid, version)
    else
      reply -> reply
    end
  end

  def update_branch(%OrgBranchPro{uuid: branch_uuid} = _organization, attrs) do
    update_branch =
      attrs
      |> UpdateBranch.new()
      |> UpdateBranch.assign_uuid(branch_uuid)

    with {:ok, version} <- Router.dispatch(update_branch, include_aggregate_version: true) do
      OrgPubsub.wait_for(OrgBranchPro, branch_uuid, version)
    else
      reply -> reply
    end
  end

  def get_org_branch!(uuid), do: Repo.get(OrgBranchPro, uuid)

  @doc """
  Closes a Branch.
  Branch can be closed Only if status is Active/InActive.

  ## Examples

      iex> close_branch(%OrgBranchPro{})
      {:ok, %OrgBranchPro{}}

      iex> close_branch(%OrgBranchPro{})
      {:error, %Ecto.Changeset{}}

  """
  def close_branch(%OrgBranchPro{uuid: branch_uuid, org_uuid: org_uuid}) do
    close_branch =
      InitiateBranchClosure.new(%{})
      |> InitiateBranchClosure.assign_uuid(branch_uuid)
      |> InitiateBranchClosure.assign_org_uuid(org_uuid)

    with {:ok, version} <- Router.dispatch(close_branch, include_aggregate_version: true) do
      OrgPubsub.wait_for(OrgBranchPro, branch_uuid, version)
    else
      reply -> reply
    end
  end

  @doc """
  Update Status of a Branch.
  Valid Status Transitions are
      Created -> Verified
      Verified -> Active
      Active -> InActive
      InActive -> Active

  ## Examples

      iex> update_branch_status(%OrgBranchPro{},attrs)
      {:ok, %OrgBranchPro{}}

      iex> update_branch_status(%OrgBranchPro{},attrs)
      {:error, %Ecto.Changeset{}}

  """
  def update_branch_status(%OrgBranchPro{uuid: branch_uuid, org_uuid: org_uuid} = _branch, %{
        "status" => status
      }) do
    update_branch_status =
      UpdateBranchStatus.new(%{})
      |> UpdateBranchStatus.assign_uuid(branch_uuid)
      |> UpdateBranchStatus.assign_org_uuid(org_uuid)
      |> UpdateBranchStatus.update_status(
        EnumsOrgBranch.org_branch_status()[String.to_atom(status)]
      )

    with {:ok, version} <- Router.dispatch(update_branch_status, include_aggregate_version: true) do
      OrgPubsub.wait_for(OrgBranchPro, branch_uuid, version)
    else
      reply -> reply
    end
  end

  def add_branch_attribute(%OrgBranchPro{uuid: b_uuid}, attrs) do
    add_branch_attribute =
      attrs
      |> AddBranchAttribute.new()
      |> AddBranchAttribute.assign_branch_uuid(b_uuid)

    with {:ok, version} <- Router.dispatch(add_branch_attribute, include_aggregate_version: true) do
      OrgPubsub.wait_for(
        BranchAttributes,
        b_uuid,
        "#{add_branch_attribute.key}_#{version}",
        QBranchAttributes.get_attribute(b_uuid, add_branch_attribute.key, version)
      )
    else
      reply -> reply
    end
  end

  def update_branch_attribute(%OrgBranchPro{uuid: b_uuid}, attrs) do
    update_branch_attribute =
      attrs
      |> UpdateBranchAttribute.new()
      |> UpdateBranchAttribute.assign_branch_uuid(b_uuid)

    with {:ok, version} <-
           Router.dispatch(update_branch_attribute, include_aggregate_version: true) do
      OrgPubsub.wait_for(
        BranchAttributes,
        b_uuid,
        "#{update_branch_attribute.key}_#{version}",
        QBranchAttributes.get_attribute(b_uuid, update_branch_attribute.key, version)
      )
    else
      reply -> reply
    end
  end

  def delete_branch_attribute(%OrgBranchPro{uuid: b_uuid}, attrs) do
    delete_branch_attribute =
      attrs
      |> DeleteBranchAttribute.new()
      |> DeleteBranchAttribute.assign_branch_uuid(b_uuid)

    Router.dispatch(delete_branch_attribute)
  end

  def list_b_attributes(%{"b_uuid" => b_uuid}) do
    b_uuid
    |> QBranchAttributes.attributes()
    |> Repo.all()
  end
end
