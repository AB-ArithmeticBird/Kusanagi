defmodule Kusanagi.OrgBranch.Events.BranchAttrAdded do
  @derive [Poison.Encoder]

  defstruct [
    :b_uuid,
    :key,
    :value
  ]
end
