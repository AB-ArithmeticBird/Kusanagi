defmodule Kusanagi.OrgBranch.Events.BranchCreated do
  @derive [Poison.Encoder]

  defstruct [
    :uuid,
    :name,
    :address,
    :identifier,
    :org_uuid,
    :status,
    :branch_type
  ]
end
