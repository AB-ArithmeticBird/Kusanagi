defmodule Kusanagi.OrgBranch.Events.BranchClosureInitiated do
  @derive [Poison.Encoder]

  defstruct [
    :uuid,
    :org_uuid
  ]
end
