defmodule Kusanagi.OrgBranch.Events.BranchUpdated do
  @derive [Poison.Encoder]

  defstruct [
    :uuid,
    :name,
    :address
  ]
end
