defmodule Kusanagi.OrgBranch.Events.BranchStatusUpdated do
  @derive [Poison.Encoder]

  defstruct [
    :uuid,
    :org_uuid,
    :status
  ]
end
