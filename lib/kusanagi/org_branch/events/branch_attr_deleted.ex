defmodule Kusanagi.OrgBranch.Events.BranchAttrDeleted do
  @derive [Poison.Encoder]

  defstruct [
    :b_uuid,
    :key
  ]
end
