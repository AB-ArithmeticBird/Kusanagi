defmodule Kusanagi.OrgBranch.Events.BranchClosureFinished do
  @derive [Poison.Encoder]

  defstruct [
    :uuid,
    :org_uuid
  ]
end
