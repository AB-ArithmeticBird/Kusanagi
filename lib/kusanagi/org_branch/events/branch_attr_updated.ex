defmodule Kusanagi.OrgBranch.Events.BranchAttrUpdated do
  @derive [Poison.Encoder]

  defstruct [
    :b_uuid,
    :key,
    :value
  ]
end
