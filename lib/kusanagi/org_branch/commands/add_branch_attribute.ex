defmodule Kusanagi.OrgBranch.Commands.AddBranchAttribute do
  alias __MODULE__

  defstruct [
    :branch_uuid,
    :key,
    :value
  ]

  use ExConstructor
  use Vex.Struct

  validates(:branch_uuid,
    presence: [message: "can't be empty"]
  )

  validates(:key,
    presence: [message: "can't be empty"]
  )

  validates(:value,
    presence: [message: "can't be empty"]
  )

  def assign_branch_uuid(%AddBranchAttribute{} = add_branch_attribute, branch_uuid) do
    %AddBranchAttribute{add_branch_attribute | branch_uuid: branch_uuid}
  end
end
