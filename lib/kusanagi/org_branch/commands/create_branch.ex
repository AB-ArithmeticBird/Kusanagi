defmodule Kusanagi.OrgBranch.Commands.CreateBranch do
  alias __MODULE__

  defstruct [
    :uuid,
    :name,
    :address,
    :identifier,
    :branch_type,
    :org_uuid
  ]

  use ExConstructor
  use Vex.Struct

  validates(:uuid, uuid: true)

  validates(:org_uuid,
    presence: [message: "can't be empty"]
  )

  validates(:name,
    presence: [message: "can't be empty"]
  )

  validates(:branch_type,
    presence: [message: "can't be empty"]
  )

  validates(:identifier,
    presence: [message: "can't be empty"]
  )

  def assign_uuid(%CreateBranch{} = create_branch, uuid) do
    %CreateBranch{create_branch | uuid: uuid}
  end

  def assign_org_uuid(%CreateBranch{} = create_branch, org_uuid) do
    %CreateBranch{create_branch | org_uuid: org_uuid}
  end
end
