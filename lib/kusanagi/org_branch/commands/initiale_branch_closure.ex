defmodule Kusanagi.OrgBranch.Commands.InitiateBranchClosure do
  alias __MODULE__

  defstruct [
    :uuid,
    :org_uuid
  ]

  use ExConstructor
  use Vex.Struct

  validates(:uuid,
    presence: [message: "can't be empty"]
  )

  validates(:org_uuid,
    presence: [message: "can't be empty"]
  )

  def assign_uuid(%InitiateBranchClosure{} = i_close_branch, uuid) do
    %InitiateBranchClosure{i_close_branch | uuid: uuid}
  end

  def assign_org_uuid(%InitiateBranchClosure{} = i_close_branch, org_uuid) do
    %InitiateBranchClosure{i_close_branch | org_uuid: org_uuid}
  end
end
