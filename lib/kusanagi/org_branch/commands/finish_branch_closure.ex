defmodule Kusanagi.OrgBranch.Commands.FinishBranchClosure do
  alias __MODULE__

  defstruct [
    :uuid,
    :org_uuid
  ]

  use ExConstructor
  use Vex.Struct

  validates(:uuid,
    presence: [message: "can't be empty"]
  )

  validates(:org_uuid,
    presence: [message: "can't be empty"]
  )

  def assign_uuid(%FinishBranchClosure{} = f_close_branch, uuid) do
    %FinishBranchClosure{f_close_branch | uuid: uuid}
  end

  def assign_org_uuid(%FinishBranchClosure{} = f_close_branch, org_uuid) do
    %FinishBranchClosure{f_close_branch | org_uuid: org_uuid}
  end
end
