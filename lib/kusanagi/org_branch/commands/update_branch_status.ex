defmodule Kusanagi.OrgBranch.Commands.UpdateBranchStatus do
  alias __MODULE__

  defstruct [
    :uuid,
    :org_uuid,
    :status
  ]

  use ExConstructor
  use Vex.Struct

  validates(:uuid,
    presence: [message: "can't be empty"]
  )

  validates(:status,
    presence: [message: "can't be empty"]
  )

  @doc """
  Assign a unique identity for the account
  """
  def assign_uuid(%UpdateBranchStatus{} = update_branch_status, uuid) do
    %UpdateBranchStatus{update_branch_status | uuid: uuid}
  end

  def assign_org_uuid(%UpdateBranchStatus{} = update_branch_status, org_uuid) do
    %UpdateBranchStatus{update_branch_status | org_uuid: org_uuid}
  end

  def update_status(%UpdateBranchStatus{} = update_branch_status, status) do
    %UpdateBranchStatus{update_branch_status | status: status}
  end
end
