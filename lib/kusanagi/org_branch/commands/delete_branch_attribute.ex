defmodule Kusanagi.OrgBranch.Commands.DeleteBranchAttribute do
  alias __MODULE__

  defstruct [
    :branch_uuid,
    :key
  ]

  use ExConstructor
  use Vex.Struct

  validates(:branch_uuid,
    presence: [message: "can't be empty"]
  )

  validates(:key,
    presence: [message: "can't be empty"]
  )

  def assign_branch_uuid(%DeleteBranchAttribute{} = delete_b_attribute, branch_uuid) do
    %DeleteBranchAttribute{delete_b_attribute | branch_uuid: branch_uuid}
  end

  def assign_key(%DeleteBranchAttribute{} = delete_b_attribute, key) do
    %DeleteBranchAttribute{delete_b_attribute | key: key}
  end
end
