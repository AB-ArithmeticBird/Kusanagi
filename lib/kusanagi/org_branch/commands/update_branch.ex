defmodule Kusanagi.OrgBranch.Commands.UpdateBranch do
  alias __MODULE__

  defstruct [
    :uuid,
    :name,
    :address
  ]

  use ExConstructor
  use Vex.Struct

  validates(:uuid,
    presence: [message: "can't be empty"]
  )

  validates(:name,
    presence: [message: "can't be empty"]
  )

  validates(:address,
    presence: [message: "can't be empty"]
  )

  def assign_uuid(%UpdateBranch{} = update_branch, uuid) do
    %UpdateBranch{update_branch | uuid: uuid}
  end
end
