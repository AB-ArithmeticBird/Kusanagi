defmodule Kusanagi.OrgBranch.Pubsub do
  alias Kusanagi.OrgBranch.Projections.OrgBranch
  alias Kusanagi.OrgBranch.Projections.BranchAttributes
  alias Kusanagi.Repo
  alias Phoenix.PubSub

  def wait_for(schema, uuid, version) do
    case Repo.get_by(schema, uuid: uuid, version: version) do
      nil -> subscribe_and_wait(schema, uuid, version)
      projection -> {:ok, projection}
    end
  end

  def wait_for(schema, uuid, identifier, query) do
    case Repo.one(query) do
      nil ->
        subscribe_and_wait(schema, uuid, identifier)

      projection ->
        {:ok, projection}
    end
  end

  # Ordeirng of pattern mathc is Important
  def publish_changes(%{branch_attributes: %BranchAttributes{} = branch_attr}),
    do: publish(branch_attr)

  def publish_changes(%{branch_attributes: {_, branch_attrs}}) when is_list(branch_attrs),
    do: Enum.each(branch_attrs, &publish/1)

  def publish_changes(%{org_branch: %OrgBranch{} = org_branch}), do: publish(org_branch)

  def publish_changes(%{org_branch: {_, org_branches}}) when is_list(org_branches),
    do: Enum.each(org_branches, &publish/1)

  def publish_changes(_changes), do: :ok

  defp publish(%OrgBranch{uuid: uuid, version: version} = org_branch) do
    PubSub.broadcast(Kusanagi.PubSub, "OrgBranch:#{uuid}:#{version}", {OrgBranch, org_branch})
  end

  defp publish(%BranchAttributes{branch_uuid: b_uuid, key: key, version: version} = b_attrs) do
    PubSub.broadcast(
      Kusanagi.PubSub,
      "BranchAttributes:#{b_uuid}:#{key}_#{version}",
      {BranchAttributes, b_attrs}
    )
  end

  defp subscribe_and_wait(schema, org_uuid, extra_identifier) do
    PubSub.subscribe(
      Kusanagi.PubSub,
      "#{getSchemaString(schema)}:#{org_uuid}:#{extra_identifier}"
    )

    receive do
      {^schema, projection} ->
        {:ok, projection}
    after
      20_000 ->
        {:error, :timeout}
    end
  end

  defp getSchemaString(OrgBranch) do
    "OrgBranch"
  end

  defp getSchemaString(BranchAttributes) do
    "BranchAttributes"
  end
end
