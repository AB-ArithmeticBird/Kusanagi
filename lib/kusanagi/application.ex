defmodule Kusanagi.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      # Start the Ecto repository
      supervisor(Kusanagi.Repo, []),
      # Start the endpoint when the application starts
      supervisor(KusanagiWeb.Endpoint, []),
      # Start your own worker by calling: Kusanagi.Worker.start_link(arg1, arg2, arg3)
      # worker(Kusanagi.Worker, [arg1, arg2, arg3]),

      # Accounts supervisor
      supervisor(Kusanagi.Accounts.Supervisor, []),

      # FinancialAccounting supervisor
      supervisor(Kusanagi.FinancialAccounting.Supervisor, []),

      # Organization supervisor
      supervisor(Kusanagi.Organizations.Supervisor, []),

      # OrgBranch supervisor
      supervisor(Kusanagi.OrgBranch.Supervisor, []),

      # Enforce unique constraints
      worker(Kusanagi.Support.Unique, [])
    ]

    opts = [strategy: :one_for_one, name: Kusanagi.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    KusanagiWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
