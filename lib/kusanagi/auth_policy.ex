defmodule Kusanagi.AuthPolicy do
  @behaviour Bodyguard.Policy

  alias Kusanagi.Accounts.Projections.User
  alias Kusanagi.FinancialAccounting.Projections.FAccount

  def authorize(_action, %User{role: role}, _params) when role == "admin", do: :ok

  def authorize(action, %User{uuid: user_id}, %FAccount{user_uuid: account_user_id})
      when action in [:get_f_account, :credit_f_account, :debit_f_account] do
    if user_id == account_user_id do
      :ok
    else
      {:error, :unauthorized}
    end
  end

  def authorize(_, _, _), do: {:error, :unauthorized}
end
