defmodule Kusanagi.Router do
  use Commanded.Commands.CompositeRouter

  router(Kusanagi.Accounts.Routers.AccountRouter)
  router(Kusanagi.FinancialAccounting.Routers.FAccountRouter)
  router(Kusanagi.Organizations.Routers.OrganizationRouter)
  router(Kusanagi.OrgBranch.Routers.OrgBranchRouter)
end
