defmodule Kusanagi.Repo.Migrations.DropOrganization do
  use Ecto.Migration

  def change do
    drop(table(:organizations))
  end
end
