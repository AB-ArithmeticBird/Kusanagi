defmodule Kusanagi.Repo.Migrations.CreateOrgBranchTable do
  use Ecto.Migration

  def change do
    create table(:org_branch, primary_key: false) do
      add(:uuid, :uuid, primary_key: true)
      add(:branch_type, :int, default: 1)
      add(:name, :string)
      add(:address, :string)
      add(:identifier, :string)
      add(:org_uuid, references(:organizations, column: :uuid, type: :uuid))
      add(:version, :bigint)

      timestamps()
    end
  end
end
