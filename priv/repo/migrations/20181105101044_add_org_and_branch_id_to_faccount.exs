defmodule Kusanagi.Repo.Migrations.AddOrgAndBranchIdToFaccount do
  use Ecto.Migration

  def change do
    alter table(:financial_accounts) do
      add(:org_id, :uuid)
      add(:branch_id, :uuid)
    end
  end
end
