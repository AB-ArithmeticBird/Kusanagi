defmodule Kusanagi.Repo.Migrations.AddVersionToOrgAttributeTable do
  use Ecto.Migration

  def change do
    alter table(:org_attributes) do
      add(:version, :bigint)
    end
  end
end
