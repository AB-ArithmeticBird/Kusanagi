defmodule Kusanagi.Repo.Migrations.AddStatusToOrgBranch do
  use Ecto.Migration

  def change do
    alter table(:org_branch) do
      add(:status, :integer, default: 1)
    end
  end
end
