defmodule Kusanagi.Repo.Migrations.AddVersionToTables do
  use Ecto.Migration

  def change do
    alter table(:accounts_users) do
      add(:version, :bigint)
    end

    alter table(:financial_accounts) do
      add(:version, :bigint)
    end
  end
end
