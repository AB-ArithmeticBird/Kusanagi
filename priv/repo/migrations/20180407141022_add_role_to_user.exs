defmodule Kusanagi.Repo.Migrations.AddRoleToUser do
  use Ecto.Migration

  def change do
    alter table(:accounts_users) do
      add(:role, :string)
    end
  end
end
