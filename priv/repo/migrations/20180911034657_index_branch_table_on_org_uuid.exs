defmodule Kusanagi.Repo.Migrations.IndexBranchTableOnOrgUuid do
  use Ecto.Migration

  def change do
    create(index(:org_branch, [:org_uuid]))
    create(index(:org_branch, [:identifier]))
  end
end
