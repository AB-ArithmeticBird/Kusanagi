defmodule Kusanagi.Repo.Migrations.CreateBranchAttributeTable do
  use Ecto.Migration

  def change do
    create table(:branch_attributes) do
      add(:key, :string, null: false)
      add(:value, :string, null: false)
      add(:branch_uuid, :uuid, null: false)
      add(:version, :bigint)

      timestamps()
    end

    create(index(:branch_attributes, [:branch_uuid]))
    create(index(:branch_attributes, [:key]))
  end
end
