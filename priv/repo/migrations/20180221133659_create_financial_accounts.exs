defmodule Kusanagi.Repo.Migrations.CreateFinancialAccounts do
  use Ecto.Migration

  def change do
    create table(:financial_accounts, primary_key: false) do
      add(:uuid, :uuid, primary_key: true)
      add(:account_type, :int)
      add(:account_name, :string)
      add(:description, :string)
      add(:balance, :bigint)
      add(:user_uuid, references(:accounts_users, column: :uuid, type: :uuid))
      timestamps()
    end
  end
end
