defmodule Kusanagi.Repo.Migrations.AddCurrencyToFAccount do
  use Ecto.Migration

  def change do
    alter table(:financial_accounts) do
      add(:currency, :string)
    end
  end
end
