defmodule Kusanagi.Repo.Migrations.AddVersionToOrganization do
  use Ecto.Migration

  def change do
    alter table(:organizations) do
      add(:version, :bigint)
    end
  end
end
