defmodule Kusanagi.Repo.Migrations.AddMoreColumnsToOrganization do
  use Ecto.Migration

  def change do
    alter table(:organizations) do
      add(:status, :integer, default: 1)
      add(:website, :string)
    end
  end
end
