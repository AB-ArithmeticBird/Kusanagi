defmodule Kusanagi.Repo.Migrations.CreateOrganizations do
  use Ecto.Migration

  def change do
    create table(:organizations) do
      add(:name, :string)
      add(:email, :string)
      add(:phone_number, :string)
      add(:timeZone, :string)
      add(:address, :string)
      add(:attribute, :string)

      timestamps()
    end
  end
end
