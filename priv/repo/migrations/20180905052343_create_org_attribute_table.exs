defmodule Kusanagi.Repo.Migrations.CreateOrgAttributeTable do
  use Ecto.Migration

  def change do
    create table(:org_attributes) do
      add(:key, :string, null: false)
      add(:value, :string, null: false)
      add(:org_uuid, :uuid)

      timestamps()
    end

    create(index(:org_attributes, [:org_uuid]))
    create(index(:org_attributes, [:key]))
  end
end
