defmodule Kusanagi.Repo.Migrations.AddStatusToFinancialAccount do
  use Ecto.Migration

  def change do
    alter table(:financial_accounts) do
      add(:status, :integer, default: 1)
    end
  end
end
