defmodule Kusanagi.Repo.Migrations.RecreateOrganization do
  use Ecto.Migration

  def change do
    create table(:organizations, primary_key: false) do
      add(:uuid, :uuid, primary_key: true)
      add(:name, :string)
      add(:email, :string)
      add(:phone_number, :string)
      add(:timeZone, :string)
      add(:address, :string)
      add(:status, :integer, default: 1)
      add(:website, :string)

      timestamps()
    end
  end
end
