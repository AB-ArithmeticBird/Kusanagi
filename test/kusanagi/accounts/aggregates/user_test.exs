defmodule Kusanagi.Accounts.Aggregates.UserTest do
  use Kusanagi.AggregateCase, aggregate: Kusanagi.Accounts.Aggregates.User

  alias Kusanagi.Accounts.Events.UserRegistered

  describe "register user" do
    @tag :unit
    test "should succeed when valid" do
      user_uuid = UUID.uuid4()

      assert_events(build(:register_user, user_uuid: user_uuid), [
        %UserRegistered{
          user_uuid: user_uuid,
          email: "jake@jake.jake",
          username: "jake",
          hashed_password: "jakejake",
          role: "user"
        }
      ])
    end
  end
end
