defmodule Kusanagi.OrgBranchTest do
  use Kusanagi.DataCase

  alias Kusanagi.OrgBranch

  describe "organizations" do
    alias Kusanagi.OrgBranch.Projections.OrgBranch, as: OrgBranchPro
    alias Kusanagi.OrgBranch.Projections.BranchAttributes
    alias Kusanagi.OrgBranch.Enums.EnumsOrgBranch

    @valid_attrs %{
      address: "some address",
      attribute: "some attribute",
      email: "some email",
      name: "some name",
      phone_number: "some phone_number",
      timeZone: "some timeZone",
      website: "some website",
      status: 1
    }
    @create_branch %{
      name: "some name",
      address: "some address",
      branch_type: "PHYSICAL",
      identifier: "IDENT_#{:rand.uniform(50)}_#{:rand.uniform(80)}"
    }
    @update_branch %{name: "some updated name", address: "some updated address"}
    @create_branch_attribute %{key: "Timing", value: "8:00AM-6:00PM"}
    @update_branch_attribute %{key: "Timing", value: "7:30AM-2:30PM"}

    @branch_verified_status EnumsOrgBranch.org_branch_status()[:VERIFIED]
    @branch_active_status EnumsOrgBranch.org_branch_status()[:ACTIVE]
    @branch_inactive_status EnumsOrgBranch.org_branch_status()[:INACTIVE]
    @branch_created_status EnumsOrgBranch.org_branch_status()[:CREATED]
    @branch_closing_status EnumsOrgBranch.org_branch_status()[:CLOSING]

    defp organization_fixture(attrs \\ %{}) do
      {:ok, organization} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Kusanagi.Organizations.create_organization()

      organization
    end

    defp update_status_organization(organization, status) do
      {:ok, org} =
        organization
        |> Kusanagi.Organizations.update_status(%{"status" => status})

      org
    end

    defp close_organization(organization) do
      {:ok, org} =
        organization
        |> Kusanagi.Organizations.close_organization()

      org
    end

    defp create_active_organization() do
      organization_fixture()
      |> update_status_organization("VERIFIED")
      |> update_status_organization("ACTIVE")
    end

    test "create_branch/2 creates the branch for the active organization" do
      organization = create_active_organization()

      assert {:ok, branch} = OrgBranch.create_branch(organization, @create_branch)
      assert %OrgBranchPro{} = branch
      assert branch.org_uuid == organization.uuid
      assert branch.name == "some name"
      assert branch.address == "some address"
      assert branch.identifier == Map.get(@create_branch, :identifier)
    end

    test "create_branch/2 should fail to create the branch for the non verified organization" do
      organization = organization_fixture()
      assert {:error, :org_not_verified} = OrgBranch.create_branch(organization, @create_branch)
    end

    test "create_branch/2 should fail to create the branch for the non active organization" do
      organization = organization_fixture() |> update_status_organization("VERIFIED")
      assert {:error, :org_not_active} = OrgBranch.create_branch(organization, @create_branch)
    end

    test "create_branch/2 should fail to create the branch for the inactive organization" do
      organization = create_active_organization() |> update_status_organization("INACTIVE")
      assert {:error, :org_not_found} = OrgBranch.create_branch(organization, @create_branch)
    end

    test "create_branch/2 should fail to create the branch for the closed organization" do
      organization = create_active_organization() |> close_organization()
      assert {:error, :org_not_found} = OrgBranch.create_branch(organization, @create_branch)
    end

    test "update_branch/2 updates the branch for the active organization and non-inactive branch" do
      organization = create_active_organization()
      assert {:ok, branch} = OrgBranch.create_branch(organization, @create_branch)
      assert %OrgBranchPro{} = branch
      assert branch.org_uuid == organization.uuid
      assert branch.name == "some name"
      assert branch.address == "some address"
      assert branch.identifier == Map.get(@create_branch, :identifier)
      assert {:ok, updated_branch} = OrgBranch.update_branch(branch, @update_branch)
      assert %OrgBranchPro{} = updated_branch
      assert updated_branch.name == "some updated name"
      assert updated_branch.address == "some updated address"
    end

    test "update_branch/2 fails to updates the branch for the inactive branch" do
      organization = create_active_organization()

      assert assert {:ok, %OrgBranchPro{status: @branch_created_status} = branch} =
                      OrgBranch.create_branch(organization, @create_branch)

      assert assert {:ok, %OrgBranchPro{status: @branch_verified_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "VERIFIED"})

      assert assert {:ok, %OrgBranchPro{status: @branch_active_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "ACTIVE"})

      assert assert {:ok, %OrgBranchPro{status: @branch_inactive_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "INACTIVE"})

      assert {:error, :branch_not_active} = OrgBranch.update_branch(branch, @update_branch)
    end

    test "update_branch/2 fails to updates the branch for the non_existing branch" do
      organization = create_active_organization()

      assert assert {:ok, %OrgBranchPro{status: @branch_created_status} = branch} =
                      OrgBranch.create_branch(organization, @create_branch)

      assert {:error, :branch_not_found} =
               OrgBranch.update_branch(%OrgBranchPro{branch | uuid: UUID.uuid4()}, @update_branch)
    end

    test "update_branch_status/1 should trasit from Created-> Verified status" do
      organization = create_active_organization()

      assert assert {:ok, %OrgBranchPro{status: @branch_created_status} = branch} =
                      OrgBranch.create_branch(organization, @create_branch)

      assert assert {:ok, %OrgBranchPro{status: @branch_verified_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "VERIFIED"})
    end

    test "update_branch_status/1 should fail to trasit to status other than VERIFED from CREATED status" do
      organization = create_active_organization()

      assert assert {:ok, %OrgBranchPro{status: @branch_created_status} = branch} =
                      OrgBranch.create_branch(organization, @create_branch)

      assert {:error, :branch_not_verified} =
               OrgBranch.update_branch_status(branch, %{
                 "status" => Enum.random(["CREATED", "ACTIVE", "INACTIVE", "CLOSED"])
               })
    end

    test "update_branch_status/1 should trasit from VERIFIED-> ACTIVE" do
      organization = create_active_organization()

      assert assert {:ok, %OrgBranchPro{status: @branch_created_status} = branch} =
                      OrgBranch.create_branch(organization, @create_branch)

      assert assert {:ok, %OrgBranchPro{status: @branch_verified_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "VERIFIED"})

      assert assert {:ok, %OrgBranchPro{status: @branch_active_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "ACTIVE"})
    end

    test "update_branch_status/1 should fail to trasit to status other than ACTIVE from VERIFIED status" do
      organization = create_active_organization()

      assert assert {:ok, %OrgBranchPro{status: @branch_created_status} = branch} =
                      OrgBranch.create_branch(organization, @create_branch)

      assert assert {:ok, %OrgBranchPro{status: @branch_verified_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "VERIFIED"})

      assert {:error, :branch_not_active} =
               OrgBranch.update_branch_status(branch, %{
                 "status" => Enum.random(["CREATED", "VERIFIED", "INACTIVE", "CLOSED"])
               })
    end

    test "update_branch_status/1 should trasit from ACTIVE-> INACTIVE" do
      organization = create_active_organization()

      assert assert {:ok, %OrgBranchPro{status: @branch_created_status} = branch} =
                      OrgBranch.create_branch(organization, @create_branch)

      assert assert {:ok, %OrgBranchPro{status: @branch_verified_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "VERIFIED"})

      assert assert {:ok, %OrgBranchPro{status: @branch_active_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "ACTIVE"})

      assert assert {:ok, %OrgBranchPro{status: @branch_inactive_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "INACTIVE"})
    end

    test "update_branch_status/1 should fail to trasit to status other than INACTIVE from ACTIVE status" do
      organization = create_active_organization()

      assert assert {:ok, %OrgBranchPro{status: @branch_created_status} = branch} =
                      OrgBranch.create_branch(organization, @create_branch)

      assert assert {:ok, %OrgBranchPro{status: @branch_verified_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "VERIFIED"})

      assert assert {:ok, %OrgBranchPro{status: @branch_active_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "ACTIVE"})

      assert {:error, :status_update_not_allowed} =
               OrgBranch.update_branch_status(branch, %{
                 "status" => Enum.random(["CREATED", "VERIFIED", "ACTIVE", "CLOSED"])
               })
    end

    test "update_branch_status/1 should trasit from INACTIVE-> ACTIVE" do
      organization = create_active_organization()

      assert assert {:ok, %OrgBranchPro{status: @branch_created_status} = branch} =
                      OrgBranch.create_branch(organization, @create_branch)

      assert assert {:ok, %OrgBranchPro{status: @branch_verified_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "VERIFIED"})

      assert assert {:ok, %OrgBranchPro{status: @branch_active_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "ACTIVE"})

      assert assert {:ok, %OrgBranchPro{status: @branch_inactive_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "INACTIVE"})

      assert assert {:ok, %OrgBranchPro{status: @branch_active_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "ACTIVE"})
    end

    test "update_branch_status/1 should fail to trasit to status other than ACTIVE from INACTIVE status" do
      organization = create_active_organization()

      assert assert {:ok, %OrgBranchPro{status: @branch_created_status} = branch} =
                      OrgBranch.create_branch(organization, @create_branch)

      assert assert {:ok, %OrgBranchPro{status: @branch_verified_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "VERIFIED"})

      assert assert {:ok, %OrgBranchPro{status: @branch_active_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "ACTIVE"})

      assert assert {:ok, %OrgBranchPro{status: @branch_inactive_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "INACTIVE"})

      assert {:error, :status_update_not_allowed} =
               OrgBranch.update_branch_status(branch, %{
                 "status" => Enum.random(["CREATED", "VERIFIED", "INACTIVE", "CLOSED"])
               })
    end

    test "close_branch/1 closes the branch when status was ACTIVE" do
      organization = create_active_organization()

      assert assert {:ok, %OrgBranchPro{status: @branch_created_status} = branch} =
                      OrgBranch.create_branch(organization, @create_branch)

      assert assert {:ok, %OrgBranchPro{status: @branch_verified_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "VERIFIED"})

      assert assert {:ok, %OrgBranchPro{status: @branch_active_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "ACTIVE"})

      assert {:ok, %OrgBranchPro{status: @branch_closing_status}} = OrgBranch.close_branch(branch)
    end

    test "close_organization/1 closes the organization when status was INACTIVE" do
      organization = create_active_organization()

      assert assert {:ok, %OrgBranchPro{status: @branch_created_status} = branch} =
                      OrgBranch.create_branch(organization, @create_branch)

      assert assert {:ok, %OrgBranchPro{status: @branch_verified_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "VERIFIED"})

      assert assert {:ok, %OrgBranchPro{status: @branch_active_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "ACTIVE"})

      assert assert {:ok, %OrgBranchPro{status: @branch_inactive_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "INACTIVE"})

      assert {:ok, %OrgBranchPro{status: @branch_closing_status}} = OrgBranch.close_branch(branch)
    end

    test "close_organization/1 should fail to close the organization when status was CREATED" do
      organization = create_active_organization()

      assert assert {:ok, %OrgBranchPro{status: @branch_created_status} = branch} =
                      OrgBranch.create_branch(organization, @create_branch)

      assert {:error, :branch_not_found} = OrgBranch.close_branch(branch)
    end

    test "close_organization/1 should fail to close the organization when status was VERIFIED" do
      organization = create_active_organization()

      assert assert {:ok, %OrgBranchPro{status: @branch_created_status} = branch} =
                      OrgBranch.create_branch(organization, @create_branch)

      assert assert {:ok, %OrgBranchPro{status: @branch_verified_status}} =
                      OrgBranch.update_branch_status(branch, %{"status" => "VERIFIED"})

      assert {:error, :branch_not_found} = OrgBranch.close_branch(branch)
    end

    test "add_branch_attribute/1 adds the attributes to the branch" do
      organization = create_active_organization()
      assert {:ok, branch} = OrgBranch.create_branch(organization, @create_branch)
      assert %OrgBranchPro{} = branch
      assert branch.org_uuid == organization.uuid
      assert branch.name == "some name"
      assert branch.address == "some address"
      assert branch.identifier == Map.get(@create_branch, :identifier)

      assert {:ok, b_attributes} =
               OrgBranch.add_branch_attribute(branch, @create_branch_attribute)

      assert %BranchAttributes{} = b_attributes
      assert b_attributes.key == "Timing"
      assert b_attributes.value == "8:00AM-6:00PM"
    end

    test "update_branch_attribute/2 updates the attributes of the branch" do
      organization = create_active_organization()
      assert {:ok, branch} = OrgBranch.create_branch(organization, @create_branch)
      assert %OrgBranchPro{} = branch
      assert branch.org_uuid == organization.uuid
      assert branch.name == "some name"
      assert branch.address == "some address"
      assert branch.identifier == Map.get(@create_branch, :identifier)

      assert {:ok, b_attributes} =
               OrgBranch.add_branch_attribute(branch, @create_branch_attribute)

      assert %BranchAttributes{} = b_attributes
      assert b_attributes.key == "Timing"
      assert b_attributes.value == "8:00AM-6:00PM"

      assert {:ok, updated_b_attributes} =
               OrgBranch.update_branch_attribute(branch, @update_branch_attribute)

      assert %BranchAttributes{} = updated_b_attributes
      assert updated_b_attributes.key == "Timing"
      assert updated_b_attributes.value == "7:30AM-2:30PM"
    end

    test "delete_branch_attribute/2 deletes the attributes of the branch" do
      organization = create_active_organization()
      assert {:ok, branch} = OrgBranch.create_branch(organization, @create_branch)
      assert %OrgBranchPro{} = branch
      assert branch.org_uuid == organization.uuid
      assert branch.name == "some name"
      assert branch.address == "some address"
      assert branch.identifier == Map.get(@create_branch, :identifier)

      assert {:ok, b_attributes} =
               OrgBranch.add_branch_attribute(branch, @create_branch_attribute)

      assert %BranchAttributes{} = b_attributes
      assert b_attributes.key == "Timing"
      assert b_attributes.value == "8:00AM-6:00PM"
      assert :ok = OrgBranch.delete_branch_attribute(branch, %{key: "Timing"})
    end
  end
end
