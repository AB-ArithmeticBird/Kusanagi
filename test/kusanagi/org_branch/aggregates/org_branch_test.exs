defmodule Kusanagi.OrgBranch.Aggregates.OrgBranchTest do
  use Kusanagi.AggregateCase, aggregate: Kusanagi.OrgBranch.Aggregates.OrgBranch

  alias Kusanagi.OrgBranch.Commands.{
    UpdateBranch,
    AddBranchAttribute,
    UpdateBranchAttribute,
    DeleteBranchAttribute,
    UpdateBranchStatus,
    InitiateBranchClosure,
    FinishBranchClosure
  }

  alias Kusanagi.OrgBranch.Events.{
    BranchCreated,
    BranchUpdated,
    BranchAttrUpdated,
    BranchAttrAdded,
    BranchAttrDeleted,
    BranchStatusUpdated,
    BranchClosureInitiated
  }

  alias Kusanagi.OrgBranch.Enums.EnumsOrgBranch

  describe "create Branch" do
    @tag :unit
    test "should succeed when valid" do
      uuid = UUID.uuid4()
      org_uuid = UUID.uuid4()

      identifier = "IDENT_#{:rand.uniform(50)}_#{:rand.uniform(80)}"

      assert_events(
        build(:create_branch, uuid: uuid, org_uuid: org_uuid, identifier: identifier),
        [
          %BranchCreated{
            uuid: uuid,
            org_uuid: org_uuid,
            name: "some name",
            branch_type: EnumsOrgBranch.org_branch_type()[:PHYSICAL],
            identifier: identifier,
            address: "some address",
            status: EnumsOrgBranch.org_branch_status()[:CREATED]
          }
        ]
      )
    end
  end

  describe "Update Branch Status Created -> Verified" do
    setup [
      :create_branch
    ]

    @tag :unit
    test "should succeed when transited to Verified", %{branch: branch} do
      assert_events(
        branch,
        %UpdateBranchStatus{
          uuid: branch.uuid,
          org_uuid: branch.org_uuid,
          status: EnumsOrgBranch.org_branch_status()[:VERIFIED]
        },
        [
          %BranchStatusUpdated{
            uuid: branch.uuid,
            org_uuid: branch.org_uuid,
            status: EnumsOrgBranch.org_branch_status()[:VERIFIED]
          }
        ]
      )
    end

    @tag :unit
    test "should fail when transited to any other status than VERIFIED", %{branch: branch} do
      assert_error(
        branch,
        %UpdateBranchStatus{
          uuid: branch.uuid,
          org_uuid: branch.org_uuid,
          status:
            EnumsOrgBranch.org_branch_status()[
              Enum.random([:CREATED, :ACTIVE, :INACTIVE, :CLOSED])
            ]
        },
        {:error, :branch_not_verified}
      )
    end
  end

  describe "Update Branch Status VERIFIED -> ACTIVE" do
    setup [
      :create_branch,
      :verify_branch
    ]

    @tag :unit
    test "should succeed when transited to ACTIVE", %{branch: branch} do
      assert_events(
        branch,
        %UpdateBranchStatus{
          uuid: branch.uuid,
          org_uuid: branch.org_uuid,
          status: EnumsOrgBranch.org_branch_status()[:ACTIVE]
        },
        [
          %BranchStatusUpdated{
            uuid: branch.uuid,
            org_uuid: branch.org_uuid,
            status: EnumsOrgBranch.org_branch_status()[:ACTIVE]
          }
        ]
      )
    end

    @tag :unit
    test "should fail when transited to any other status than ACTIVE", %{branch: branch} do
      assert_error(
        branch,
        %UpdateBranchStatus{
          uuid: branch.uuid,
          org_uuid: branch.org_uuid,
          status:
            EnumsOrgBranch.org_branch_status()[
              Enum.random([:CREATED, :VERIFIED, :INACTIVE, :CLOSED])
            ]
        },
        {:error, :branch_not_active}
      )
    end
  end

  describe "Update Branch Status ACTIVE -> INACTIVE" do
    setup [
      :create_branch,
      :verify_branch,
      :activate_branch
    ]

    @tag :unit
    test "should succeed when transited to INACTIVE", %{branch: branch} do
      assert_events(
        branch,
        %UpdateBranchStatus{
          uuid: branch.uuid,
          org_uuid: branch.org_uuid,
          status: EnumsOrgBranch.org_branch_status()[:INACTIVE]
        },
        [
          %BranchStatusUpdated{
            uuid: branch.uuid,
            org_uuid: branch.org_uuid,
            status: EnumsOrgBranch.org_branch_status()[:INACTIVE]
          }
        ]
      )
    end

    @tag :unit
    test "should fail when transited to any other status than INACTIVE", %{branch: branch} do
      assert_error(
        branch,
        %UpdateBranchStatus{
          uuid: branch.uuid,
          org_uuid: branch.org_uuid,
          status:
            EnumsOrgBranch.org_branch_status()[
              Enum.random([:CREATED, :ACTIVE, :VERIFIED, :CLOSED])
            ]
        },
        {:error, :status_update_not_allowed}
      )
    end
  end

  describe "Update Branch Status INACTIVE -> ACTIVE" do
    setup [
      :create_branch,
      :verify_branch,
      :activate_branch,
      :deactivate_branch
    ]

    @tag :unit
    test "should succeed when transited to ACTIVE", %{branch: branch} do
      assert_events(
        branch,
        %UpdateBranchStatus{
          uuid: branch.uuid,
          org_uuid: branch.org_uuid,
          status: EnumsOrgBranch.org_branch_status()[:ACTIVE]
        },
        [
          %BranchStatusUpdated{
            uuid: branch.uuid,
            org_uuid: branch.org_uuid,
            status: EnumsOrgBranch.org_branch_status()[:ACTIVE]
          }
        ]
      )
    end

    @tag :unit
    test "should fail when transited to any other status than ACTIVE", %{branch: branch} do
      assert_error(
        branch,
        %UpdateBranchStatus{
          uuid: branch.uuid,
          org_uuid: branch.org_uuid,
          status:
            EnumsOrgBranch.org_branch_status()[
              Enum.random([:CREATED, :VERIFIED, :INACTIVE, :CLOSED])
            ]
        },
        {:error, :status_update_not_allowed}
      )
    end
  end

  # # -------------------------

  describe "Any Command Should Fail if Branch Is InActive" do
    setup [
      :create_branch,
      :verify_branch,
      :activate_branch,
      :deactivate_branch
    ]

    @tag :unit
    test "UpdateBranch Command should fail", %{branch: branch} do
      assert_error(
        branch,
        %UpdateBranch{
          uuid: branch.uuid,
          name: "some updated name",
          address: "some updated address"
        },
        {:error, :branch_not_active}
      )
    end

    @tag :unit
    test "AddBranchAttribute Command should fail", %{branch: branch} do
      assert_error(
        branch,
        %AddBranchAttribute{key: "Timing", value: "7:30AM-2:30PM"},
        {:error, :branch_not_active}
      )
    end

    @tag :unit
    test "UpdateBranchAttribute Command should fail", %{branch: branch} do
      assert_error(
        branch,
        %UpdateBranchAttribute{key: "Timing", value: "9:30AM-5:30PM"},
        {:error, :branch_not_active}
      )
    end

    @tag :unit
    test "DeleteBranchAttribute Command should fail", %{branch: branch} do
      assert_error(
        branch,
        %DeleteBranchAttribute{key: "Timing"},
        {:error, :branch_not_active}
      )
    end
  end

  describe "Any Command Should Fail if Branch Is Closing status" do
    setup [
      :create_branch,
      :verify_branch,
      :activate_branch,
      :deactivate_branch,
      :initiate_closure
    ]

    @tag :unit
    test "UpdateBranch Command should fail", %{branch: branch} do
      assert_error(
        branch,
        %UpdateBranch{
          uuid: branch.uuid,
          name: "some updated name",
          address: "some updated address"
        },
        {:error, :branch_closed}
      )
    end

    @tag :unit
    test "AddBranchAttribute Command should fail", %{branch: branch} do
      assert_error(
        branch,
        %AddBranchAttribute{key: "Timing", value: "7:30AM-2:30PM"},
        {:error, :branch_closed}
      )
    end

    @tag :unit
    test "UpdateBranchAttribute Command should fail", %{branch: branch} do
      assert_error(
        branch,
        %UpdateBranchAttribute{key: "Timing", value: "9:30AM-5:30PM"},
        {:error, :branch_closed}
      )
    end

    @tag :unit
    test "DeleteBranchAttribute Command should fail", %{branch: branch} do
      assert_error(
        branch,
        %DeleteBranchAttribute{key: "Timing"},
        {:error, :branch_closed}
      )
    end
  end

  describe "Any Command Should Fail if Branch Is Closed" do
    setup [
      :create_branch,
      :verify_branch,
      :activate_branch,
      :deactivate_branch,
      :initiate_closure,
      :finish_closure
    ]

    @tag :unit
    test "UpdateBranch Command should fail", %{branch: branch} do
      assert_error(
        branch,
        %UpdateBranch{
          uuid: branch.uuid,
          name: "some updated name",
          address: "some updated address"
        },
        {:error, :branch_closed}
      )
    end

    @tag :unit
    test "AddBranchAttribute Command should fail", %{branch: branch} do
      assert_error(
        branch,
        %AddBranchAttribute{key: "Timing", value: "7:30AM-2:30PM"},
        {:error, :branch_closed}
      )
    end

    @tag :unit
    test "UpdateBranchAttribute Command should fail", %{branch: branch} do
      assert_error(
        branch,
        %UpdateBranchAttribute{key: "Timing", value: "9:30AM-5:30PM"},
        {:error, :branch_closed}
      )
    end

    @tag :unit
    test "DeleteBranchAttribute Command should fail", %{branch: branch} do
      assert_error(
        branch,
        %DeleteBranchAttribute{key: "Timing"},
        {:error, :branch_closed}
      )
    end
  end

  describe "Close branch " do
    setup [
      :create_branch
    ]

    @tag :unit
    test " should successfully close when status was Active", %{branch: branch} do
      assert_events(
        branch,
        [
          %UpdateBranchStatus{
            uuid: branch.uuid,
            org_uuid: branch.org_uuid,
            status: EnumsOrgBranch.org_branch_status()[:VERIFIED]
          },
          %UpdateBranchStatus{
            uuid: branch.uuid,
            org_uuid: branch.org_uuid,
            status: EnumsOrgBranch.org_branch_status()[:ACTIVE]
          },
          %InitiateBranchClosure{
            uuid: branch.uuid,
            org_uuid: branch.org_uuid
          }
        ],
        [
          %BranchClosureInitiated{
            uuid: branch.uuid,
            org_uuid: branch.org_uuid
          }
        ]
      )
    end

    @tag :unit
    test " should successfully close when status was InActive", %{branch: branch} do
      assert_events(
        branch,
        [
          %UpdateBranchStatus{
            uuid: branch.uuid,
            org_uuid: branch.org_uuid,
            status: EnumsOrgBranch.org_branch_status()[:VERIFIED]
          },
          %UpdateBranchStatus{
            uuid: branch.uuid,
            org_uuid: branch.org_uuid,
            status: EnumsOrgBranch.org_branch_status()[:ACTIVE]
          },
          %UpdateBranchStatus{
            uuid: branch.uuid,
            org_uuid: branch.org_uuid,
            status: EnumsOrgBranch.org_branch_status()[:INACTIVE]
          },
          %InitiateBranchClosure{
            uuid: branch.uuid,
            org_uuid: branch.org_uuid
          }
        ],
        [
          %BranchClosureInitiated{
            uuid: branch.uuid,
            org_uuid: branch.org_uuid
          }
        ]
      )
    end

    @tag :unit
    test " should failed to close when status was VERIFIED", %{branch: branch} do
      assert_error(
        branch,
        [
          %UpdateBranchStatus{
            uuid: branch.uuid,
            org_uuid: branch.org_uuid,
            status: EnumsOrgBranch.org_branch_status()[:VERIFIED]
          },
          %InitiateBranchClosure{
            uuid: branch.uuid,
            org_uuid: branch.org_uuid
          }
        ],
        {:error, :branch_not_found}
      )
    end

    @tag :unit
    test " should failed to close when status was CREATED", %{branch: branch} do
      assert_error(
        branch,
        [
          %InitiateBranchClosure{
            uuid: branch.uuid,
            org_uuid: branch.org_uuid
          }
        ],
        {:error, :branch_not_found}
      )
    end
  end

  describe "Update Branch" do
    setup [
      :create_branch
    ]

    @tag :unit
    test "should succeed when valid", %{branch: branch} do
      assert_events(
        branch,
        %UpdateBranch{
          uuid: branch.uuid,
          name: "some updated name",
          address: "some updated address"
        },
        [
          %BranchUpdated{
            uuid: branch.uuid,
            name: "some updated name",
            address: "some updated address"
          }
        ]
      )
    end
  end

  describe "Add Attribte to a Branch" do
    setup [
      :create_branch
    ]

    @tag :unit
    test "Should add attribute for valid input", %{branch: branch} do
      assert_events(branch, %AddBranchAttribute{key: "Timing", value: "7:30AM-2:30PM"}, [
        %BranchAttrAdded{
          b_uuid: branch.uuid,
          key: "Timing",
          value: "7:30AM-2:30PM"
        }
      ])
    end
  end

  describe "Update Attribte of a Branch" do
    setup [
      :create_branch,
      :verify_branch,
      :activate_branch,
      :add_branch_attribute
    ]

    @tag :unit
    test "Should Update attribute for valid input", %{branch: branch} do
      assert_events(branch, %UpdateBranchAttribute{key: "Timing", value: "9:30AM-5:30PM"}, [
        %BranchAttrUpdated{
          b_uuid: branch.uuid,
          key: "Timing",
          value: "9:30AM-5:30PM"
        }
      ])
    end
  end

  describe "Delete Attribte of a Branch" do
    setup [
      :create_branch,
      :add_branch_attribute
    ]

    @tag :unit
    test "Should Delete attribute for valid input", %{branch: branch} do
      assert_events(branch, %DeleteBranchAttribute{key: "Timing"}, [
        %BranchAttrDeleted{
          b_uuid: branch.uuid,
          key: "Timing"
        }
      ])
    end
  end

  defp create_branch(_ctx) do
    uuid = UUID.uuid4()
    org_uuid = UUID.uuid4()
    identifier = "IDENT_#{:rand.uniform(50)}_#{:rand.uniform(80)}"

    {branch, _events, _error} =
      execute(
        List.wrap(build(:create_branch, uuid: uuid, org_uuid: org_uuid, identifier: identifier))
      )

    [
      branch: branch
    ]
  end

  defp add_branch_attribute(%{branch: branch}) do
    {branch1, _events, _error} =
      execute(List.wrap(build(:add_branch_attribute, branch_uuid: branch.uuid)), branch)

    [
      branch: branch1
    ]
  end

  defp verify_branch(%{branch: branch}) do
    {branch_verified, _events, _error} =
      execute(
        List.wrap(
          build(:update_branch_status,
            uuid: branch.uuid,
            org_uuid: branch.org_uuid,
            status: EnumsOrgBranch.org_branch_status()[:VERIFIED]
          )
        ),
        branch
      )

    [
      branch: branch_verified
    ]
  end

  defp activate_branch(%{branch: branch}) do
    {branch_activated, _events, _error} =
      execute(
        List.wrap(
          build(:update_branch_status,
            uuid: branch.uuid,
            org_uuid: branch.org_uuid,
            status: EnumsOrgBranch.org_branch_status()[:ACTIVE]
          )
        ),
        branch
      )

    [
      branch: branch_activated
    ]
  end

  defp deactivate_branch(%{branch: branch}) do
    {branch_deactivated, _events, _error} =
      execute(
        List.wrap(
          build(:update_branch_status,
            uuid: branch.uuid,
            org_uuid: branch.org_uuid,
            status: EnumsOrgBranch.org_branch_status()[:INACTIVE]
          )
        ),
        branch
      )

    [
      branch: branch_deactivated
    ]
  end

  defp initiate_closure(%{branch: branch}) do
    {branch_closuer_initiated, _events, _error} =
      execute(
        List.wrap(%InitiateBranchClosure{
          uuid: branch.uuid,
          org_uuid: branch.org_uuid
        }),
        branch
      )

    [
      branch: branch_closuer_initiated
    ]
  end

  defp finish_closure(%{branch: branch}) do
    {branch_closuer_finished, _events, _error} =
      execute(
        List.wrap(%FinishBranchClosure{
          uuid: branch.uuid,
          org_uuid: branch.org_uuid
        }),
        branch
      )

    [
      branch: branch_closuer_finished
    ]
  end
end
