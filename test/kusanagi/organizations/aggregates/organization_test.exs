defmodule Kusanagi.Organizations.Aggregates.OrganizationTest do
  use Kusanagi.AggregateCase, aggregate: Kusanagi.Organizations.Aggregates.Organization

  alias Kusanagi.Organizations.Commands.{
    UpdateOrgStatus,
    UpdateOrganization,
    AddOrgAttribute,
    DeleteOrgAttribute,
    UpdateOrgAttribute,
    InitiateOrgClosure
  }

  alias Kusanagi.Organizations.Events.{
    OrganizationCreated,
    OrgStatusUpdated,
    OrganizationUpdated,
    OrgAttributeAdded,
    OrgAttributeDeleted,
    OrgAttributeUpdated,
    OrgClosureInitiated
  }

  alias Kusanagi.Organizations.Enums.EnumsOrganization

  describe "create Organization" do
    @tag :unit
    test "should succeed when valid" do
      uuid = UUID.uuid4()

      assert_events(build(:create_organization, uuid: uuid), [
        %OrganizationCreated{
          uuid: uuid,
          address: "some address",
          email: "some email",
          name: "some name",
          phone_number: "some phone_number",
          timeZone: "some timeZone",
          status: EnumsOrganization.organization_status()[:CREATED],
          website: "some website"
        }
      ])
    end
  end

  describe "Update Organization Status Created -> Verified" do
    setup [
      :create_organization
    ]

    @tag :unit
    test "should succeed when transited to Verified", %{organization: organization} do
      assert_events(
        organization,
        %UpdateOrgStatus{
          uuid: organization.uuid,
          status: EnumsOrganization.organization_status()[:VERIFIED]
        },
        [
          %OrgStatusUpdated{
            uuid: organization.uuid,
            status: EnumsOrganization.organization_status()[:VERIFIED]
          }
        ]
      )
    end

    @tag :unit
    test "should fail when transited to any other status than VERIFIED", %{
      organization: organization
    } do
      assert_error(
        organization,
        %UpdateOrgStatus{
          uuid: organization.uuid,
          status:
            EnumsOrganization.organization_status()[
              Enum.random([:CREATED, :ACTIVE, :INACTIVE, :CLOSED])
            ]
        },
        {:error, :org_not_verified}
      )
    end
  end

  describe "Update Organization Status VERIFIED -> ACTIVE" do
    setup [
      :create_organization,
      :verify_organization
    ]

    @tag :unit
    test "should succeed when transited to ACTIVE", %{organization: organization} do
      assert_events(
        organization,
        %UpdateOrgStatus{
          uuid: organization.uuid,
          status: EnumsOrganization.organization_status()[:ACTIVE]
        },
        [
          %OrgStatusUpdated{
            uuid: organization.uuid,
            status: EnumsOrganization.organization_status()[:ACTIVE]
          }
        ]
      )
    end

    @tag :unit
    test "should fail when transited to any other status than ACTIVE", %{
      organization: organization
    } do
      assert_error(
        organization,
        %UpdateOrgStatus{
          uuid: organization.uuid,
          status:
            EnumsOrganization.organization_status()[
              Enum.random([:CREATED, :VERIFIED, :INACTIVE, :CLOSED])
            ]
        },
        {:error, :org_not_active}
      )
    end
  end

  describe "Update Organization Status ACTIVE -> INACTIVE" do
    setup [
      :create_organization,
      :verify_organization,
      :activate_organization
    ]

    @tag :unit
    test "should succeed when transited to INACTIVE", %{organization: organization} do
      assert_events(
        organization,
        %UpdateOrgStatus{
          uuid: organization.uuid,
          status: EnumsOrganization.organization_status()[:INACTIVE]
        },
        [
          %OrgStatusUpdated{
            uuid: organization.uuid,
            status: EnumsOrganization.organization_status()[:INACTIVE]
          }
        ]
      )
    end

    @tag :unit
    test "should fail when transited to any other status than INACTIVE", %{
      organization: organization
    } do
      assert_error(
        organization,
        %UpdateOrgStatus{
          uuid: organization.uuid,
          status:
            EnumsOrganization.organization_status()[
              Enum.random([:CREATED, :ACTIVE, :VERIFIED, :CLOSED])
            ]
        },
        {:error, :status_update_not_allowed}
      )
    end
  end

  describe "Update Organization Status INACTIVE -> ACTIVE" do
    setup [
      :create_organization,
      :verify_organization,
      :activate_organization,
      :deactivate_organization
    ]

    @tag :unit
    test "should succeed when transited to ACTIVE", %{organization: organization} do
      assert_events(
        organization,
        %UpdateOrgStatus{
          uuid: organization.uuid,
          status: EnumsOrganization.organization_status()[:ACTIVE]
        },
        [
          %OrgStatusUpdated{
            uuid: organization.uuid,
            status: EnumsOrganization.organization_status()[:ACTIVE]
          }
        ]
      )
    end

    @tag :unit
    test "should fail when transited to any other status than ACTIVE", %{
      organization: organization
    } do
      assert_error(
        organization,
        %UpdateOrgStatus{
          uuid: organization.uuid,
          status:
            EnumsOrganization.organization_status()[
              Enum.random([:CREATED, :VERIFIED, :INACTIVE, :CLOSED])
            ]
        },
        {:error, :status_update_not_allowed}
      )
    end
  end

  describe "Update Organization" do
    setup [
      :create_organization
    ]

    @tag :unit
    test "Should update Organization for valid input", %{organization: organization} do
      assert_events(
        organization,
        %UpdateOrganization{
          uuid: organization.uuid,
          address: "some updated address",
          email: "some updated email",
          name: "some updated name",
          phone_number: "some updated phone_number",
          timeZone: "some updated timeZone",
          website: "some updated website"
        },
        [
          %OrganizationUpdated{
            uuid: organization.uuid,
            address: "some updated address",
            email: "some updated email",
            name: "some updated name",
            phone_number: "some updated phone_number",
            timeZone: "some updated timeZone",
            website: "some updated website"
          }
        ]
      )
    end
  end

  describe "Command Should Fail if Organization Is InActive" do
    setup [
      :create_organization,
      :verify_organization,
      :activate_organization,
      :deactivate_organization
    ]

    @tag :unit
    test "UpdateOrganization Command should fail", %{organization: organization} do
      assert_error(
        organization,
        %UpdateOrganization{
          uuid: organization.uuid,
          address: "some updated address",
          email: "some updated email",
          name: "some updated name",
          phone_number: "some updated phone_number",
          timeZone: "some updated timeZone",
          website: "some updated website"
        },
        {:error, :org_not_active}
      )
    end

    @tag :unit
    test "AddBranchAttribute Command should fail", %{organization: organization} do
      assert_error(
        organization,
        %AddOrgAttribute{key: "Timing", value: "7:30AM-2:30PM"},
        {:error, :org_not_active}
      )
    end

    @tag :unit
    test "UpdateBranchAttribute Command should fail", %{organization: organization} do
      assert_error(
        organization,
        %UpdateOrgAttribute{key: "Timing", value: "9:30AM-5:30PM"},
        {:error, :org_not_active}
      )
    end

    @tag :unit
    test "DeleteBranchAttribute Command should fail", %{organization: organization} do
      assert_error(
        organization,
        %DeleteOrgAttribute{key: "Timing"},
        {:error, :org_not_active}
      )
    end
  end

  describe "Close Organization " do
    setup [
      :create_organization
    ]

    @tag :unit
    test " should successfully close when status was Active", %{organization: organization} do
      assert_events(
        organization,
        [
          %UpdateOrgStatus{
            uuid: organization.uuid,
            status: EnumsOrganization.organization_status()[:VERIFIED]
          },
          %UpdateOrgStatus{
            uuid: organization.uuid,
            status: EnumsOrganization.organization_status()[:ACTIVE]
          },
          %InitiateOrgClosure{
            uuid: organization.uuid
          }
        ],
        [
          %OrgClosureInitiated{
            uuid: organization.uuid
          }
        ]
      )
    end

    @tag :unit
    test " should successfully close when status was InActive", %{organization: organization} do
      assert_events(
        organization,
        [
          %UpdateOrgStatus{
            uuid: organization.uuid,
            status: EnumsOrganization.organization_status()[:VERIFIED]
          },
          %UpdateOrgStatus{
            uuid: organization.uuid,
            status: EnumsOrganization.organization_status()[:ACTIVE]
          },
          %UpdateOrgStatus{
            uuid: organization.uuid,
            status: EnumsOrganization.organization_status()[:INACTIVE]
          },
          %InitiateOrgClosure{
            uuid: organization.uuid
          }
        ],
        [
          %OrgClosureInitiated{
            uuid: organization.uuid
          }
        ]
      )
    end

    @tag :unit
    test " should failed to close when status was VERIFIED", %{organization: organization} do
      assert_error(
        organization,
        [
          %UpdateOrgStatus{
            uuid: organization.uuid,
            status: EnumsOrganization.organization_status()[:VERIFIED]
          },
          %InitiateOrgClosure{
            uuid: organization.uuid
          }
        ],
        {:error, :org_not_found}
      )
    end

    @tag :unit
    test " should failed to close when status was CREATED", %{organization: organization} do
      assert_error(
        organization,
        [
          %InitiateOrgClosure{
            uuid: organization.uuid
          }
        ],
        {:error, :org_not_found}
      )
    end
  end

  # # ----------------------------------------------
  describe "Add Attribte to Organization" do
    setup [
      :create_organization
    ]

    @tag :unit
    test "Should add attribute for valid input", %{organization: organization} do
      assert_events(organization, %AddOrgAttribute{key: "Timing", value: "7:30AM-2:30PM"}, [
        %OrgAttributeAdded{
          uuid: organization.uuid,
          key: "Timing",
          value: "7:30AM-2:30PM"
        }
      ])
    end
  end

  describe "Update Attribte of an Organization" do
    setup [
      :create_organization,
      :verify_organization,
      :activate_organization,
      :add_org_attribute
    ]

    @tag :unit
    test "Should Update attribute for valid input", %{organization: organization} do
      assert_events(
        organization,
        %UpdateOrgAttribute{key: "Timing", value: "9:30AM-5:30PM"},
        [
          %OrgAttributeUpdated{
            uuid: organization.uuid,
            key: "Timing",
            value: "9:30AM-5:30PM"
          }
        ]
      )
    end
  end

  describe "Delete Attribte of an Organization" do
    setup [
      :create_organization,
      :add_org_attribute
    ]

    @tag :unit
    test "Should Delete attribute for valid input", %{organization: organization} do
      assert_events(organization, %DeleteOrgAttribute{key: "Timing"}, [
        %OrgAttributeDeleted{
          uuid: organization.uuid,
          key: "Timing"
        }
      ])
    end
  end

  defp create_organization(_context) do
    uuid = UUID.uuid4()
    {organization, _events, _error} = execute(List.wrap(build(:create_organization, uuid: uuid)))

    [
      organization: organization
    ]
  end

  defp add_org_attribute(%{organization: organization}) do
    {organization1, _events, _error} =
      execute(List.wrap(build(:add_org_attribute, org_uuid: organization.uuid)), organization)

    [
      organization: organization1
    ]
  end

  defp verify_organization(%{organization: organization}) do
    {organization_verified, _events, _error} =
      execute(
        List.wrap(
          build(:update_organization_status,
            uuid: organization.uuid,
            status: EnumsOrganization.organization_status()[:VERIFIED]
          )
        ),
        organization
      )

    [
      organization: organization_verified
    ]
  end

  defp activate_organization(%{organization: organization}) do
    {organization_activated, _events, _error} =
      execute(
        List.wrap(
          build(:update_organization_status,
            uuid: organization.uuid,
            status: EnumsOrganization.organization_status()[:ACTIVE]
          )
        ),
        organization
      )

    [
      organization: organization_activated
    ]
  end

  defp deactivate_organization(%{organization: organization}) do
    {organization_deactivated, _events, _error} =
      execute(
        List.wrap(
          build(:update_organization_status,
            uuid: organization.uuid,
            status: EnumsOrganization.organization_status()[:INACTIVE]
          )
        ),
        organization
      )

    [
      organization: organization_deactivated
    ]
  end
end
