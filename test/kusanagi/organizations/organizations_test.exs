defmodule Kusanagi.OrganizationsTest do
  use Kusanagi.DataCase

  alias Kusanagi.Organizations
  alias Kusanagi.Organizations.Enums.EnumsOrganization

  describe "organizations" do
    alias Kusanagi.Organizations.Projections.Organization
    alias Kusanagi.Organizations.Projections.OrgAttributes

    @valid_attrs %{
      address: "some address",
      attribute: "some attribute",
      email: "some email",
      name: "some name",
      phone_number: "some phone_number",
      timeZone: "some timeZone",
      website: "some website",
      status: 1
    }
    @update_attrs %{
      address: "some updated address",
      attribute: "some updated attribute",
      email: "some updated email",
      name: "some updated name",
      phone_number: "some updated phone_number",
      timeZone: "some updated timeZone",
      website: "some updated website"
    }
    @invalid_attrs %{
      address: nil,
      attribute: nil,
      email: nil,
      name: nil,
      phone_number: nil,
      timeZone: nil,
      website: nil
    }
    @create_org_attribute %{key: "Timing", value: "8:00AM-6:00PM"}
    @update_org_attribute %{key: "Timing", value: "7:30AM-2:30PM"}

    @org_verified_status EnumsOrganization.organization_status()[:VERIFIED]
    @org_active_status EnumsOrganization.organization_status()[:ACTIVE]
    @org_inactive_status EnumsOrganization.organization_status()[:INACTIVE]

    def organization_fixture(attrs \\ %{}) do
      {:ok, organization} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Organizations.create_organization()

      organization
    end

    test "list_organizations/0 returns all organizations" do
      organization = organization_fixture()
      assert Organizations.list_organizations() == [organization]
    end

    test "get_organization!/1 returns the organization with given uuid" do
      organization = organization_fixture()
      assert Organizations.get_organization!(organization.uuid) == organization
    end

    test "create_organization/1 with valid data creates a organization" do
      assert {:ok, %Organization{} = organization} =
               Organizations.create_organization(@valid_attrs)

      assert organization.address == "some address"
      assert organization.email == "some email"
      assert organization.name == "some name"
      assert organization.phone_number == "some phone_number"
      assert organization.timeZone == "some timeZone"
      assert organization.status == 1
      assert organization.website == "some website"
    end

    test "create_organization/1 with invalid data returns error changeset" do
      assert {:error, :validation_failure, _errors} =
               Organizations.create_organization(@invalid_attrs)
    end

    test "update_organization/2 with valid data updates the organization" do
      organization = organization_fixture()
      assert {:ok, organization} = Organizations.update_organization(organization, @update_attrs)
      assert %Organization{} = organization
      assert organization.address == "some updated address"
      assert organization.email == "some updated email"
      assert organization.name == "some updated name"
      assert organization.phone_number == "some updated phone_number"
      assert organization.timeZone == "some updated timeZone"
      assert organization.status == 1
      assert organization.website == "some updated website"
    end

    test "update_organization/2 with invalid data returns error changeset" do
      organization = organization_fixture()

      assert {:error, :validation_failure, _errors} =
               Organizations.update_organization(organization, @invalid_attrs)

      assert organization == Organizations.get_organization!(organization.uuid)
    end

    test "update_organization/2 with valid data should fail when status is inactive" do
      organization = organization_fixture()

      assert {:ok, %Organization{status: @org_verified_status}} =
               Organizations.update_status(organization, %{"status" => "VERIFIED"})

      assert {:ok, %Organization{status: @org_active_status}} =
               Organizations.update_status(organization, %{"status" => "ACTIVE"})

      assert {:ok, %Organization{status: @org_inactive_status}} =
               Organizations.update_status(organization, %{"status" => "INACTIVE"})

      assert {:error, :org_not_active} =
               Organizations.update_organization(organization, @update_attrs)
    end

    test "close_organization/1 closes the organization when status was ACTIVE" do
      organization = organization_fixture()

      assert {:ok, %Organization{status: @org_verified_status}} =
               Organizations.update_status(organization, %{"status" => "VERIFIED"})

      assert {:ok, %Organization{status: @org_active_status}} =
               Organizations.update_status(organization, %{"status" => "ACTIVE"})

      assert {:ok, %Organization{} = u_organization} =
               Organizations.close_organization(organization)

      assert u_organization.status == EnumsOrganization.organization_status()[:CLOSING]
    end

    test "close_organization/1 closes the organization when status was INACTIVE" do
      organization = organization_fixture()

      assert {:ok, %Organization{status: @org_verified_status}} =
               Organizations.update_status(organization, %{"status" => "VERIFIED"})

      assert {:ok, %Organization{status: @org_active_status}} =
               Organizations.update_status(organization, %{"status" => "ACTIVE"})

      assert {:ok, %Organization{status: @org_inactive_status}} =
               Organizations.update_status(organization, %{"status" => "INACTIVE"})

      assert {:ok, %Organization{} = u_organization} =
               Organizations.close_organization(organization)

      assert u_organization.status == EnumsOrganization.organization_status()[:CLOSING]
    end

    test "close_organization/1 should fail to close the organization when status was VERIFIED" do
      organization = organization_fixture()

      assert {:ok, %Organization{status: @org_verified_status}} =
               Organizations.update_status(organization, %{"status" => "VERIFIED"})

      assert {:error, :org_not_found} = Organizations.close_organization(organization)
    end

    test "close_organization/1 should fail to close the organization when status was CREATED" do
      organization = organization_fixture()

      assert {:error, :org_not_found} = Organizations.close_organization(organization)
    end

    test "update_org_status/1 should trasit from Created-> Verified status" do
      organization = organization_fixture()

      assert {:ok, %Organization{status: @org_verified_status}} =
               Organizations.update_status(organization, %{"status" => "VERIFIED"})
    end

    test "update_org_status/1 should fail to trasit to status other than VERIFED from CREATED status" do
      organization = organization_fixture()

      assert {:error, :org_not_verified} =
               Organizations.update_status(organization, %{
                 "status" => Enum.random(["CREATED", "ACTIVE", "INACTIVE", "CLOSED"])
               })
    end

    test "update_org_status/1 should trasit from VERIFIED-> ACTIVE" do
      organization = organization_fixture()

      assert {:ok, %Organization{status: @org_verified_status}} =
               Organizations.update_status(organization, %{"status" => "VERIFIED"})

      assert {:ok, %Organization{status: @org_active_status}} =
               Organizations.update_status(organization, %{"status" => "ACTIVE"})
    end

    test "update_org_status/1 should fail to trasit to status other than ACTIVE from VERIFIED status" do
      organization = organization_fixture()

      assert {:ok, %Organization{status: @org_verified_status}} =
               Organizations.update_status(organization, %{"status" => "VERIFIED"})

      assert {:error, :org_not_active} =
               Organizations.update_status(organization, %{
                 "status" => Enum.random(["CREATED", "VERIFIED", "INACTIVE", "CLOSED"])
               })
    end

    test "update_org_status/1 should trasit from ACTIVE-> INACTIVE" do
      organization = organization_fixture()

      assert {:ok, %Organization{status: @org_verified_status}} =
               Organizations.update_status(organization, %{"status" => "VERIFIED"})

      assert {:ok, %Organization{status: @org_active_status}} =
               Organizations.update_status(organization, %{"status" => "ACTIVE"})

      assert {:ok, %Organization{status: @org_inactive_status}} =
               Organizations.update_status(organization, %{"status" => "INACTIVE"})
    end

    test "update_org_status/1 should fail to trasit to status other than INACTIVE from ACTIVE status" do
      organization = organization_fixture()

      assert {:ok, %Organization{status: @org_verified_status}} =
               Organizations.update_status(organization, %{"status" => "VERIFIED"})

      assert {:ok, %Organization{status: @org_active_status}} =
               Organizations.update_status(organization, %{"status" => "ACTIVE"})

      assert {:error, :status_update_not_allowed} =
               Organizations.update_status(organization, %{
                 "status" => Enum.random(["CREATED", "VERIFIED", "ACTIVE", "CLOSED"])
               })
    end

    test "update_org_status/1 should trasit from INACTIVE-> ACTIVE" do
      organization = organization_fixture()

      assert {:ok, %Organization{status: @org_verified_status}} =
               Organizations.update_status(organization, %{"status" => "VERIFIED"})

      assert {:ok, %Organization{status: @org_active_status}} =
               Organizations.update_status(organization, %{"status" => "ACTIVE"})

      assert {:ok, %Organization{status: @org_inactive_status}} =
               Organizations.update_status(organization, %{"status" => "INACTIVE"})

      assert {:ok, %Organization{status: @org_active_status}} =
               Organizations.update_status(organization, %{"status" => "ACTIVE"})
    end

    test "update_org_status/1 should fail to trasit to status other than ACTIVE from INACTIVE status" do
      organization = organization_fixture()

      assert {:ok, %Organization{status: @org_verified_status}} =
               Organizations.update_status(organization, %{"status" => "VERIFIED"})

      assert {:ok, %Organization{status: @org_active_status}} =
               Organizations.update_status(organization, %{"status" => "ACTIVE"})

      assert {:ok, %Organization{status: @org_inactive_status}} =
               Organizations.update_status(organization, %{"status" => "INACTIVE"})

      assert {:error, :status_update_not_allowed} =
               Organizations.update_status(organization, %{
                 "status" => Enum.random(["CREATED", "VERIFIED", "INACTIVE", "CLOSED"])
               })
    end

    test "add_organization_attribute/1 adds the attributes to the organization" do
      organization = organization_fixture()

      assert {:ok, org_attributes} =
               Organizations.add_org_attribute(organization, @create_org_attribute)

      assert %OrgAttributes{} = org_attributes
      assert org_attributes.key == "Timing"
      assert org_attributes.value == "8:00AM-6:00PM"
    end

    test "update_organization_attribute/2 updates the attributes of the organization" do
      organization = organization_fixture()

      assert {:ok, org_attributes} =
               Organizations.add_org_attribute(organization, @create_org_attribute)

      assert %OrgAttributes{} = org_attributes
      assert org_attributes.key == "Timing"
      assert org_attributes.value == "8:00AM-6:00PM"

      assert {:ok, updated_org_attributes} =
               Organizations.update_org_attribute(organization, @update_org_attribute)

      assert %OrgAttributes{} = updated_org_attributes
      assert updated_org_attributes.key == "Timing"
      assert updated_org_attributes.value == "7:30AM-2:30PM"
    end

    test "delete_organization_attribute/2 updates the attributes of the organization" do
      organization = organization_fixture()

      assert {:ok, org_attributes} =
               Organizations.add_org_attribute(organization, @create_org_attribute)

      assert %OrgAttributes{} = org_attributes
      assert org_attributes.key == "Timing"
      assert org_attributes.value == "8:00AM-6:00PM"
      assert :ok = Organizations.delete_org_attribute(organization, %{key: "Timing"})
    end
  end
end
