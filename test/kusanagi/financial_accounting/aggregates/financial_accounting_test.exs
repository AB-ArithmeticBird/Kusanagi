defmodule Kusanagi.FinancialAccounting.Aggregates.FAccountTest do
  use Kusanagi.AggregateCase, aggregate: Kusanagi.FinancialAccounting.Aggregates.FAccount

  alias Kusanagi.FinancialAccounting.Events.{
    FAccountCreated,
    FAccountCredited,
    FAccountDebited,
    FAccountClosed,
    FAccountMigrated
  }

  alias Kusanagi.FinancialAccounting.Enums.EnumsFAcoount

  alias Kusanagi.FinancialAccounting.Commands.{
    CreditFAccount,
    DebitFAccount,
    CloseFAccount,
    MigrateFAccount,
    CreateFAccount
  }

  describe "create account" do
    @tag :unit
    test "should succeed when valid" do
      user_uuid = UUID.uuid4()
      uuid = UUID.uuid4()
      org_uuid = UUID.uuid4()
      branch_uuid = UUID.uuid4()

      assert_events(
        build(:create_f_account,
          user_uuid: user_uuid,
          uuid: uuid,
          branch_id: branch_uuid,
          org_id: org_uuid
        ),
        [
          %FAccountCreated{
            uuid: uuid,
            account_type: EnumsFAcoount.account_type()[:asset],
            account_name: "cash account",
            description: "Cash account of the bank",
            user_uuid: user_uuid,
            status: EnumsFAcoount.account_status()[:OPEN],
            currency: "USD",
            branch_id: branch_uuid,
            org_id: org_uuid
          }
        ]
      )
    end
  end

  describe "Close Account" do
    setup [
      :create_f_account
    ]

    @tag :unit
    test "Should Close the account", %{f_account: f_account} do
      assert_events(f_account, %CloseFAccount{uuid: f_account.uuid}, [
        %FAccountClosed{
          uuid: f_account.uuid,
          branch_id: f_account.branch_id,
          org_id: f_account.org_id
        }
      ])
    end
  end

  describe "credit Account" do
    setup [
      :create_liability_f_account
    ]

    @tag :unit
    test "Should Credit the balance for valid input", %{f_account: f_account} do
      amount = Enum.random(5000..5000_0)

      assert_events(f_account, %CreditFAccount{uuid: f_account.uuid, amount: amount}, [
        %FAccountCredited{
          uuid: f_account.uuid,
          amount: amount
        }
      ])
    end

    @tag :unit
    test "Should return error for insufficient balance", %{f_account: f_account} do
      amount = Enum.random(5000..5000_0)

      assert_error(
        f_account,
        %DebitFAccount{uuid: f_account.uuid, amount: amount},
        {:error, :f_account_insufficient_balanace}
      )
    end
  end

  describe "debit Account" do
    setup [
      :create_f_account
    ]

    @tag :unit
    test "Should Debit the balance for valid input", %{f_account: f_account} do
      amount = Enum.random(5000..5000_0)

      assert_events(f_account, %DebitFAccount{uuid: f_account.uuid, amount: amount}, [
        %FAccountDebited{
          uuid: f_account.uuid,
          amount: amount
        }
      ])
    end

    @tag :unit
    test "Should Credit the balance for valid input", %{f_account: f_account} do
      amount = Enum.random(5000..5000_0)

      assert_error(
        f_account,
        %CreditFAccount{uuid: f_account.uuid, amount: amount},
        {:error, :f_account_insufficient_balanace}
      )
    end
  end

  describe "Migrate Account" do
    setup [
      :create_f_account
    ]

    @tag :unit
    test "Should Migrate the account for valid input", %{f_account: f_account} do
      new_branch_id = UUID.uuid4()

      assert_events(
        f_account,
        %MigrateFAccount{
          uuid: f_account.uuid,
          old_branch_id: f_account.branch_id,
          new_branch_id: new_branch_id,
          new_org_id: f_account.org_id
        },
        [
          %FAccountMigrated{
            uuid: f_account.uuid,
            old_branch_id: f_account.branch_id,
            new_branch_id: new_branch_id
          }
        ]
      )
    end
  end

  defp create_f_account(%CreateFAccount{} = create) do
    {f_account, _events, _error} = execute(List.wrap(create))

    [
      f_account: f_account
    ]
  end

  defp create_f_account(_context) do
    user_uuid = UUID.uuid4()
    uuid = UUID.uuid4()
    org_uuid = UUID.uuid4()
    branch_uuid = UUID.uuid4()

    {f_account, _events, _error} =
      execute(
        List.wrap(
          build(:create_f_account,
            user_uuid: user_uuid,
            uuid: uuid,
            branch_id: branch_uuid,
            org_id: org_uuid
          )
        )
      )

    [
      f_account: f_account
    ]
  end

  defp create_liability_f_account(_context) do
    user_uuid = UUID.uuid4()
    uuid = UUID.uuid4()
    org_uuid = UUID.uuid4()
    branch_uuid = UUID.uuid4()

    {f_account, _events, _error} =
      execute(
        List.wrap(
          build(:create_liability_f_account,
            user_uuid: user_uuid,
            uuid: uuid,
            branch_id: branch_uuid,
            org_id: org_uuid
          )
        )
      )

    [
      f_account: f_account
    ]
  end
end
