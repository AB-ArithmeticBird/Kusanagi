defmodule Kusanagi.FinancialAccountingTest do
  use Kusanagi.DataCase

  alias Kusanagi.FinancialAccounting

  describe "faccounts" do
    alias Kusanagi.FinancialAccounting.Projections.FAccount

    @valid_attrs %{
      account_name: "some account_name",
      account_type: 1,
      balance: 12,
      description: "some description",
      user_uuid: nil,
      currency: "USD",
      org_id: UUID.uuid4(),
      branch_id: UUID.uuid4()
    }

    defp setup_org_and_branch() do
      uuid = Ecto.UUID.generate()
      org_uuid = Ecto.UUID.generate()
      branch_uuid = Ecto.UUID.generate()
      add_user(uuid)
      add_org(org_uuid)
      add_org_branch(org_uuid, branch_uuid, Enum.random(1..1_000))

      @valid_attrs
      |> Map.put(:user_uuid, uuid)
      |> Map.put(:org_id, org_uuid)
      |> Map.put(:branch_id, branch_uuid)
    end

    def f_account_fixture(attrs \\ %{}) do
      f = attrs |> Enum.into(setup_org_and_branch())
      {:ok, f_account} = f |> FinancialAccounting.create_f_account()

      f_account
    end

    @tag :integration
    test "get_f_account!/1 returns the f_account with given id" do
      f_account = f_account_fixture()
      assert FinancialAccounting.get_f_account!(f_account.uuid) == f_account
    end

    @tag :integration
    test "create_f_account/1 with valid data creates a f_account" do
      assert {:ok, %FAccount{} = f_account} =
               setup_org_and_branch()
               |> FinancialAccounting.create_f_account()

      assert f_account.account_name == "some account_name"
      assert f_account.balance == 0
      assert f_account.description == "some description"
    end

    @tag :integration
    test "create_f_account/1 with invalid account name returns error" do
      assert {:error, :validation_failure, errors} =
               setup_org_and_branch()
               |> Map.put(:account_name, "")
               |> FinancialAccounting.create_f_account()

      assert errors == %{account_name: ["can't be empty"]}
    end

    @tag :integration
    test "create_f_account/1 with invalid currency returns error" do
      assert {:error, :validation_failure, errors} =
               setup_org_and_branch()
               |> Map.delete(:currency)
               |> FinancialAccounting.create_f_account()

      assert errors == %{currency: ["can't be empty", "must be valid"]}
    end

    @tag :integration
    test "credit_f_account/1 with valid data credit amount" do
      f_account = f_account_fixture()
      amount = Enum.random(10000..10000_0)

      assert {:ok, %FAccount{} = f_account_2} =
               FinancialAccounting.credit_f_account(%{
                 "uuid" => f_account.uuid,
                 "amount" => amount
               })

      account = FinancialAccounting.get_f_account!(f_account_2.uuid)
      assert f_account.uuid == account.uuid
      assert f_account.balance + amount == account.balance
      assert f_account.user_uuid == account.user_uuid
    end

    @tag :integration
    test "debit_f_account/1 from insufficent balance account should return error" do
      f_account = f_account_fixture()
      amount = Enum.random(10000..10000_0)

      assert {:error, :f_account_insufficient_balanace} =
               FinancialAccounting.debit_f_account(%{"uuid" => f_account.uuid, "amount" => amount})
    end

    @tag :integration
    test "credit and debit same amount should result in zero balance For Credit As Normal Balance" do
      # Normal Balance is term used transaction type for which in account balance is increased
      account_type = Enum.random([1, 4, 5, 6, 8, 10])
      with_random_account_type = Map.put(setup_org_and_branch(), :account_type, account_type)
      f = %{} |> Enum.into(with_random_account_type)
      {:ok, f_account} = f |> FinancialAccounting.create_f_account()

      amount = Enum.random(10000..10000_0)

      assert {:ok, %FAccount{} = f_account_2} =
               FinancialAccounting.credit_f_account(%{
                 "uuid" => f_account.uuid,
                 "amount" => amount
               })

      account = FinancialAccounting.get_f_account!(f_account_2.uuid)
      assert f_account.uuid == account.uuid
      assert f_account.balance + amount == account.balance

      assert {:ok, %FAccount{} = f_account_3} =
               FinancialAccounting.debit_f_account(%{"uuid" => f_account.uuid, "amount" => amount})

      account = FinancialAccounting.get_f_account!(f_account_3.uuid)

      assert f_account.uuid == account.uuid
      assert f_account.balance == account.balance
    end

    @tag :integration
    test "credit and debit same amount should result in zero balance For Debit As Normal Balance" do
      # Normal Balance is term used transaction type for which in account balance is increased
      account_type = Enum.random([2, 3, 7, 9, 11, 12])
      with_random_account_type = Map.put(setup_org_and_branch(), :account_type, account_type)
      f = %{} |> Enum.into(with_random_account_type)
      {:ok, f_account} = f |> FinancialAccounting.create_f_account()

      amount = Enum.random(10000..10000_0)

      assert {:ok, %FAccount{} = f_account_3} =
               FinancialAccounting.debit_f_account(%{"uuid" => f_account.uuid, "amount" => amount})

      account = FinancialAccounting.get_f_account!(f_account_3.uuid)

      assert f_account.uuid == account.uuid
      assert f_account.balance + amount == account.balance

      assert {:ok, %FAccount{} = f_account_2} =
               FinancialAccounting.credit_f_account(%{
                 "uuid" => f_account.uuid,
                 "amount" => amount
               })

      account = FinancialAccounting.get_f_account!(f_account_2.uuid)

      assert f_account.uuid == account.uuid
      assert f_account.balance == account.balance
    end

    @tag :integration
    test "migrate account should succeed" do
      f_account = f_account_fixture()
      new_branch_uuid = UUID.uuid4()
      add_org_branch(f_account.org_id, new_branch_uuid, Enum.random(1..1_000))

      assert {:ok, %FAccount{} = f_account2} =
               FinancialAccounting.migrate_f_account(%{
                 "uuid" => f_account.uuid,
                 "new_branch_id" => new_branch_uuid,
                 "old_branch_id" => f_account.branch_id,
                 "new_org_id" => f_account.org_id
               })

      assert f_account2.uuid == f_account.uuid
      assert f_account2.branch_id == new_branch_uuid
      assert f_account2.org_id == f_account.org_id
    end

    defp add_user(uuid) do
      readstore_config = Application.get_env(:kusanagi, Kusanagi.Repo)

      {:ok, conn} = Postgrex.start_link(readstore_config)

      Postgrex.query!(conn, add_user_in_user_account_table(uuid), [])
    end

    defp add_org(uuid) do
      readstore_config = Application.get_env(:kusanagi, Kusanagi.Repo)

      {:ok, conn} = Postgrex.start_link(readstore_config)

      Postgrex.query!(conn, add_org_in_organization_table(uuid), [])
    end

    defp add_org_branch(org_uuid, branch_uuid, randome) do
      readstore_config = Application.get_env(:kusanagi, Kusanagi.Repo)

      {:ok, conn} = Postgrex.start_link(readstore_config)

      Postgrex.query!(conn, add_branch_in_org_branch_table(org_uuid, branch_uuid, randome), [])
    end

    defp add_user_in_user_account_table(uuid) do
      """
      INSERT INTO public.accounts_users (uuid, username, email, hashed_password, bio, image, inserted_at, updated_at)
        VALUES ('#{uuid}', 'jake', 'jake@jake.jake',
        '$2b$12$kfAEBsoqUu8Gpw6Ju16BveVI9Gctx3IDT9c4cw0cx.h148QhPhLpO', null, null,
        '2018-03-02 07:39:53.629510', '2018-03-02 07:39:53.629522');
      """
    end

    defp add_org_in_organization_table(uuid) do
      """
      INSERT INTO public.organizations (uuid, name, email, phone_number, address, status, website, inserted_at, updated_at)
        VALUES ('#{uuid}', 'Kusanagi', 'support@kusanagi.com', '012-0345-567',
        'ABCD Area, ABCD Country', 3, null,
        '2018-03-02 07:39:53.629510', '2018-03-02 07:39:53.629522');
      """
    end

    defp add_branch_in_org_branch_table(org_uuid, branch_uuid, randome) do
      """
      INSERT INTO public.org_branch (uuid, name, branch_type, identifier, org_uuid, address, status, inserted_at, updated_at)
        VALUES ('#{branch_uuid}', 'Kusanagi_Singapore', 1, 'KUSANAGI_SGP_#{randome}', '#{org_uuid}',
        'ABCD Area, ABCD Country', 3,
        '2018-03-02 07:39:53.629510', '2018-03-02 07:39:53.629522');
      """
    end
  end
end
