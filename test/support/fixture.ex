defmodule Kusanagi.Fixture do
  import Kusanagi.Factory

  alias Kusanagi.Accounts
  alias Kusanagi.FinancialAccounting
  alias Kusanagi.Organizations
  alias Kusanagi.OrgBranch

  def register_user(_ctx) do
    {:ok, user} = fixture(:user)

    [
      user: user
    ]
  end

  def register_user2(%{branch: branch}) do
    {:ok, user} = fixture(:user)

    [
      branch: branch,
      user: user
    ]
  end

  def create_f_account(%{user: user}) do
    {:ok, f_account} = fixture(:f_account, user_uuid: user.uuid)

    [
      user: user,
      f_account: f_account
    ]
  end

  def create_f_account2(%{user: user, branch: branch}) do
    {:ok, f_account} =
      fixture(:f_account, user_uuid: user.uuid, org_id: branch.org_uuid, branch_id: branch.uuid)

    [
      user: user,
      f_account: f_account
    ]
  end

  def debit_f_account(%{user: user, f_account: f_account}) do
    {:ok, f_account} = fixture(:d_f_account, %{"uuid" => f_account.uuid, "amount" => 100})

    [
      user: user,
      f_account: f_account
    ]
  end

  def create_organization(_c) do
    {:ok, organization} = fixture(:organization, [])

    [
      organization: organization
    ]
  end

  def verify_org(%{organization: organization}) do
    {:ok, organization} = fixture(:u_status_organization, organization, %{"status" => "VERIFIED"})

    [
      organization: organization
    ]
  end

  def activate_org(%{organization: organization}) do
    {:ok, organization} = fixture(:u_status_organization, organization, %{"status" => "ACTIVE"})

    [
      organization: organization
    ]
  end

  def inactivate_org(%{organization: organization}) do
    {:ok, organization} = fixture(:u_status_organization, organization, %{"status" => "INACTIVE"})

    [
      organization: organization
    ]
  end

  def close_org(%{organization: organization}) do
    {:ok, organization} = fixture(:close_org, organization, [])

    [
      organization: organization
    ]
  end

  def create_organization_for_attr(_c) do
    {:ok, organization} = fixture(:organization, [])

    [
      organization: organization
    ]
  end

  def add_org_attribute(%{organization: organization}) do
    {:ok, org_attributes} = fixture(:add_org_attribute, organization, [])

    [
      organization: organization,
      org_attributes: org_attributes
    ]
  end

  def add_branch_attribute(%{branch: branch}) do
    {:ok, b_attributes} = fixture(:add_branch_attribute, branch, [])

    [
      branch: branch,
      b_attributes: b_attributes
    ]
  end

  def create_branch(%{organization: organization}) do
    {:ok, branch} = fixture(:create_branch, organization, [])

    [
      branch: branch
    ]
  end

  def verify_branch(%{branch: branch}) do
    {:ok, branch} = fixture(:u_status_branch, branch, %{"status" => "VERIFIED"})

    [
      branch: branch
    ]
  end

  def activate_branch(%{branch: branch}) do
    {:ok, branch} = fixture(:u_status_branch, branch, %{"status" => "ACTIVE"})

    [
      branch: branch
    ]
  end

  def inactivate_branch(%{branch: branch}) do
    {:ok, branch} = fixture(:u_status_branch, branch, %{"status" => "INACTIVE"})

    [
      branch: branch
    ]
  end

  def close_branch(%{branch: branch}) do
    {:ok, branch} = fixture(:close_branch, branch, [])

    [
      branch: branch
    ]
  end

  def fixture(resource, attrs \\ [])

  def fixture(:user, attrs) do
    build(:user, attrs) |> Accounts.register_user()
  end

  def fixture(:f_account, attrs) do
    build(:f_account, attrs) |> FinancialAccounting.create_f_account()
  end

  def fixture(:d_f_account, attrs) do
    attrs |> FinancialAccounting.debit_f_account()
  end

  def fixture(:organization, attrs) do
    build(:organization, attrs) |> Organizations.create_organization()
  end

  def fixture(:u_status_organization, organisation, attrs) do
    organisation |> Organizations.update_status(attrs)
  end

  def fixture(:close_org, organisation, attrs) do
    organisation |> Organizations.close_organization()
  end

  def fixture(:add_org_attribute, organization, attrs) do
    organization |> Organizations.add_org_attribute(build(:org_attr, attrs))
  end

  def fixture(:add_branch_attribute, branch, attrs) do
    branch |> OrgBranch.add_branch_attribute(build(:org_attr, attrs))
  end

  def fixture(:create_branch, organization, attrs) do
    organization |> OrgBranch.create_branch(build(:branch, attrs))
  end

  def fixture(:u_status_branch, branch, attrs) do
    branch |> OrgBranch.update_branch_status(attrs)
  end

  def fixture(:close_branch, branch, attrs) do
    branch |> OrgBranch.close_branch()
  end
end
