defmodule Kusanagi.Factory do
  use ExMachina

  alias Kusanagi.Accounts.Commands.RegisterUser
  alias Kusanagi.FinancialAccounting.Commands.CreateFAccount
  alias Kusanagi.FinancialAccounting.Enums.EnumsFAcoount
  alias Kusanagi.Organizations.Commands.{CreateOrganization, AddOrgAttribute, UpdateOrgStatus}
  alias Kusanagi.OrgBranch.Commands.{CreateBranch, AddBranchAttribute, UpdateBranchStatus}
  alias Kusanagi.Organizations.Enums.EnumsOrganization

  def user_factory do
    %{
      email: "jake@jake.jake",
      username: "jake",
      password: "jakejake",
      hashed_password: "jakejake",
      bio: "I like to skateboard",
      role: "user",
      image: "https://i.stack.imgur.com/xHWG8.jpg"
    }
  end

  def f_account_factory do
    %{
      account_type: EnumsFAcoount.account_type()[:asset],
      account_name: "cash account",
      description: "Cash account of the bank",
      balance: 0,
      currency: "USD",
      status: EnumsFAcoount.account_status()[:OPEN],
      org_id: UUID.uuid4(),
      branch_id: UUID.uuid4()
    }
  end

  def f_account_liability_factory do
    %{
      account_type: EnumsFAcoount.account_type()[:liability],
      account_name: "cash account",
      description: "Cash account of the bank",
      balance: 0,
      currency: "INR",
      status: EnumsFAcoount.account_status()[:OPEN],
      org_id: UUID.uuid4(),
      branch_id: UUID.uuid4()
    }
  end

  def organization_factory do
    %{
      address: "some address",
      email: "some email",
      name: "some name",
      phone_number: "some phone_number",
      timeZone: "some timeZone",
      status: EnumsOrganization.organization_status()[:CREATED],
      website: "some website"
    }
  end

  def branch_factory do
    %{
      address: "some address",
      name: "some name",
      identifier: "IDEN_",
      branch_type: "PHYSICAL"
    }
  end

  def org_attr_factory do
    %{
      key: "Timing",
      value: "9:00AM-6:00PM"
    }
  end

  def d_f_account_factory do
    %{
      amount: 100
    }
  end

  def register_user_factory do
    struct(RegisterUser, build(:user))
  end

  def create_f_account_factory do
    struct(CreateFAccount, build(:f_account))
  end

  def create_liability_f_account_factory do
    struct(CreateFAccount, build(:f_account_liability))
  end

  def create_organization_factory do
    struct(CreateOrganization, build(:organization))
  end

  def add_org_attribute_factory do
    struct(AddOrgAttribute, build(:org_attr))
  end

  def add_branch_attribute_factory do
    struct(AddBranchAttribute, build(:org_attr))
  end

  def create_branch_factory do
    struct(CreateBranch, build(:branch, %{}))
  end

  def update_branch_status_factory do
    struct(UpdateBranchStatus, %{})
  end

  def update_organization_status_factory do
    struct(UpdateOrgStatus, %{})
  end
end
