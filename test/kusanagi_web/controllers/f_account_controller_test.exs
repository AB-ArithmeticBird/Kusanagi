defmodule KusanagiWeb.FAccountControllerTest do
  use KusanagiWeb.ConnCase

  import Kusanagi.Factory

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "create f_account" do
    @tag :wip
    test "should create financial account when data is valid", %{conn: conn} do
      uuid = Ecto.UUID.generate()
      org_uuid = Ecto.UUID.generate()
      branch_uuid = Ecto.UUID.generate()
      add_user(uuid)
      add_org(org_uuid)
      add_org_branch(org_uuid, branch_uuid, Enum.random(1..1_000))

      conn =
        post(conn, f_account_path(conn, :create),
          f_account: build(:f_account, user_uuid: uuid, org_id: org_uuid, branch_id: branch_uuid)
        )

      assert json_response(conn, 201)["data"]["account_name"] == "cash account"
      assert json_response(conn, 201)["data"]["description"] == "Cash account of the bank"
    end

    test "should fail to create financial account when user does not exist", %{conn: conn} do
      uuid = Ecto.UUID.generate()
      org_uuid = Ecto.UUID.generate()
      branch_uuid = Ecto.UUID.generate()
      add_user(uuid)
      add_org(org_uuid)
      add_org_branch(org_uuid, branch_uuid, Enum.random(1..1_000))

      conn =
        post(conn, f_account_path(conn, :create),
          f_account: build(:f_account, user_uuid: uuid, org_id: org_uuid, branch_id: branch_uuid)
        )

      assert json_response(conn, 201)["data"]["account_name"] == "cash account"
      assert json_response(conn, 201)["data"]["description"] == "Cash account of the bank"
    end
  end

  describe "debit f_account" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch,
      :verify_branch,
      :activate_branch,
      :register_user2,
      :create_f_account2
    ]

    @tag :wip
    test "should debit financial account when data is valid", %{
      conn: conn,
      user: user,
      f_account: f_account
    } do
      conn =
        post(authenticated_conn(conn, user), f_account_path(conn, :debit),
          f_d_account: %{uuid: f_account.uuid, amount: 1}
        )

      assert json_response(conn, 200)["data"]["balance"] == 1
    end
  end

  describe "credit f_account" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch,
      :verify_branch,
      :activate_branch,
      :register_user2,
      :create_f_account2,
      :debit_f_account
    ]

    @tag :wip
    test "should credit financial account when data is valid", %{
      conn: conn,
      user: user,
      f_account: f_account
    } do
      conn =
        post(authenticated_conn(conn, user), f_account_path(conn, :credit),
          f_c_account: %{uuid: f_account.uuid, amount: 50}
        )

      assert json_response(conn, 200)["data"]["balance"] == 50
    end
  end

  defp add_user(uuid) do
    readstore_config = Application.get_env(:kusanagi, Kusanagi.Repo)

    {:ok, conn} = Postgrex.start_link(readstore_config)

    Postgrex.query!(conn, add_user_in_user_account_table(uuid), [])
  end

  defp add_org(uuid) do
    readstore_config = Application.get_env(:kusanagi, Kusanagi.Repo)

    {:ok, conn} = Postgrex.start_link(readstore_config)

    Postgrex.query!(conn, add_org_in_organization_table(uuid), [])
  end

  defp add_org_branch(org_uuid, branch_uuid, randome) do
    readstore_config = Application.get_env(:kusanagi, Kusanagi.Repo)

    {:ok, conn} = Postgrex.start_link(readstore_config)

    Postgrex.query!(conn, add_branch_in_org_branch_table(org_uuid, branch_uuid, randome), [])
  end

  defp add_user_in_user_account_table(uuid) do
    """
    INSERT INTO public.accounts_users (uuid, username, email, hashed_password, bio, image, inserted_at, updated_at)
      VALUES ('#{uuid}', 'jake', 'jake@jake.jake',
      '$2b$12$kfAEBsoqUu8Gpw6Ju16BveVI9Gctx3IDT9c4cw0cx.h148QhPhLpO', null, null,
      '2018-03-02 07:39:53.629510', '2018-03-02 07:39:53.629522');
    """
  end

  defp add_org_in_organization_table(uuid) do
    """
    INSERT INTO public.organizations (uuid, name, email, phone_number, address, status, website, inserted_at, updated_at)
      VALUES ('#{uuid}', 'Kusanagi', 'support@kusanagi.com', '012-0345-567',
      'ABCD Area, ABCD Country', 3, null,
      '2018-03-02 07:39:53.629510', '2018-03-02 07:39:53.629522');
    """
  end

  defp add_branch_in_org_branch_table(org_uuid, branch_uuid, randome) do
    """
    INSERT INTO public.org_branch (uuid, name, branch_type, identifier, org_uuid, address, status, inserted_at, updated_at)
      VALUES ('#{branch_uuid}', 'Kusanagi_Singapore', 1, 'KUSANAGI_SGP_#{randome}', '#{org_uuid}',
      'ABCD Area, ABCD Country', 3,
      '2018-03-02 07:39:53.629510', '2018-03-02 07:39:53.629522');
    """
  end
end
