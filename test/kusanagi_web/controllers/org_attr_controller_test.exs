defmodule KusanagiWeb.OrgAttrControllerTest do
  use KusanagiWeb.ConnCase

  # @create_org_attrs %{address: "some address", attribute: "some attribute", email: "some email", name: "some name", phone_number: "some phone_number", timeZone: "some timeZone", status: 1, website: "some website"}
  @create_org_attribute %{"key" => "Timing", "value" => "8:00AM-6:00PM"}
  @update_org_attribute %{"key" => "Timing", "value" => "7:30AM-2:30PM"}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "list all org_attributes" do
    setup [:create_organization]

    test "should lists all organization_Attribute", %{conn: conn, organization: organization} do
      conn = get(conn, org_attr_path(conn, :index_attr, organization.uuid))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "Add Attribute" do
    setup [
      :create_organization
    ]

    test "should add attribute to a organization", %{conn: conn, organization: organization} do
      uuid = organization.uuid
      updated_params = Map.put(@create_org_attribute, :org_uuid, uuid)
      conn = post(conn, org_attr_path(conn, :add_attr), org_attribute: updated_params)

      assert json_response(conn, 200)["data"] ==
               %{
                 "org_uuid" => uuid,
                 "key" => "Timing",
                 "value" => "8:00AM-6:00PM"
               }
    end
  end

  describe "Delete Attribute" do
    setup [
      :create_organization,
      :add_org_attribute
    ]

    test "should delete a attribute of an organization", %{
      conn: conn,
      organization: organization,
      org_attributes: org_attributes
    } do
      conn =
        delete(conn, org_attr_path(conn, :delete_attr, organization.uuid, org_attributes.key))

      assert response(conn, 204) == ""
    end
  end

  describe "Update Attribute" do
    setup [
      :create_organization,
      :add_org_attribute
    ]

    test "should update attribute of an organization", %{conn: conn, organization: organization} do
      conn =
        put(conn, org_attr_path(conn, :update_attr),
          org_attribute: Map.put(@update_org_attribute, :org_uuid, organization.uuid)
        )

      assert json_response(conn, 200)["data"] ==
               %{
                 "org_uuid" => organization.uuid,
                 "key" => "Timing",
                 "value" => "7:30AM-2:30PM"
               }

      conn = get(conn, org_attr_path(conn, :index_attr, organization.uuid))

      assert json_response(conn, 200)["data"] == [
               %{
                 "org_uuid" => organization.uuid,
                 "key" => "Timing",
                 "value" => "7:30AM-2:30PM"
               }
             ]
    end
  end
end
