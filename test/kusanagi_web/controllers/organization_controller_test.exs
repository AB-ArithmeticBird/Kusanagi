defmodule KusanagiWeb.OrganizationControllerTest do
  use KusanagiWeb.ConnCase

  @create_attrs %{
    address: "some address",
    attribute: "some attribute",
    email: "some email",
    name: "some name",
    phone_number: "some phone_number",
    timeZone: "some timeZone",
    status: 1,
    website: "some website"
  }
  @update_attrs %{
    address: "some updated address",
    attribute: "some updated attribute",
    email: "some updated email",
    name: "some updated name",
    phone_number: "some updated phone_number",
    timeZone: "some updated timeZone",
    website: "some updated website"
  }
  @active_status %{status: "ACTIVE"}
  @verify_status %{status: "VERIFIED"}
  @inactive_status %{status: "INACTIVE"}
  @invalid_status %{status: "ACTIVATED"}
  @invalid_attrs %{
    address: nil,
    attribute: nil,
    email: nil,
    name: nil,
    phone_number: nil,
    timeZone: nil,
    website: nil
  }

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all organizations", %{conn: conn} do
      conn = get(conn, organization_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create organization" do
    test "renders organization when data is valid", %{conn: conn} do
      conn = post(conn, organization_path(conn, :create), organization: @create_attrs)
      assert %{"uuid" => uuid} = json_response(conn, 201)["data"]

      conn = get(conn, organization_path(conn, :show, uuid))

      assert json_response(conn, 200)["data"] == %{
               "uuid" => uuid,
               "address" => "some address",
               "email" => "some email",
               "name" => "some name",
               "phone_number" => "some phone_number",
               "timeZone" => "some timeZone",
               "website" => "some website",
               "status" => 1
             }
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, organization_path(conn, :create), organization: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update organization" do
    setup [:create_organization]

    test "renders organization when data is valid", %{conn: conn, organization: organization} do
      uuid = organization.uuid
      conn = put(conn, organization_path(conn, :update, uuid), organization: @update_attrs)
      assert %{"uuid" => ^uuid} = json_response(conn, 200)["data"]

      conn = get(conn, organization_path(conn, :show, uuid))

      assert json_response(conn, 200)["data"] == %{
               "uuid" => uuid,
               "address" => "some updated address",
               "email" => "some updated email",
               "name" => "some updated name",
               "phone_number" => "some updated phone_number",
               "timeZone" => "some updated timeZone",
               "website" => "some updated website",
               "status" => 1
             }
    end

    test "renders errors when data is invalid", %{conn: conn, organization: organization} do
      conn =
        put(conn, organization_path(conn, :update, organization.uuid),
          organization: @invalid_attrs
        )

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "Update Inactive Organization " do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :inactivate_org
    ]

    test "should render error", %{
      conn: conn,
      organization: organization
    } do
      conn =
        put(conn, organization_path(conn, :update, organization.uuid), organization: @update_attrs)

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "Update Closed Organization should " do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :close_org
    ]

    test "should render error", %{
      conn: conn,
      organization: organization
    } do
      conn =
        put(conn, organization_path(conn, :update, organization.uuid), organization: @update_attrs)

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "Close Inactive organization" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :inactivate_org
    ]

    test "should succed", %{
      conn: conn,
      organization: organization
    } do
      conn = delete(conn, organization_path(conn, :close, organization.uuid))
      assert response(conn, 204)
    end
  end

  describe "Close active organization" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org
    ]

    test "should succed", %{
      conn: conn,
      organization: organization
    } do
      conn = delete(conn, organization_path(conn, :close, organization.uuid))
      assert response(conn, 204)
    end
  end

  describe "Close New Created organization" do
    setup [
      :create_organization
    ]

    test "should fail", %{
      conn: conn,
      organization: organization
    } do
      conn = delete(conn, organization_path(conn, :close, organization.uuid))
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "Close Verified organization" do
    setup [
      :create_organization,
      :verify_org
    ]

    test "should fail", %{
      conn: conn,
      organization: organization
    } do
      conn = delete(conn, organization_path(conn, :close, organization.uuid))
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "Update organization Status From Created To Verified" do
    setup [:create_organization]

    test "renders organization when data is valid", %{conn: conn, organization: organization} do
      uuid = organization.uuid

      updated_params =
        @verify_status
        |> Map.put(:uuid, uuid)

      conn = put(conn, organization_path(conn, :status), organization: updated_params)
      assert %{"uuid" => ^uuid} = json_response(conn, 200)["data"]

      conn = get(conn, organization_path(conn, :show, uuid))

      assert json_response(conn, 200)["data"] == %{
               "uuid" => uuid,
               "address" => "some address",
               "email" => "some email",
               "name" => "some name",
               "phone_number" => "some phone_number",
               "timeZone" => "some timeZone",
               "website" => "some website",
               "status" => 2
             }
    end
  end

  describe "Update organization Status From Created To Unknown Status" do
    setup [:create_organization]

    test "renders errors", %{conn: conn, organization: organization} do
      uuid = organization.uuid

      updated_params =
        @invalid_status
        |> Map.put(:uuid, uuid)

      conn = put(conn, organization_path(conn, :status), organization: updated_params)
      assert json_response(conn, 422)["errors"] != []
    end
  end

  describe "Update organization Status From Verified To ACTIVE" do
    setup [
      :create_organization,
      :verify_org
    ]

    test "renders organization when data is valid", %{conn: conn, organization: organization} do
      uuid = organization.uuid

      updated_params =
        @active_status
        |> Map.put(:uuid, uuid)

      conn = put(conn, organization_path(conn, :status), organization: updated_params)
      assert %{"uuid" => ^uuid} = json_response(conn, 200)["data"]

      conn = get(conn, organization_path(conn, :show, uuid))

      assert json_response(conn, 200)["data"] == %{
               "uuid" => uuid,
               "address" => "some address",
               "email" => "some email",
               "name" => "some name",
               "phone_number" => "some phone_number",
               "timeZone" => "some timeZone",
               "website" => "some website",
               "status" => 3
             }
    end
  end

  describe "Update organization Status From ACTIVE To INACTIVE" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org
    ]

    test "renders organization when data is valid", %{conn: conn, organization: organization} do
      uuid = organization.uuid

      updated_params =
        @inactive_status
        |> Map.put(:uuid, uuid)

      conn = put(conn, organization_path(conn, :status), organization: updated_params)
      assert %{"uuid" => ^uuid} = json_response(conn, 200)["data"]

      conn = get(conn, organization_path(conn, :show, uuid))

      assert json_response(conn, 200)["data"] == %{
               "uuid" => uuid,
               "address" => "some address",
               "email" => "some email",
               "name" => "some name",
               "phone_number" => "some phone_number",
               "timeZone" => "some timeZone",
               "website" => "some website",
               "status" => 4
             }
    end
  end

  describe "Update organization Status From INACTIVE To ACTIVE" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :inactivate_org
    ]

    test "renders organization when data is valid", %{conn: conn, organization: organization} do
      uuid = organization.uuid

      updated_params =
        @active_status
        |> Map.put(:uuid, uuid)

      conn = put(conn, organization_path(conn, :status), organization: updated_params)
      assert %{"uuid" => ^uuid} = json_response(conn, 200)["data"]

      conn = get(conn, organization_path(conn, :show, uuid))

      assert json_response(conn, 200)["data"] == %{
               "uuid" => uuid,
               "address" => "some address",
               "email" => "some email",
               "name" => "some name",
               "phone_number" => "some phone_number",
               "timeZone" => "some timeZone",
               "website" => "some website",
               "status" => 3
             }
    end
  end
end
