defmodule KusanagiWeb.OrgBranchControllerTest do
  use KusanagiWeb.ConnCase

  alias Kusanagi.OrgBranch.Enums.EnumsOrgBranch

  @create_branch %{
    name: "some name",
    address: "some address",
    branch_type: "PHYSICAL",
    identifier: "IDENT_#{:rand.uniform(50)}_#{:rand.uniform(80)}"
  }
  @update_branch %{name: "some updated name", address: "some updated address"}
  @create_branch_attribute %{"key" => "Timing", "value" => "8:00AM-6:00PM"}
  @update_branch_attribute %{"key" => "Timing", "value" => "7:30AM-2:30PM"}

  @active_status %{status: "ACTIVE"}
  @verify_status %{status: "VERIFIED"}
  @inactive_status %{status: "INACTIVE"}
  @invalid_status %{status: "ACTIVATED"}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "list all branches" do
    setup [:create_organization]

    test "should lists all organization branches", %{conn: conn, organization: organization} do
      conn = get(conn, org_branch_path(conn, :index, organization.uuid))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "Create Branch" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org
    ]

    test "should create a branch for an organization", %{conn: conn, organization: organization} do
      org_uuid = organization.uuid
      uuid = UUID.uuid4()

      updated_params =
        @create_branch
        |> Map.put(:uuid, uuid)
        |> Map.put(:org_uuid, org_uuid)

      conn = post(conn, org_branch_path(conn, :add), branch: updated_params)
      assert json_response(conn, 200)["data"]["org_uuid"] == org_uuid
      assert json_response(conn, 200)["data"]["name"] == "some name"
      assert json_response(conn, 200)["data"]["address"] == "some address"

      assert json_response(conn, 200)["data"]["branch_type"] ==
               EnumsOrgBranch.org_branch_type()[:PHYSICAL]
    end
  end

  describe "Update Branch" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch
    ]

    test "should update a branch for an organization", %{conn: conn, branch: branch} do
      updated_params =
        @update_branch
        |> Map.put(:uuid, branch.uuid)

      conn = put(conn, org_branch_path(conn, :update), branch: updated_params)
      assert json_response(conn, 200)["data"]["name"] == "some updated name"
      assert json_response(conn, 200)["data"]["address"] == "some updated address"
    end
  end

  describe "Update Inactive Branch" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch,
      :verify_branch,
      :activate_branch,
      :inactivate_branch
    ]

    test "should render errors", %{conn: conn, branch: branch} do
      updated_params =
        @update_branch
        |> Map.put(:uuid, branch.uuid)

      conn = put(conn, org_branch_path(conn, :update), branch: updated_params)
      assert json_response(conn, 422)["errors"] != []
    end
  end

  describe "Update Closed Branch" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch,
      :verify_branch,
      :activate_branch,
      :close_branch
    ]

    test "should render errors", %{conn: conn, branch: branch} do
      updated_params =
        @update_branch
        |> Map.put(:uuid, branch.uuid)

      conn = put(conn, org_branch_path(conn, :update), branch: updated_params)
      assert json_response(conn, 422)["errors"] != []
    end
  end

  describe "Close Active Branch" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch,
      :verify_branch,
      :activate_branch
    ]

    test "should succeed", %{conn: conn, branch: branch} do
      conn = delete(conn, org_branch_path(conn, :close, branch.uuid))
      assert response(conn, 200)
    end
  end

  describe "Close InActive Branch" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch,
      :verify_branch,
      :activate_branch,
      :inactivate_branch
    ]

    test "should succeed", %{conn: conn, branch: branch} do
      conn = delete(conn, org_branch_path(conn, :close, branch.uuid))
      assert response(conn, 200)
    end
  end

  describe "Close Newly Created Branch" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch
    ]

    test "should Render Error", %{conn: conn, branch: branch} do
      conn = delete(conn, org_branch_path(conn, :close, branch.uuid))
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "Close Verified Branch" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch,
      :verify_branch
    ]

    test "should Render Error", %{conn: conn, branch: branch} do
      conn = delete(conn, org_branch_path(conn, :close, branch.uuid))
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "Update Branch Status From Created To Verified" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch
    ]

    test "renders branch when data is valid", %{conn: conn, branch: branch} do
      uuid = branch.uuid

      updated_params =
        @verify_status
        |> Map.put(:uuid, uuid)

      conn = put(conn, org_branch_path(conn, :status), branch: updated_params)
      assert %{"uuid" => ^uuid} = json_response(conn, 200)["data"]

      conn = get(conn, org_branch_path(conn, :show, uuid))

      assert json_response(conn, 200)["data"]["org_uuid"] == branch.org_uuid
      assert json_response(conn, 200)["data"]["name"] == "some name"
      assert json_response(conn, 200)["data"]["address"] == "some address"
      assert json_response(conn, 200)["data"]["status"] == 2
    end
  end

  describe "Update branch Status From Created To Unknown Status" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch
    ]

    test "renders errors", %{conn: conn, branch: branch} do
      uuid = branch.uuid

      updated_params =
        @invalid_status
        |> Map.put(:uuid, uuid)

      conn = put(conn, org_branch_path(conn, :status), branch: updated_params)
      assert json_response(conn, 422)["errors"] != []
    end
  end

  describe "Update Branch Status From Verified To ACTIVE" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch,
      :verify_branch
    ]

    test "renders branch when data is valid", %{conn: conn, branch: branch} do
      uuid = branch.uuid

      updated_params =
        @active_status
        |> Map.put(:uuid, uuid)

      conn = put(conn, org_branch_path(conn, :status), branch: updated_params)
      assert %{"uuid" => ^uuid} = json_response(conn, 200)["data"]

      conn = get(conn, org_branch_path(conn, :show, uuid))

      assert json_response(conn, 200)["data"]["org_uuid"] == branch.org_uuid
      assert json_response(conn, 200)["data"]["name"] == "some name"
      assert json_response(conn, 200)["data"]["address"] == "some address"
      assert json_response(conn, 200)["data"]["status"] == 3
    end
  end

  describe "Update Branch Status From ACTIVE To INACTIVE" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch,
      :verify_branch,
      :activate_branch
    ]

    test "renders Branch when data is valid", %{conn: conn, branch: branch} do
      uuid = branch.uuid

      updated_params =
        @inactive_status
        |> Map.put(:uuid, uuid)

      conn = put(conn, org_branch_path(conn, :status), branch: updated_params)
      assert %{"uuid" => ^uuid} = json_response(conn, 200)["data"]

      conn = get(conn, org_branch_path(conn, :show, uuid))

      assert json_response(conn, 200)["data"]["org_uuid"] == branch.org_uuid
      assert json_response(conn, 200)["data"]["name"] == "some name"
      assert json_response(conn, 200)["data"]["address"] == "some address"
      assert json_response(conn, 200)["data"]["status"] == 4
    end
  end

  describe "Update Branch Status From INACTIVE To ACTIVE" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch,
      :verify_branch,
      :activate_branch,
      :inactivate_branch
    ]

    test "renders Branch when data is valid", %{conn: conn, branch: branch} do
      uuid = branch.uuid

      updated_params =
        @active_status
        |> Map.put(:uuid, uuid)

      conn = put(conn, org_branch_path(conn, :status), branch: updated_params)
      assert %{"uuid" => ^uuid} = json_response(conn, 200)["data"]

      conn = get(conn, org_branch_path(conn, :show, uuid))

      assert json_response(conn, 200)["data"]["org_uuid"] == branch.org_uuid
      assert json_response(conn, 200)["data"]["name"] == "some name"
      assert json_response(conn, 200)["data"]["address"] == "some address"
      assert json_response(conn, 200)["data"]["status"] == 3
    end
  end

  describe "list all branch_attributes" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch
    ]

    test "should lists all branch_Attribute of a specific branch", %{conn: conn, branch: branch} do
      conn = get(conn, org_branch_path(conn, :index_attr, branch.uuid))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "Add Attribute" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch
    ]

    test "should add attribute to a branch", %{conn: conn, branch: branch} do
      uuid = branch.uuid
      updated_params = Map.put(@create_branch_attribute, :b_uuid, uuid)
      conn = post(conn, org_branch_path(conn, :add_attr), branch_attribute: updated_params)

      assert json_response(conn, 200)["data"] ==
               %{
                 "branch_uuid" => uuid,
                 "key" => "Timing",
                 "value" => "8:00AM-6:00PM"
               }
    end
  end

  describe "Delete Attribute" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch,
      :add_branch_attribute
    ]

    test "should delete a attribute of an organization", %{
      conn: conn,
      branch: branch,
      b_attributes: b_attributes
    } do
      conn = delete(conn, org_branch_path(conn, :delete_attr, branch.uuid, b_attributes.key))
      assert response(conn, 204) == ""
    end
  end

  describe "Update Attribute" do
    setup [
      :create_organization,
      :verify_org,
      :activate_org,
      :create_branch,
      :add_branch_attribute
    ]

    test "should update attribute of an organization", %{
      conn: conn,
      branch: branch,
      b_attributes: b_attributes
    } do
      conn =
        put(conn, org_branch_path(conn, :update_attr),
          branch_attribute:
            @update_branch_attribute
            |> Map.put(:b_uuid, branch.uuid)
            |> Map.put(:key, b_attributes.key)
        )

      assert json_response(conn, 200)["data"] ==
               %{
                 "branch_uuid" => branch.uuid,
                 "key" => "Timing",
                 "value" => "7:30AM-2:30PM"
               }

      conn = get(conn, org_branch_path(conn, :index_attr, branch.uuid))

      assert json_response(conn, 200)["data"] == [
               %{
                 "branch_uuid" => branch.uuid,
                 "key" => "Timing",
                 "value" => "7:30AM-2:30PM"
               }
             ]
    end
  end
end
