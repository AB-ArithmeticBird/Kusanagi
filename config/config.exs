# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :kusanagi,
  ecto_repos: [Kusanagi.Repo]

# Configures the endpoint
config :kusanagi, KusanagiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "BgJEwxSXRmstaAAezIPwC6w+QLe5ZspC/XEJMlwlZ9ZJSWB/D4sVyIKoeCgKl6Gi",
  render_errors: [view: KusanagiWeb.ErrorView, accepts: ~w(json)]

config :kusanagi, :phoenix_swagger,
  swagger_files: %{
    "priv/static/swagger.json" => [
      # phoenix routes will be converted to swagger paths
      router: KusanagiWeb.Router,
      # (optional) endpoint config used to set host, port and https schemes.
      endpoint: KusanagiWeb.Endpoint
    ]
  }

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :libcluster,
  topologies: [
    example: [
      strategy: ClusterConsul.Strategy,
      connect: {:net_kernel, :connect_node, []},
      disconnect: {:erlang, :disconnect_node, []},
      list_nodes: {:erlang, :nodes, [:connected]},
      config: [
        service_name: "kusanagi",
        service_port: 4000
      ]
    ]
  ]

config :commanded,
  event_store_adapter: Commanded.EventStore.Adapters.EventStore

config :commanded_ecto_projections,
  repo: Kusanagi.Repo

config :commanded_audit_middleware,
  ecto_repos: [Commanded.Middleware.Auditing.Repo],
  serializer: Commanded.Serialization.JsonSerializer

config :vex,
  sources: [
    Kusanagi.Accounts.Validators,
    Kusanagi.Support.Validators,
    Vex.Validators
  ]

config :eventstore, registry: :distributed

config :commanded,
  registry: Commanded.Registration.SwarmRegistry

# config(:exometer_core, report: [reporters: [{:exometer_report_tty, []}]])
#
# config(:elixometer,
#          reporter: :exometer_report_tty,
#          env: Mix.env,
#          metric_prefix: "kusanagi")

# config :elixometer, env: {:system, "ELIXOMETER_ENV"}

config :commanded, Kusanagi.FinancialAccounting.Aggregates.FAccount,
  snapshot_every: 10,
  snapshot_version: 1

config :guardian, Guardian,
  allowed_algos: ["HS512"],
  verify_module: Guardian.JWT,
  issuer: "Kusanagi",
  ttl: {30, :days},
  allowed_drift: 2000,
  verify_issuer: true,
  secret_key: "vM3GMlpDrH4jFxfDsbx/Y7m9ul3QL1Ucc7cQ78mZGCx9KJX/6su99OgI5Clw0g5q",
  serializer: Kusanagi.Auth.GuardianSerializer

import_config "#{Mix.env()}.exs"
