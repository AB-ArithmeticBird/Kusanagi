use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :kusanagi, KusanagiWeb.Endpoint,
  http: [port: 4001],
  server: false,
  pubsub: [name: Kusanagi.PubSub, adapter: Phoenix.PubSub.PG2]

# Print only warnings and errors during test
config :logger, level: :warn

config :commanded, registry: :local
config :eventstore, registry: :local

config :libcluster,
  topologies: [
    example: [
      strategy: Cluster.Strategy.Epmd,
      config: [hosts: [:"node1@127.0.0.1", :"node2@127.0.0.1", :"node3@127.0.0.1"]]
    ]
  ]

# Configure the event store database
config :eventstore, EventStore.Storage,
  serializer: Commanded.Serialization.JsonSerializer,
  username: "postgres",
  password: "postgres",
  database: "kusanagi_eventstore_test",
  hostname: "postgres",
  port: 5432,
  pool_size: 1

# Configure the read store database
config :kusanagi, Kusanagi.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "kusanagi_readstore_test",
  hostname: "postgres",
  port: 5432,
  pool_size: 1

config :commanded_audit_middleware, Commanded.Middleware.Auditing.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "kusanagi_audit_middleware_test",
  username: "postgres",
  password: "postgres",
  hostname: "postgres",
  port: 5432,
  pool_size: 1

config :comeonin, :bcrypt_log_rounds, 4
