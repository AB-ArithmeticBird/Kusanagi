defmodule Kusanagi.Mixfile do
  use Mix.Project

  def project do
    [
      app: :kusanagi,
      version: "0.0.2",
      elixir: "~> 1.7.3",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext, :phoenix_swagger] ++ Mix.compilers(),
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
    ]
  end

  def application do
    [
      extra_applications: [
        :phoenix_pubsub_redis,
        :logger,
        :runtime_tools,
        :swarm,
        :libcluster,
        :eventstore,
        :edeliver,
        :phoenix
      ],
      mod: {Kusanagi.Application, []}
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp deps do
    [
      {:distillery, "~> 1.5", runtime: false},
      # {:elixometer, "~> 1.2"}, #TODO Configure it as some error popping up
      {:guardian, "0.14.5"},
      {:bcrypt_elixir, "~> 1.0"},
      {:comeonin, "~> 4.0"},
      {:vex, "~> 0.6"},
      {:commanded_ecto_projections, "~> 0.6"},
      {:exconstructor, "~> 1.1"},
      {:ex_machina, "~> 2.1", only: :test},
      # {:commanded, "~> 0.17.1", override: true},
      {:commanded,
       git: "https://github.com/Navneet-gupta01/commanded.git",
       ref: "1aa51422668e678616a5eb2b4b9aec59266997f5",
       override: true},
      {:commanded_eventstore_adapter, "~> 0.4"},
      # {:commanded_eventstore_adapter,
      #  git: "https://github.com/commanded/commanded-eventstore-adapter.git", branch: "master"},
      {:phoenix, "~> 1.3.2"},
      {:phoenix_ecto, "~> 3.3"},
      {:postgrex, ">= 0.0.0"},
      {:gettext, "~> 0.11"},
      {:cowboy, "~> 1.0"},
      # {:ex_doc, "~> 0.12"}, # to generate html docs available inside doc/index.html  through command mix docs
      {:bodyguard, "~> 2.2"},
      {:commanded_audit_middleware, "~> 0.3"},
      # {:commanded_audit_middleware,
      #  git: "https://github.com/Navneet-gupta01/commanded-audit-middleware.git",
      #  branch: "uuid_version_update"},
      {:edeliver, "~> 1.5.0"},
      {:phoenix_pubsub, "~> 1.0"},
      {:phoenix_pubsub_redis, "~> 2.1.0"},
      {:libcluster, "~> 2.5.0"},
      {:libcluster_consul,
       git: "https://github.com/Navneet-gupta01/cluster_test.git", tag: "0.6"},
      {:commanded_swarm_registry, "~> 0.1"},
      {:excoveralls, "~> 0.10", only: :test},
      # {:dialyxir, "~> 0.5", only: [:dev], runtime: false},
      {:mix_test_watch, "~> 0.5", only: :dev, runtime: false},
      {:phoenix_swagger, "~> 0.8"},
      {:ex_json_schema, "~> 0.5"},
      {:uuid, "~> 1.1.8", override: true},
      {:plug_cowboy, "~> 1.0"}
    ]
  end

  defp aliases do
    [
      "event_store.reset": ["event_store.drop", "event_store.create", "event_store.init"],
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
